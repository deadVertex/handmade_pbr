#include <cstdlib> // For malloc

#include <vulkan/vulkan.h>
#include <GLFW/glfw3.h>

#include "renderer_backend.h"
#include "vulkan_renderer_backend.h"
#include "shader_types.h"
#include "mat4.cpp"

#pragma warning(disable : 4100)
#define IGNORE_FOR_UNIT_TESTING_FOR_NOW
#include "vulkan_renderer_backend.cpp"

#pragma warning(push)
#pragma warning(disable : 4191)
#pragma warning(disable : 4702)
DEFINE_FFF_GLOBALS;
FAKE_VALUE_FUNC(VkResult, vkCreateImage, VkDevice, const VkImageCreateInfo *,
    const VkAllocationCallbacks *, VkImage *);

FAKE_VOID_FUNC(vkGetPhysicalDeviceMemoryProperties, VkPhysicalDevice,
    VkPhysicalDeviceMemoryProperties *);

FAKE_VALUE_FUNC(VkResult, vkAllocateMemory, VkDevice,
    const VkMemoryAllocateInfo *, const VkAllocationCallbacks *,
    VkDeviceMemory *);

FAKE_VALUE_FUNC(VkResult, vkBindImageMemory, VkDevice, VkImage, VkDeviceMemory,
    VkDeviceSize);

FAKE_VOID_FUNC(
    vkGetImageMemoryRequirements, VkDevice, VkImage, VkMemoryRequirements *);

FAKE_VALUE_FUNC(VkResult, vkCreateImageView, VkDevice,
    const VkImageViewCreateInfo *, const VkAllocationCallbacks *,
    VkImageView *);

FAKE_VALUE_FUNC(
    VkCommandBuffer, CreateTemporaryCommandBuffer, VkDevice, VkCommandPool);

FAKE_VOID_FUNC(SubmitTemporaryCommandBuffer, VkDevice, VkCommandBuffer,
    VkCommandPool, VkQueue);

FAKE_VOID_FUNC(vkCmdCopyBufferToImage, VkCommandBuffer, VkBuffer, VkImage,
    VkImageLayout, uint32_t, const VkBufferImageCopy *);

FAKE_VOID_FUNC(vkCmdPipelineBarrier, VkCommandBuffer, VkPipelineStageFlags,
    VkPipelineStageFlags, VkDependencyFlags, uint32_t, const VkMemoryBarrier *,
    uint32_t, const VkBufferMemoryBarrier *, uint32_t,
    const VkImageMemoryBarrier *);

FAKE_VALUE_FUNC(VkResult, vkCreateShaderModule, VkDevice,
    const VkShaderModuleCreateInfo *, const VkAllocationCallbacks *,
    VkShaderModule *);

FAKE_VALUE_FUNC(VkResult, vkCreatePipelineCache, VkDevice,
    const VkPipelineCacheCreateInfo *, const VkAllocationCallbacks *,
    VkPipelineCache *);

FAKE_VALUE_FUNC(VkResult, vkCreateGraphicsPipelines, VkDevice, VkPipelineCache,
    uint32_t, const VkGraphicsPipelineCreateInfo *,
    const VkAllocationCallbacks *, VkPipeline *);

FAKE_VALUE_FUNC(VkResult, vkCreatePipelineLayout, VkDevice,
    const VkPipelineLayoutCreateInfo *, const VkAllocationCallbacks *,
    VkPipelineLayout *);

FAKE_VALUE_FUNC(VkResult, vkCreateDescriptorSetLayout, VkDevice,
    const VkDescriptorSetLayoutCreateInfo *, const VkAllocationCallbacks *,
    VkDescriptorSetLayout *);

FAKE_VALUE_FUNC(VkResult, vkAllocateDescriptorSets, VkDevice,
    const VkDescriptorSetAllocateInfo *, VkDescriptorSet *);

FAKE_VOID_FUNC(vkUpdateDescriptorSets, VkDevice, uint32_t,
    const VkWriteDescriptorSet *, uint32_t, const VkCopyDescriptorSet *);

FAKE_VALUE_FUNC(VkResult, vkCreateComputePipelines, VkDevice, VkPipelineCache,
    uint32_t, const VkComputePipelineCreateInfo *,
    const VkAllocationCallbacks *, VkPipeline *);

FAKE_VOID_FUNC(
    vkCmdBindPipeline, VkCommandBuffer, VkPipelineBindPoint, VkPipeline);

FAKE_VOID_FUNC(vkCmdBindDescriptorSets, VkCommandBuffer, VkPipelineBindPoint,
    VkPipelineLayout, uint32_t, uint32_t, const VkDescriptorSet *, uint32_t,
    const uint32_t *);

FAKE_VOID_FUNC(vkCmdDispatch, VkCommandBuffer, uint32_t, uint32_t, uint32_t);

FAKE_VALUE_FUNC(VkResult, vkCreateFramebuffer, VkDevice,
    const VkFramebufferCreateInfo *, const VkAllocationCallbacks *,
    VkFramebuffer *);

FAKE_VALUE_FUNC(VkResult, vkCreateRenderPass, VkDevice,
    const VkRenderPassCreateInfo *, const VkAllocationCallbacks *,
    VkRenderPass *);
#pragma warning(pop)

TEST_GROUP(VulkanRendererBackend);

TEST_SETUP(VulkanRendererBackend)
{
    RESET_FAKE(vkCreateImage);
}

TEST_TEAR_DOWN(VulkanRendererBackend)
{
}

TEST(VulkanRendererBackend, Instantiate)
{
    VulkanRenderer renderer = {};
    TEST_ASSERT_EQUAL(0, renderer.pipelineCount);
}

TEST(VulkanRendererBackend, CreateImageCommand)
{
    // Given a renderer
    VulkanRenderer renderer = {};

    // When we process a CreateImageCommand
    u32 imageId = 1;
    RendererBackendCommand command = {};
    command.type = RendererBackendCommandType_CreateImage;
    command.createImage.id = imageId;

    ProcessCommands(&renderer, &command, 1);

    // Then the image state can be retrieved by ID
    TEST_ASSERT_NOT_NULL(FindTexture(&renderer, imageId));
}

TEST(VulkanRendererBackend, UpdateImageCommand)
{
    // Given a renderer with an image
    VulkanRenderer renderer = {};

    u32 imageId = 1;
    RendererBackendCommand command = {};
    command.type = RendererBackendCommandType_CreateImage;
    command.createImage.id = imageId;
    ProcessCommands(&renderer, &command, 1);

    // When we update the image
    RendererBackendCommand command2 = {};
    command2.type = RendererBackendCommandType_UpdateImage;
    command2.updateImage.id = imageId;

    ProcessCommands(&renderer, &command2, 1);

    // Then 
    TEST_ASSERT_NOT_NULL(FindTexture(&renderer, imageId));
}

TEST(VulkanRendererBackend, CreateGraphicsPipeline)
{
    // Given a vulkan renderer with a render pass
    VulkanRenderer renderer = {};
    u32 renderPassId = 4;
    RenderPassState renderPassState = {};
    StoreRenderPass(&renderer, renderPassId, renderPassState);

    // When we create a graphics pipeline
    u32 pipelineId = 12;
    RendererBackendCommand command = {};
    command.type = RendererBackendCommandType_CreateGraphicsPipeline;
    command.createGraphicsPipeline.id = pipelineId;
    command.createGraphicsPipeline.renderPass = renderPassId;

    ProcessCommands(&renderer, &command, 1);

    // Then we can retrieve it by ID
    TEST_ASSERT_NOT_NULL(FindGraphicsPipeline(&renderer, pipelineId));
}

TEST_GROUP_RUNNER(VulkanRendererBackend)
{
    RUN_TEST_CASE(VulkanRendererBackend, Instantiate);
    RUN_TEST_CASE(VulkanRendererBackend, CreateImageCommand);
    RUN_TEST_CASE(VulkanRendererBackend, UpdateImageCommand);
    RUN_TEST_CASE(VulkanRendererBackend, CreateGraphicsPipeline);
}
