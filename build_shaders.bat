@echo off

if not exist build mkdir build
pushd build

REM Assuming glslangvalidator is on our system path
glslangvalidator ..\src\shaders\mesh.vert.glsl -V -o mesh.vert.spv
glslangvalidator ..\src\shaders\mesh.frag.glsl -V -o mesh.frag.spv
glslangvalidator ..\src\shaders\mesh_green.frag.glsl -V -o mesh_green.frag.spv
glslangvalidator ..\src\shaders\mesh_lighting.frag.glsl -V -o mesh_lighting.frag.spv
glslangvalidator ..\src\shaders\pbr_material.frag.glsl -V -o pbr_material.frag.spv
glslangvalidator ..\src\shaders\skybox.vert.glsl -V -o skybox.vert.spv
glslangvalidator ..\src\shaders\skybox.frag.glsl -V -o skybox.frag.spv
glslangvalidator ..\src\shaders\post_processing.vert.glsl -V -o post_processing.vert.spv
glslangvalidator ..\src\shaders\post_processing.frag.glsl -V -o post_processing.frag.spv
glslangvalidator ..\src\shaders\irradiance_cubemap.comp.glsl -V -o irradiance_cubemap.comp.spv
glslangvalidator ..\src\shaders\convert_equirectangular.comp.glsl -V -o convert_equirectangular.comp.spv

popd
