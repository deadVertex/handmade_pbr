HdrCubeMap CreateHdrCubeMap(StackAllocator *allocator, u32 width, u32 height)
{
    HdrCubeMap result = {};
    for (u32 faceIdx = 0; faceIdx < MAX_CUBE_MAP_FACES; faceIdx++)
    {
        result.images[faceIdx] = CreateHdrImage(allocator, width, height);
    }

    return result;
}

vec3 MapCubeMapLayerIndexToVector(u32 layerIndex)
{
    vec3 result = {};

    switch (layerIndex)
    {
        case CUBE_MAP_POSITIVE_X:
            result = Vec3(1, 0, 0);
            break;
        case CUBE_MAP_NEGATIVE_X:
            result = Vec3(-1, 0, 0);
            break;
        case CUBE_MAP_POSITIVE_Y:
            result = Vec3(0, 1, 0);
            break;
        case CUBE_MAP_NEGATIVE_Y:
            result = Vec3(0, -1, 0);
            break;
        case CUBE_MAP_POSITIVE_Z:
            result = Vec3(0, 0, 1);
            break;
        case CUBE_MAP_NEGATIVE_Z:
            result = Vec3(0, 0, -1);
            break;
        default:
            INVALID_CODE_PATH();
            break;
    }

    return result;
}

BasisVectors MapCubeMapLayerIndexToBasisVectors(u32 layerIndex)
{
    vec3 forward = {};
    vec3 up = {};
    vec3 right = {};

    // NOTE: Doesn't match the cross product
    // https://en.wikipedia.org/wiki/Cube_mapping#/media/File:Cube_map.svg
    switch (layerIndex)
    {
    case CUBE_MAP_POSITIVE_X:
        forward = Vec3(1, 0, 0);
        up = Vec3(0, 1, 0);
        right = Vec3(0, 0, -1);
        break;
    case CUBE_MAP_NEGATIVE_X:
        forward = Vec3(-1, 0, 0);
        up = Vec3(0, 1, 0);
        right = Vec3(0, 0, 1);
        break;
    case CUBE_MAP_POSITIVE_Y:
        forward = Vec3(0, 1, 0);
        up = Vec3(0, 0, -1);
        right = Vec3(1, 0, 0);
        break;
    case CUBE_MAP_NEGATIVE_Y:
        forward = Vec3(0, -1, 0);
        up = Vec3(0, 0, 1);
        right = Vec3(1, 0, 0);
        break;
    case CUBE_MAP_POSITIVE_Z:
        forward = Vec3(0, 0, 1);
        up = Vec3(0, 1, 0);
        right = Vec3(1, 0, 0);
        break;
    case CUBE_MAP_NEGATIVE_Z:
        forward = Vec3(0, 0, -1);
        up = Vec3(0, 1, 0);
        right = Vec3(-1, 0, 0);
        break;
    default:
        INVALID_CODE_PATH();
        break;
    }

    BasisVectors result = {forward, up, right};

    return result;
}

HdrCubeMap CreateHdrCubeMapFromEquirectangularImage(
    HdrImage equirectangularImage, StackAllocator *allocator, u32 width,
    u32 height)
{
    HdrCubeMap result = CreateHdrCubeMap(allocator, width, height);

    // Copy test data into upload buffer
    for (u32 layerIndex = 0; layerIndex < MAX_CUBE_MAP_FACES; ++layerIndex)
    {
        // Map layer index to basis vectors for cube map face
        BasisVectors basis = MapCubeMapLayerIndexToBasisVectors(layerIndex);

        HdrImage *dstImage = result.images + layerIndex;

        for (u32 y = 0; y < height; ++y)
        {
            for (u32 x = 0; x < width; ++x)
            {
                // Convert pixel to cartesian direction vector
                f32 fx = (f32)x / (f32)width;
                f32 fy = (f32)y / (f32)height;

                // Flip Y axis
                fy = 1.0f - fy;

                // Map to -1 to 1
                fx = fx * 2.0f - 1.0f;
                fy = fy * 2.0f - 1.0f;

                vec3 dir = basis.forward + basis.right * fx + basis.up * fy;
                dir = Normalize(dir);

                // Sample equirectangular texture using direction vector
                vec2 sphereCoords = ToSphericalCoordinates(dir);
                vec2 uv = MapToEquirectangular(sphereCoords);
                uv.y = 1.0f - uv.y; // Flip Y axis as usual
                vec4 sample = SampleImageBilinear(equirectangularImage, uv);

                // Store sample for pixel
                u32 pixelIndex = (y * dstImage->width + x) * 4;
                *(vec4 *)(dstImage->pixels + pixelIndex) = sample;
            }
        }
    }

    return result;
}

// TODO: Does it rather make sense to pass in an already initialize HdrCubeMap
// structure which has been allocated from the textureUploadArena
HdrCubeMap CreateIrradianceCubeMap(HdrImage equirectangularImage,
    StackAllocator *allocator, u32 width, u32 height)
{
    HdrCubeMap result = CreateHdrCubeMap(allocator, width, height);

    RandomNumberGenerator rng = {};
    rng.state = 0x45BA12F3;

    for (u32 layerIndex = 0; layerIndex < MAX_CUBE_MAP_FACES; ++layerIndex)
    {
        // Map layer index to basis vectors for cube map face
        BasisVectors basis =
            MapCubeMapLayerIndexToBasisVectors(layerIndex);

        HdrImage *dstImage = result.images + layerIndex;

        for (u32 y = 0; y < dstImage->height; ++y)
        {
            for (u32 x = 0; x < dstImage->width; ++x)
            {
                // Convert pixel to cartesian direction vector
                f32 fx = (f32)x / (f32)width;
                f32 fy = (f32)y / (f32)height;

                // Flip Y axis
                fy = 1.0f - fy;

                // Map to -1 to 1
                fx = fx * 2.0f - 1.0f;
                fy = fy * 2.0f - 1.0f;

                vec3 dir = basis.forward + basis.right * fx + basis.up * fy;
                dir = Normalize(dir);

                vec3 irradiance = {};

                // From https://learnopengl.com/PBR/IBL/Diffuse-irradiance
                vec3 normal = dir;
                vec3 tangent = Normalize(Cross(basis.up, normal));
                vec3 bitangent = Normalize(Cross(normal, tangent));

                f32 sampleDelta = 0.1f; // TODO: Parameterize
                u32 sampleCount = 0;
                for (f32 phi = 0.0f; phi < 2.0f * PI; phi += sampleDelta)
                {
                    for (f32 theta = 0.0f; theta < 0.5f * PI; theta += sampleDelta)
                    {
                        // Get sample vector in tangent space
                        vec3 tangentDir = MapSphericalToCartesianCoordinates(
                            Vec2(phi, theta));

                        // Map vector from tangent space to world space so we
                        // can sample the environment map
                        vec3 worldDir = normal * tangentDir.y +
                                        tangent * tangentDir.x +
                                        bitangent * tangentDir.z;

                        // Sample environment map
                        vec2 sphereCoords = ToSphericalCoordinates(worldDir);
                        vec2 uv = MapToEquirectangular(sphereCoords);
                        uv.y = 1.0f - uv.y; // Flip Y axis as usual
                        vec3 sample =
                            SampleImageBilinear(equirectangularImage, uv).xyz;

                        // Perform radiance clamp
                        vec3 radiance = sample;
//#if RADIANCE_CLAMP
                        //radiance = Clamp(radiance, Vec3(0), Vec3(RADIANCE_CLAMP));
//#endif

                        // Add irradiance contribution for integral
                        irradiance += radiance * Cos(theta) * Sin(theta);
                        sampleCount++;
                    }
                }

                irradiance = PI * irradiance * (1.0f / (f32)sampleCount);

                // Write irradiance value to image
                u32 pixelIndex = (y * dstImage->width + x) * 4;
                *(vec4 *)(dstImage->pixels + pixelIndex) = Vec4(irradiance, 1);
            }
        }
    }

    return result;
}
