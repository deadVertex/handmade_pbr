VkDescriptorSetLayout CreateDescriptorSetLayout(VkDevice device,
        VkDescriptorSetLayoutBinding *bindings, u32 count)
{
    VkDescriptorSetLayoutCreateInfo createInfo = {
        VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO};
    createInfo.bindingCount = count;
    createInfo.pBindings = bindings;

    VkDescriptorSetLayout descriptorSetLayout = VK_NULL_HANDLE;

    VK_CHECK(vkCreateDescriptorSetLayout(
        device, &createInfo, NULL, &descriptorSetLayout));

    return descriptorSetLayout;
}

VkPipelineCache CreatePipelineCache(VkDevice device)
{
    VkPipelineCacheCreateInfo createInfo = {
        VK_STRUCTURE_TYPE_PIPELINE_CACHE_CREATE_INFO};
    createInfo.initialDataSize = 0;

    VkPipelineCache pipelineCache = VK_NULL_HANDLE;
    VK_CHECK(vkCreatePipelineCache(device, &createInfo, NULL, &pipelineCache));
    return pipelineCache;
}

VkDescriptorSet AllocateDescriptorSet(VkDevice device,
    VkDescriptorPool descriptorPool, VkDescriptorSetLayout descriptorSetLayout)
{
    VkDescriptorSetAllocateInfo allocInfo = {
        VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO};
    allocInfo.descriptorPool = descriptorPool;
    allocInfo.descriptorSetCount = 1;
    allocInfo.pSetLayouts = &descriptorSetLayout;

    VkDescriptorSet descriptorSet = VK_NULL_HANDLE;
    VK_CHECK(vkAllocateDescriptorSets(device, &allocInfo, &descriptorSet));
    return descriptorSet;
}


VkPipelineLayout CreatePipelineLayout(VkDevice device,
    VkDescriptorSetLayout descriptorSetLayout, u32 pushConstantsSize,
    u32 stageFlags)
{
    VkPushConstantRange pushConstantRange = {};
    pushConstantRange.size = pushConstantsSize;
    pushConstantRange.stageFlags = stageFlags;

    VkPipelineLayoutCreateInfo createInfo = {
        VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO};
    createInfo.setLayoutCount = 1;
    createInfo.pSetLayouts = &descriptorSetLayout;
    if (pushConstantsSize > 0)
    {
        createInfo.pPushConstantRanges = &pushConstantRange;
        createInfo.pushConstantRangeCount = 1;
    }

    VkPipelineLayout layout = VK_NULL_HANDLE;
    VK_CHECK(vkCreatePipelineLayout(device, &createInfo, 0, &layout));
    return layout;
}

VkPipeline CreatePipeline(VkDevice device,
    VkShaderModule vertexShader, VkShaderModule fragmentShader,
    VkPipelineLayout layout, VkRenderPass renderPass,
    VkPipelineCache pipelineCache)
{
    VkPipelineShaderStageCreateInfo stages[2] = {};
    stages[0].sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
    stages[0].stage = VK_SHADER_STAGE_VERTEX_BIT;
    stages[0].module = vertexShader;
    stages[0].pName = "main";
    stages[1].sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
    stages[1].stage = VK_SHADER_STAGE_FRAGMENT_BIT;
    stages[1].module = fragmentShader;
    stages[1].pName = "main";

    VkPipelineVertexInputStateCreateInfo vertexInput = {
        VK_STRUCTURE_TYPE_PIPELINE_VERTEX_INPUT_STATE_CREATE_INFO};

    VkPipelineInputAssemblyStateCreateInfo inputAssembly = {
        VK_STRUCTURE_TYPE_PIPELINE_INPUT_ASSEMBLY_STATE_CREATE_INFO};
    inputAssembly.topology = VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST;

    VkPipelineViewportStateCreateInfo viewportState = {
        VK_STRUCTURE_TYPE_PIPELINE_VIEWPORT_STATE_CREATE_INFO};
    viewportState.viewportCount = 1;
    viewportState.scissorCount = 1;

    VkPipelineRasterizationStateCreateInfo rasterizationState = {
        VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_STATE_CREATE_INFO};
    rasterizationState.polygonMode = VK_POLYGON_MODE_FILL;
    rasterizationState.cullMode = VK_CULL_MODE_NONE;
    rasterizationState.frontFace = VK_FRONT_FACE_COUNTER_CLOCKWISE;
    rasterizationState.lineWidth = 1.0f;

    VkPipelineMultisampleStateCreateInfo multisampleState = {
        VK_STRUCTURE_TYPE_PIPELINE_MULTISAMPLE_STATE_CREATE_INFO};
    multisampleState.rasterizationSamples = VK_SAMPLE_COUNT_1_BIT;

    VkPipelineDepthStencilStateCreateInfo depthStencilState = {
        VK_STRUCTURE_TYPE_PIPELINE_DEPTH_STENCIL_STATE_CREATE_INFO};
    depthStencilState.depthTestEnable = VK_TRUE;
    depthStencilState.depthWriteEnable = VK_TRUE;
    depthStencilState.depthCompareOp = VK_COMPARE_OP_LESS;
    depthStencilState.depthBoundsTestEnable = VK_FALSE;
    depthStencilState.minDepthBounds = 0.0f;
    depthStencilState.maxDepthBounds = 1.0f;
    depthStencilState.stencilTestEnable = VK_FALSE;

    VkPipelineColorBlendAttachmentState colorAttachmentState = {};
    colorAttachmentState.colorWriteMask =
        VK_COLOR_COMPONENT_R_BIT | VK_COLOR_COMPONENT_G_BIT |
        VK_COLOR_COMPONENT_B_BIT | VK_COLOR_COMPONENT_A_BIT;

    VkPipelineColorBlendStateCreateInfo colorBlendState = {
        VK_STRUCTURE_TYPE_PIPELINE_COLOR_BLEND_STATE_CREATE_INFO};
    colorBlendState.attachmentCount = 1;
    colorBlendState.pAttachments = &colorAttachmentState;

    VkDynamicState dynamicStates[] = {
        VK_DYNAMIC_STATE_VIEWPORT, VK_DYNAMIC_STATE_SCISSOR};
    VkPipelineDynamicStateCreateInfo dynamicState = {
        VK_STRUCTURE_TYPE_PIPELINE_DYNAMIC_STATE_CREATE_INFO};
    dynamicState.dynamicStateCount = ARRAY_COUNT(dynamicStates);
    dynamicState.pDynamicStates = dynamicStates;

    VkGraphicsPipelineCreateInfo createInfo = {
        VK_STRUCTURE_TYPE_GRAPHICS_PIPELINE_CREATE_INFO};

    createInfo.stageCount = ARRAY_COUNT(stages);
    createInfo.pStages = stages;
    createInfo.pVertexInputState = &vertexInput;
    createInfo.pInputAssemblyState = &inputAssembly;
    createInfo.pViewportState = &viewportState;
    createInfo.pRasterizationState = &rasterizationState;
    createInfo.pMultisampleState = &multisampleState;
    createInfo.pDepthStencilState = &depthStencilState;
    createInfo.pColorBlendState = &colorBlendState;
    createInfo.pDynamicState = &dynamicState;
    createInfo.layout = layout;
    createInfo.renderPass = renderPass;

    VkPipeline pipeline;
    VK_CHECK(vkCreateGraphicsPipelines(
        device, pipelineCache, 1, &createInfo, NULL, &pipeline));
    return pipeline;
}

VkShaderModule CreateShader(VkDevice device, u32 *byteCode, u32 length)
{
    VkShaderModuleCreateInfo createInfo = {
        VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO};
    createInfo.codeSize = length;
    createInfo.pCode = byteCode;

    VkShaderModule shaderModule = VK_NULL_HANDLE;
    VK_CHECK(vkCreateShaderModule(device, &createInfo, NULL, &shaderModule));

    return shaderModule;
}

struct GraphicsPipelineDescription
{
    u32 *vertexShaderByteCode;
    u32 vertexShaderLength;
    u32 *fragmentShaderByteCode;
    u32 fragmentShaderLength;
};

PipelineState CreateGraphicsPipeline(VkDevice device,
    VkPipelineCache pipelineCache, VkRenderPass renderPass,
    VkDescriptorPool descriptorPool, GraphicsPipelineDescription desc)
{
    // clang-format off
    VkDescriptorSetLayoutBinding bindings[] = {
        // binding, descriptorType, descriptorCount, stageFlags
        {0, VK_DESCRIPTOR_TYPE_STORAGE_BUFFER, 1, VK_SHADER_STAGE_VERTEX_BIT},
        {1, VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER, 1, VK_SHADER_STAGE_VERTEX_BIT},
        {2, VK_DESCRIPTOR_TYPE_STORAGE_BUFFER, 1, VK_SHADER_STAGE_VERTEX_BIT},
        {3, VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER, 1, VK_SHADER_STAGE_FRAGMENT_BIT},
        {4, VK_DESCRIPTOR_TYPE_STORAGE_BUFFER, 1, VK_SHADER_STAGE_FRAGMENT_BIT},
    };
    // clang-format on

    VkDescriptorSetLayout descriptorSetLayout =
        CreateDescriptorSetLayout(device, bindings, ARRAY_COUNT(bindings));

    VkPipelineLayout pipelineLayout =
        CreatePipelineLayout(device, descriptorSetLayout, sizeof(PushConstants),
            VK_SHADER_STAGE_VERTEX_BIT | VK_SHADER_STAGE_FRAGMENT_BIT);

    VkShaderModule vertexShader = CreateShader(
        device, desc.vertexShaderByteCode, desc.vertexShaderLength);
    VkShaderModule fragmentShader = CreateShader(
        device, desc.fragmentShaderByteCode, desc.fragmentShaderLength);

    VkPipeline pipeline = CreatePipeline(device, vertexShader, fragmentShader,
        pipelineLayout, renderPass, pipelineCache);

    VkDescriptorSet descriptorSet = AllocateDescriptorSet(
        device, descriptorPool, descriptorSetLayout);

    PipelineState result = {};
    result.layout = pipelineLayout;
    result.pipeline = pipeline;
    result.descriptorSet = descriptorSet;
    result.descriptorSetLayout = descriptorSetLayout;

    return result;
}

void writeDescriptorSet(VkDevice device, VkDescriptorSet descriptorSet,
    VkBuffer vertexBuffer, VkBuffer matrixBuffer, VkBuffer modelMatrixBuffer,
    VkBuffer materialBuffer)
{
    VkDescriptorBufferInfo vertexBufferInfo = {};
    vertexBufferInfo.buffer = vertexBuffer;
    vertexBufferInfo.range = VK_WHOLE_SIZE;

    VkDescriptorBufferInfo matrixBufferInfo = {};
    matrixBufferInfo.buffer = matrixBuffer;
    matrixBufferInfo.range = VK_WHOLE_SIZE;

    VkDescriptorBufferInfo modelMatrixBufferInfo = {};
    modelMatrixBufferInfo.buffer = modelMatrixBuffer;
    modelMatrixBufferInfo.range = VK_WHOLE_SIZE;

    VkDescriptorBufferInfo materialBufferInfo = {};
    materialBufferInfo.buffer = materialBuffer;
    materialBufferInfo.range = VK_WHOLE_SIZE;

    VkWriteDescriptorSet descriptorWrites[4] = {};
    {
        descriptorWrites[0].sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
        descriptorWrites[0].dstSet = descriptorSet;
        descriptorWrites[0].dstBinding = 0;
        descriptorWrites[0].descriptorType = VK_DESCRIPTOR_TYPE_STORAGE_BUFFER;
        descriptorWrites[0].descriptorCount = 1;
        descriptorWrites[0].pBufferInfo = &vertexBufferInfo;
    }
    {
        descriptorWrites[1].sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
        descriptorWrites[1].dstSet = descriptorSet;
        descriptorWrites[1].dstBinding = 1;
        descriptorWrites[1].descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
        descriptorWrites[1].descriptorCount = 1;
        descriptorWrites[1].pBufferInfo = &matrixBufferInfo;
    }
    {
        descriptorWrites[2].sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
        descriptorWrites[2].dstSet = descriptorSet;
        descriptorWrites[2].dstBinding = 2;
        descriptorWrites[2].descriptorType = VK_DESCRIPTOR_TYPE_STORAGE_BUFFER;
        descriptorWrites[2].descriptorCount = 1;
        descriptorWrites[2].pBufferInfo = &modelMatrixBufferInfo;
    }
    {
        descriptorWrites[3].sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
        descriptorWrites[3].dstSet = descriptorSet;
        descriptorWrites[3].dstBinding = 4;
        descriptorWrites[3].descriptorType = VK_DESCRIPTOR_TYPE_STORAGE_BUFFER;
        descriptorWrites[3].descriptorCount = 1;
        descriptorWrites[3].pBufferInfo = &materialBufferInfo;
    }

    vkUpdateDescriptorSets(
        device, ARRAY_COUNT(descriptorWrites), descriptorWrites, 0, NULL);
}

VkPipeline CreateComputePipeline(VkDevice device, VkPipelineCache pipelineCache,
    VkPipelineLayout layout, VkShaderModule shaderModule)
{
    VkPipeline pipeline = VK_NULL_HANDLE;

    VkComputePipelineCreateInfo computePipelineCreateInfo = {
        VK_STRUCTURE_TYPE_COMPUTE_PIPELINE_CREATE_INFO};

    computePipelineCreateInfo.stage.sType =
        VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
    computePipelineCreateInfo.stage.stage = VK_SHADER_STAGE_COMPUTE_BIT;
    computePipelineCreateInfo.stage.module = shaderModule;
    computePipelineCreateInfo.stage.pName = "main";
    computePipelineCreateInfo.layout = layout;

    VK_CHECK(vkCreateComputePipelines(
        device, pipelineCache, 1, &computePipelineCreateInfo, NULL, &pipeline));

    return pipeline;
}

PipelineState CreateComputePipeline(VkDevice device,
    VkPipelineCache pipelineCache, VkDescriptorPool descriptorPool,
    u32 *byteCode, u32 byteCodeLength)
{
    // clang-format off
    VkDescriptorSetLayoutBinding bindings[] = {
        // binding, descriptorType, descriptorCount, stageFlags
        {0, VK_DESCRIPTOR_TYPE_STORAGE_IMAGE, 1, VK_SHADER_STAGE_COMPUTE_BIT},
        {1, VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER, 1, VK_SHADER_STAGE_COMPUTE_BIT},
    };
    // clang-format on

    VkDescriptorSetLayout descriptorSetLayout = CreateDescriptorSetLayout(
        device, bindings, ARRAY_COUNT(bindings));

    VkPipelineLayout pipelineLayout =
        CreatePipelineLayout(device, descriptorSetLayout, 0, 0);

    VkShaderModule shader = CreateShader(device, byteCode, byteCodeLength);

    VkPipeline pipeline = CreateComputePipeline(
        device, pipelineCache, pipelineLayout, shader);

    VkDescriptorSet descriptorSet = AllocateDescriptorSet(
        device, descriptorPool, descriptorSetLayout);

    PipelineState result = {};
    result.layout = pipelineLayout;
    result.pipeline = pipeline;
    result.descriptorSet = descriptorSet;
    result.descriptorSetLayout = descriptorSetLayout;
    // TODO: Set flag to indicate this is a compute pipeline?

    return result;
}

void WriteComputePipelineDescriptorSets(VkDevice device,
    VkDescriptorSet descriptorSet, VkImageView inputImageView,
    VkSampler inputImageSampler, VkImageView outputImageView)
{
    VkDescriptorImageInfo outputImageInfo = {};
    outputImageInfo.imageView = outputImageView;
    outputImageInfo.imageLayout = VK_IMAGE_LAYOUT_GENERAL;

    VkDescriptorImageInfo inputImageInfo = {};
    inputImageInfo.imageView = inputImageView;
    inputImageInfo.imageLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
    inputImageInfo.sampler = inputImageSampler;

    VkWriteDescriptorSet descriptorWrites[2] = {};
    {
        descriptorWrites[0].sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
        descriptorWrites[0].dstSet = descriptorSet;
        descriptorWrites[0].dstBinding = 0;
        descriptorWrites[0].descriptorType = VK_DESCRIPTOR_TYPE_STORAGE_IMAGE;
        descriptorWrites[0].descriptorCount = 1;
        descriptorWrites[0].pImageInfo = &outputImageInfo;
    }
    {
        descriptorWrites[1].sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
        descriptorWrites[1].dstSet = descriptorSet;
        descriptorWrites[1].dstBinding = 1;
        descriptorWrites[1].descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
        descriptorWrites[1].descriptorCount = 1;
        descriptorWrites[1].pImageInfo = &inputImageInfo;
    }

    vkUpdateDescriptorSets(
        device, ARRAY_COUNT(descriptorWrites), descriptorWrites, 0, NULL);
}

void DispatchComputeShader(VkDevice device, VkCommandPool commandPool, VkQueue queue,
         VkPipeline pipeline, VkPipelineLayout layout, VkDescriptorSet descriptorSet,
         u32 groupCountX, u32 groupCountY, u32 groupCountZ)
{
    VkCommandBuffer commandBuffer =
        CreateTemporaryCommandBuffer(device, commandPool);

    vkCmdBindPipeline(commandBuffer, VK_PIPELINE_BIND_POINT_COMPUTE, pipeline);
    vkCmdBindDescriptorSets(commandBuffer, VK_PIPELINE_BIND_POINT_COMPUTE,
        layout, 0, 1, &descriptorSet, 0, NULL);

    vkCmdDispatch(commandBuffer, groupCountX, groupCountY, groupCountZ);

    SubmitTemporaryCommandBuffer(device, commandBuffer, commandPool, queue);
}

