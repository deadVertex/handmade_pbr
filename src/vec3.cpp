inline vec3 Vec3(f32 x, f32 y, f32 z)
{
    vec3 result = {x, y, z};
    return result;
}

inline vec3 Vec3(f32 x)
{
    vec3 result = {x, x, x};
    return result;
}

inline vec3 Vec3(vec2 v, f32 z)
{
    vec3 result = {v.x, v.y, z};
    return result;
}

inline vec3 operator-(vec3 a)
{
    vec3 result;
    result.x = -a.x;
    result.y = -a.y;
    result.z = -a.z;
    return result;
}

inline vec3 operator*(vec3 a, f32 b)
{
    vec3 result = {a.x * b, a.y * b, a.z * b};
    return result;
}

inline vec3 operator*(f32 a, vec3 b)
{
    vec3 result = b * a;
    return result;
}

inline vec3 operator+(vec3 a, vec3 b)
{
    vec3 result = {a.x + b.x, a.y + b.y, a.z + b.z};
    return result;
}

inline vec3& operator+=(vec3 &a, vec3 b)
{
    a = a + b;
    return a;
}

inline vec3 Lerp(vec3 a, vec3 b, f32 t)
{
    vec3 result = a * (1.0f - t) + b * t;
    return result;
}

inline f32 Dot(vec3 a, vec3 b)
{
    f32 result = a.x * b.x + a.y * b.y + a.z * b.z;
    return result;
}

inline f32 Length(vec3 v)
{
    f32 result = Sqrt(Dot(v, v));
    return result;
}

inline vec3 Cross(vec3 a, vec3 b)
{
    vec3 result;

    result.x = (a.y * b.z) - (a.z * b.y);
    result.y = (a.z * b.x) - (a.x * b.z);
    result.z = (a.x * b.y) - (a.y * b.x);

    return result;

}

inline vec3 Normalize(vec3 v)
{
    f32 length = Length(v);
    vec3 result = {};
    if (length > EPSILON)
    {
        result = v * (1.0f / length);
    }
    return result;
}

// NOTE: v must be a unit vector
// RETURNS: (azimuth, inclination)
inline vec2 ToSphericalCoordinates(vec3 v)
{
    // TODO: Do we always want these assertions compiled in?
    //Assert(LengthSq(v) - 1.0f <= EPSILON);

    //f32 r = 1.0f;
    f32 inc = Atan2(Sqrt(v.x * v.x + v.z * v.z), v.y);

    f32 azimuth = Atan2(v.z,v.x);

    return Vec2(azimuth, inc);
}

// https://en.wikipedia.org/wiki/List_of_common_coordinate_transformations#From_spherical_coordinates
// But we swap Z and Y axis to match OpenGL/Vulkan coordinate system
inline vec3 MapSphericalToCartesianCoordinates(vec2 sphereCoords)
{
    vec3 result = {};
    result.x = Sin(sphereCoords.y) * Cos(sphereCoords.x);
    result.z = Sin(sphereCoords.y) * Sin(sphereCoords.x);
    result.y = Cos(sphereCoords.y);
    return result;
}

inline vec2 MapToEquirectangular(vec2 sphereCoords)
{
    vec2 uv = {};
    if (sphereCoords.x < 0.0f)
    {
        sphereCoords.x += 2.0f * PI;
    }
    uv.x = sphereCoords.x / (2.0f * PI);
    uv.y = Cos(sphereCoords.y) * 0.5f + 0.5f;

    return uv;
}

// NOTE: This function could probably be done more efficiently, I've just done
// a direct inverse of the MapToEquirectangular function verified with some
// basic unit testing.
inline vec2 MapEquirectangularToSphereCoordinates(vec2 uv)
{
    f32 a = uv.y * 2.0f - 1.0f;
    f32 y = Acos(a);

    f32 b = uv.x * 2.0f * PI;
    if (b > PI)
    {
        b -= 2.0f * PI;
    }

    vec2 sphereCoords = Vec2(b, y);
    return sphereCoords;
}
