#version 450
#extension GL_GOOGLE_include_directive : require

#include "common.glsl"

struct Material
{
    float albedoR, albedoG, albedoB;
    float roughness;
};

layout(location = 0) out vec4 outputColor;

layout(location=0) in vec2 fragTextureCoords;
layout(location=2) in vec3 fragNormal;
layout(location=3) in vec3 fragWorldPosition;
layout(location=4) in flat vec3 fragCameraPosition;

layout(binding = 3) uniform samplerCube irradianceCubeMap;

layout(binding = 4) readonly buffer Materials
{
    Material materials[];
};

vec3 FresnelSchlickRoughness(float cosTheta, vec3 F0, float roughness)
{
    return F0 +
           (max(vec3(1.0 - roughness), F0) - F0) * pow(1.0 - cosTheta, 5.0);
}

void main()
{
    vec3 N = normalize(fragNormal);
    vec3 V = normalize(fragCameraPosition - fragWorldPosition);
    vec3 F0 = vec3(0.04); // Use same value for all dielectric materials
    vec3 albedo = vec3(materials[materialIndex].albedoR,
                       materials[materialIndex].albedoG,
                       materials[materialIndex].albedoB);
    float roughness = materials[materialIndex].roughness;

    // Fresnel
    float nDotV = max(dot(N, V), 0.0);
    vec3 kS = FresnelSchlickRoughness(nDotV, F0, roughness);
    vec3 kD = 1.0 - kS;

    vec3 irradiance = texture(irradianceCubeMap, N).rgb;
    vec3 diffuse = irradiance * albedo;

    vec3 radiance = kD * diffuse;

    outputColor = vec4(radiance, 1);
}
