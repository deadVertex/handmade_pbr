u32 findMemoryType(VkPhysicalDevice physicalDevice, u32 typeFilter,
    VkMemoryPropertyFlags properties)
{
    VkPhysicalDeviceMemoryProperties memoryProperties = {};
    vkGetPhysicalDeviceMemoryProperties(physicalDevice, &memoryProperties);

    u32 memoryIndex = U32_MAX;
    for (u32 i = 0; i < memoryProperties.memoryTypeCount; ++i)
    {
        if ((typeFilter & (1 << i)) &&
            (memoryProperties.memoryTypes[i].propertyFlags & properties) ==
                properties)
        {
            memoryIndex = i;
            break;
        }
    }

    // Check that we found a suitable memory type
    ASSERT_MACRO(memoryIndex != U32_MAX);

    return memoryIndex;
}


VkImage CreateImage(VkDevice device, u32 width, u32 height, VkFormat format,
    VkImageUsageFlags usage)
{
    VkImageCreateInfo createInfo = {VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO};
    createInfo.imageType = VK_IMAGE_TYPE_2D;
    createInfo.extent.width = width;
    createInfo.extent.height = height;
    createInfo.extent.depth = 1;
    createInfo.mipLevels = 1;
    createInfo.arrayLayers = 1;
    createInfo.format = format;
    createInfo.tiling = VK_IMAGE_TILING_OPTIMAL;
    createInfo.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
    createInfo.usage = usage;
    createInfo.sharingMode = VK_SHARING_MODE_EXCLUSIVE;
    createInfo.samples = VK_SAMPLE_COUNT_1_BIT;

    VkImage image = VK_NULL_HANDLE;
    VK_CHECK(vkCreateImage(device, &createInfo, NULL, &image));
    return image;
}

VkDeviceMemory AllocateAndBindMemoryForImage(
    VkDevice device, VkImage image, VkPhysicalDevice physicalDevice)
{
    // Choose which memory heap we will allocate this image from
    VkMemoryRequirements memoryRequirements = {};
    vkGetImageMemoryRequirements(device, image, &memoryRequirements);

    VkMemoryPropertyFlags memoryProperties =
        VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT;

    u32 memoryTypeIndex = findMemoryType(
        physicalDevice, memoryRequirements.memoryTypeBits, memoryProperties);

    // Allocate the memory
    VkMemoryAllocateInfo allocInfo = {VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO};
    allocInfo.allocationSize = memoryRequirements.size;
    allocInfo.memoryTypeIndex = memoryTypeIndex;

    VkDeviceMemory imageMemory = VK_NULL_HANDLE;
    VK_CHECK(vkAllocateMemory(device, &allocInfo, NULL, &imageMemory));

    // Bind the memory to our image
    vkBindImageMemory(device, image, imageMemory, 0);
    return imageMemory;
}

VkImageView CreateImageView(VkDevice device, VkImage image, VkFormat format,
    VkImageAspectFlags aspectFlags)
{
    VkImageViewCreateInfo viewInfo = {VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO};
    viewInfo.image = image;
    viewInfo.viewType = VK_IMAGE_VIEW_TYPE_2D;
    viewInfo.format = format;
    viewInfo.subresourceRange.aspectMask = aspectFlags;
    viewInfo.subresourceRange.baseMipLevel = 0;
    viewInfo.subresourceRange.levelCount = 1;
    viewInfo.subresourceRange.baseArrayLayer = 0;
    viewInfo.subresourceRange.layerCount = 1;

    VkImageView imageView = VK_NULL_HANDLE;
    VK_CHECK(vkCreateImageView(device, &viewInfo, NULL, &imageView));
    return imageView;
}

ImageState CreateImageState(VkDevice device, VkPhysicalDevice physicalDevice,
        u32 width, u32 height, VkFormat format, VkImageUsageFlags usage,
        VkImageAspectFlags aspectFlags)
{
    ImageState image = {};
    image.width = width;
    image.height = height;
    image.layerCount = 1;
    image.handle = CreateImage(device, width, height, format, usage);
    image.memory =
        AllocateAndBindMemoryForImage(device, image.handle, physicalDevice);
    image.view = CreateImageView(device, image.handle, format, aspectFlags);
    image.layout = VK_IMAGE_LAYOUT_UNDEFINED;

    return image;
}

void TransitionImageLayout(VkDevice device, VkImage image,
    VkImageLayout oldLayout, VkImageLayout newLayout, VkCommandPool commandPool,
    VkQueue queue)
{
    VkCommandBuffer tempCommandBuffer =
        CreateTemporaryCommandBuffer(device, commandPool);

    VkImageMemoryBarrier barrier = {VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER};
    barrier.oldLayout = oldLayout;
    barrier.newLayout = newLayout;
    barrier.srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
    barrier.dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
    barrier.image = image;
    barrier.subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
    barrier.subresourceRange.baseMipLevel = 0;
    barrier.subresourceRange.levelCount = 1;
    barrier.subresourceRange.baseArrayLayer = 0;
    barrier.subresourceRange.layerCount = VK_REMAINING_ARRAY_LAYERS;

    VkPipelineStageFlags sourceStage = 0;
    VkPipelineStageFlags destinationStage = 0;
    if (oldLayout == VK_IMAGE_LAYOUT_UNDEFINED && 
        newLayout == VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL)
    {
        barrier.srcAccessMask = 0;
        barrier.dstAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;

        sourceStage = VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT;
        destinationStage = VK_PIPELINE_STAGE_TRANSFER_BIT;
    }
    else if (oldLayout == VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL &&
             newLayout == VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL)
    {
        barrier.srcAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;
        barrier.dstAccessMask = VK_ACCESS_SHADER_READ_BIT;

        sourceStage = VK_PIPELINE_STAGE_TRANSFER_BIT;
        destinationStage = VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT;
    }
    else if (oldLayout == VK_IMAGE_LAYOUT_UNDEFINED &&
             newLayout == VK_IMAGE_LAYOUT_GENERAL)
    {
        // FIXME: Assumption that LAYOUT_GENERAL means we only want shader
        // write access
        barrier.srcAccessMask = 0;
        barrier.dstAccessMask = VK_ACCESS_SHADER_WRITE_BIT;

        sourceStage = VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT;
        destinationStage = VK_PIPELINE_STAGE_COMPUTE_SHADER_BIT;
    }
    else if (oldLayout == VK_IMAGE_LAYOUT_GENERAL &&
             newLayout == VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL)
    {
        barrier.srcAccessMask = VK_ACCESS_SHADER_WRITE_BIT;
        barrier.dstAccessMask = VK_ACCESS_SHADER_READ_BIT;

        sourceStage = VK_PIPELINE_STAGE_COMPUTE_SHADER_BIT;
        destinationStage = VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT;
    }
    else
    {
        INVALID_CODE_PATH();
    }

    vkCmdPipelineBarrier(tempCommandBuffer, sourceStage, destinationStage, 0, 0,
        NULL, 0, NULL, 1, &barrier);
    SubmitTemporaryCommandBuffer(device, tempCommandBuffer, commandPool, queue);
}

void CopyBufferToImage(VkDevice device, VkCommandPool commandPool,
    VkQueue queue, VkBuffer buffer, VkImage image, u32 width, u32 height,
    u32 layerCount, VkDeviceSize offset)
{
    VkCommandBuffer commandBuffer =
        CreateTemporaryCommandBuffer(device, commandPool);

    VkBufferImageCopy imageCopy = {};
    imageCopy.bufferOffset = offset;
    imageCopy.imageSubresource.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
    imageCopy.imageSubresource.mipLevel = 0;
    imageCopy.imageSubresource.baseArrayLayer = 0;
    imageCopy.imageSubresource.layerCount = layerCount;

    imageCopy.imageExtent.width = width;
    imageCopy.imageExtent.height = height;
    imageCopy.imageExtent.depth = 1;

    vkCmdCopyBufferToImage(commandBuffer, buffer, image,
        VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, 1, &imageCopy);

    SubmitTemporaryCommandBuffer(device ,commandBuffer, commandPool, queue);
}


void *BeginTextureUpload(VulkanRenderer *renderer, u32 pixelDataSize)
{
    // Allocate space in the texture upload buffer for the pixels
    ResetStackAllocator(&renderer->textureUploadAllocator);
    void *dst = AllocateBytes(&renderer->textureUploadAllocator, pixelDataSize);
    ASSERT_MACRO(dst != NULL);
    return dst;
}

void EndTextureUpload(VulkanRenderer *renderer, ImageState *image)
{
    // TODO: Only create and submit 1 temporary command buffer
    // Transition the image layout so we can transfer data from upload buffer to it
    TransitionImageLayout(renderer->device, image->handle,
        VK_IMAGE_LAYOUT_UNDEFINED, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
        renderer->commandPool, renderer->graphicsQueue);

    // Copy from textureUploadBuffer to image
    CopyBufferToImage(renderer->device, renderer->commandPool,
        renderer->graphicsQueue, renderer->textureUploadBuffer.handle,
        image->handle, image->width, image->height, image->layerCount, 0);

    // Transition image layout to shader read only optimal
    TransitionImageLayout(renderer->device, image->handle,
        VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
        VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL, renderer->commandPool,
        renderer->graphicsQueue);
    image->layout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
}

void UploadPixelDataToGpu(VulkanRenderer *renderer, ImageState *image,
    void *pixelData, u32 pixelDataSize)
{
    void *dst = BeginTextureUpload(renderer, pixelDataSize);
    COPY_MEMORY(dst, pixelData, pixelDataSize);
    EndTextureUpload(renderer, image);
}

VkImage CreateCubeMap(VkDevice device, u32 width, u32 height, VkFormat format,
    VkImageUsageFlags usage)
{
    VkImageCreateInfo createInfo = {VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO};
    createInfo.imageType = VK_IMAGE_TYPE_2D;
    createInfo.extent.width = width;
    createInfo.extent.height = height;
    createInfo.extent.depth = 1;
    createInfo.mipLevels = 1;
    createInfo.arrayLayers = 6;
    createInfo.format = format;
    createInfo.tiling = VK_IMAGE_TILING_OPTIMAL;
    createInfo.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
    createInfo.usage = usage;
    createInfo.sharingMode = VK_SHARING_MODE_EXCLUSIVE;
    createInfo.samples = VK_SAMPLE_COUNT_1_BIT;
    createInfo.flags = VK_IMAGE_CREATE_CUBE_COMPATIBLE_BIT;

    VkImage image = VK_NULL_HANDLE;
    VK_CHECK(vkCreateImage(device, &createInfo, NULL, &image));
    return image;
}

VkImageView CreateCubeMapImageView(VkDevice device, VkImage image,
    VkFormat format, VkImageAspectFlags aspectFlags)
{
    VkImageViewCreateInfo viewInfo = {VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO};
    viewInfo.image = image;
    viewInfo.viewType = VK_IMAGE_VIEW_TYPE_CUBE;
    viewInfo.format = format;
    viewInfo.subresourceRange.aspectMask = aspectFlags;
    viewInfo.subresourceRange.baseMipLevel = 0;
    viewInfo.subresourceRange.levelCount = 1;
    viewInfo.subresourceRange.baseArrayLayer = 0;
    viewInfo.subresourceRange.layerCount = 6;

    VkImageView imageView = VK_NULL_HANDLE;
    VK_CHECK(vkCreateImageView(device, &viewInfo, NULL, &imageView));
    return imageView;
}

ImageState CreateCubeMapImageState(VkDevice device,
    VkPhysicalDevice physicalDevice, u32 width, u32 height, VkFormat format,
    VkImageUsageFlags usage,
    VkImageAspectFlags aspectFlags)
{
    ImageState image = {};
    image.width = width;
    image.height = height;
    image.layerCount = 6;
    image.handle = CreateCubeMap(device, width, height, format, usage);
    image.memory =
        AllocateAndBindMemoryForImage(device, image.handle, physicalDevice);
    image.view =
        CreateCubeMapImageView(device, image.handle, format, aspectFlags);
    image.layout = VK_IMAGE_LAYOUT_UNDEFINED;

    return image;
}

