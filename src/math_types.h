#pragma once

#include <cfloat>

#define PI 3.14159265359f
#define EPSILON FLT_EPSILON

union vec2
{
    struct
    {
        f32 x;
        f32 y;
    };

    struct
    {
        f32 u;
        f32 v;
    };

    f32 data[2];
};

union vec3
{
    struct
    {
        f32 x;
        f32 y;
        f32 z;
    };

    struct
    {
        f32 r;
        f32 g;
        f32 b;
    };

    struct
    {
        vec2 xy;
        f32 __unused0;
    };

    f32 data[3];
};

union vec4
{
    struct
    {
        f32 r;
        f32 g;
        f32 b;
        f32 a;
    };

    struct
    {
        f32 x;
        f32 y;
        f32 z;
        f32 w;
    };

    struct
    {
        vec3 xyz;
        f32 __unused0;
    };

    struct
    {
        vec3 rgb;
        f32 __unused1;
    };

    struct
    {
        vec3 v;
        f32 s;
    };

    f32 data[4];
};

union mat4
{
    vec4 columns[4];
    f32 data[16];
};

typedef vec4 quat;
