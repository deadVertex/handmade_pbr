#pragma once

enum
{
    Pipeline_Default,
    Pipeline_Green,
    Pipeline_Lighting,
    Pipeline_Skybox,
    Pipeline_PostProcessing,
    Pipeline_ConvertEquirectangular,
    Pipeline_ComputeIrradiance,
    Pipeline_PbrMaterial,
};

enum
{
    Texture_Pink,
    Texture_CubeMapTest,
    Texture_Equirectangular,
    Texture_CubeMapFromFile,
    Texture_IrradianceCubeMap,
    Texture_HdrRenderPass, // NOTE: Magic value
};

enum
{
    RenderPass_Scene,
    RenderPass_PostProcessing,
};

struct Camera
{
    vec3 position;
    vec3 rotation;
};

enum
{
    RendererBackendCommandType_CreateImage,
    RendererBackendCommandType_UpdateImage,
    RendererBackendCommandType_CreateGraphicsPipeline,
    RendererBackendCommandType_UpdateGraphicsPipelineResources,
    RendererBackendCommandType_CreateCubeMap,
    RendererBackendCommandType_UpdateCubeMap,
    RendererBackendCommandType_CreateComputePipeline,
    RendererBackendCommandType_UpdateComputePipelineResources,
    RendererBackendCommandType_DispatchComputePipeline,
    RendererBackendCommandType_CreateRenderPass,
    RendererBackendCommandType_DrawMesh,
};

struct CreateGraphicsPipelineCommand
{
    u32 id;
    u32 renderPass;
    void *vertexShaderCode;
    u32 vertexShaderCodeLength;
    void *fragmentShaderCode;
    u32 fragmentShaderCodeLength;
};


enum
{
    CreateCubeMapFlag_None = 0x0,
    CreateCubeMapFlag_ComputeShaderOutput = 0x1,
};

struct CreateCubeMapCommand
{
    u32 id;
    u32 width;
    u32 flags;
};

struct UpdateCubeMapCommand
{
    u32 id;
    u32 faceNumBytes;
    void *pixels[6];
};

struct CreateComputePipelineCommand
{
    u32 id;
    void *shaderCode;
    u32 shaderCodeLength;
};

enum
{
    PipelineResourceType_Texture,
};

struct PipelineResource
{
    u32 type;
    u32 id;
};

#define MAX_PIPELINE_RESOURCES 4

struct UpdateComputePipelineResourcesCommand
{
    u32 id;
    PipelineResource resources[MAX_PIPELINE_RESOURCES];
    u32 resourceCount;
};

struct DispatchComputePipelineCommand
{
    u32 id;
    u32 groupCountX;
    u32 groupCountY;
    u32 groupCountZ;
};

struct CreateRenderPassCommand
{
    u32 id;
    u32 width;
    u32 height;
    u32 texture;
};

struct BackendDrawMeshCommand
{
    u32 renderPass;
    u32 pipeline;
    u32 vertexIndexOffset;
    u32 modelMatrixIndex;
    u32 indexOffset;
    u32 indexCount;
};

struct RendererBackendCommand
{
    u32 type;
    union
    {
        struct
        {
            u32 id;
            u32 width;
            u32 height;
        } createImage;

        struct
        {
            u32 id;
            u32 length;
            void *pixels;
        } updateImage;

        CreateGraphicsPipelineCommand createGraphicsPipeline;

        struct
        {
            u32 id;
        } updateGraphicsPipelineResources;

        CreateCubeMapCommand createCubeMap;
        UpdateCubeMapCommand updateCubeMap;

        CreateComputePipelineCommand createComputePipeline;

        UpdateComputePipelineResourcesCommand updateComputePipelineResources;

        DispatchComputePipelineCommand dispatchComputePipeline;

        CreateRenderPassCommand createRenderPass;

        BackendDrawMeshCommand drawMesh;
    };
};

typedef void OnFrameBufferResizeFunction(void *, u32, u32);
typedef void ProcessCommandsFunction(void *, RendererBackendCommand *, u32);

struct RendererBackend
{
    void *renderer;
    OnFrameBufferResizeFunction *onFramebufferResize;
    ProcessCommandsFunction *processCommands;
};
