#include <cstring>
#include <cassert>

#include "core_macros.h"
#include "image.h"

#pragma warning(disable : 4100)

#include "cube_map.h"
#include "renderer_backend.h"
#include "renderer_frontend.h"
#include "renderer_frontend.cpp"

TEST_GROUP(RendererFrontend);

b32 onFramebufferResizeCalled = false;
b32 onRenderFrameCalled = false;
void *backendRenderer = NULL;

RendererBackendCommand backendCmds[4];
u32 backendCmdCount = 0;

TEST_SETUP(RendererFrontend)
{
    onFramebufferResizeCalled = false;
    onRenderFrameCalled = false;
    backendRenderer = NULL;

    backendCmdCount = 0;
    memset(backendCmds, 0, sizeof(backendCmds));
}

TEST_TEAR_DOWN(RendererFrontend)
{
}

/*
struct RenderEffect
{
    ShaderDef shaders[2];
    // Some description of descriptorLayout
    // PushConstants description
    // fillMode, cullMode, depthTest, depthWrite,
    // Resource dependencies
};
*/

void StubOnFramebufferResize(void *renderer, u32 newWidth, u32 newHeight)
{
    (void)newWidth;
    (void)newHeight;

    backendRenderer = renderer;
    onFramebufferResizeCalled = true;
}

void StubProcessCommands(void *renderer, RendererBackendCommand *cmds, u32 count)
{
    backendRenderer = renderer;
    backendCmdCount = count;
    if (cmds != NULL && count <= ARRAY_COUNT(backendCmds))
    {
        memcpy(backendCmds, cmds, count * sizeof(RendererBackendCommand));
    }
}

TEST(RendererFrontend, CallsBackendFunctions)
{
    const char *expectedUserData = "user data";

    RendererBackend backend = {};
    backend.renderer = (void *)expectedUserData;
    backend.onFramebufferResize = &StubOnFramebufferResize;

    RendererFrontend frontend = CreateRendererFrontend(backend);
    OnFramebufferResize(&frontend, 32, 32);
    TEST_ASSERT_TRUE(onFramebufferResizeCalled);
    TEST_ASSERT_EQUAL_PTR(expectedUserData, backendRenderer);
}

TEST(RendererFrontend, RegisterMesh)
{
    RendererBackend backend = {};
    backend.onFramebufferResize = &StubOnFramebufferResize;

    // Given a renderer
    RendererFrontend renderer = CreateRendererFrontend(backend);

    // When we register mesh data to an id
    RegisterMesh(&renderer, Mesh_Triangle, 1, 2, 3);

    // Then we can retrieve it by that id
    Mesh *mesh = FindMesh(&renderer, Mesh_Triangle);
    TEST_ASSERT_NOT_NULL(mesh);
    TEST_ASSERT_EQUAL(1, mesh->vertexIndexOffset);
    TEST_ASSERT_EQUAL(2, mesh->indexOffset);
    TEST_ASSERT_EQUAL(3, mesh->indexCount);
}

TEST(RendererFrontend, RegisterMultipleMeshes)
{
    RendererBackend backend = {};
    backend.onFramebufferResize = &StubOnFramebufferResize;

    // Given a renderer
    RendererFrontend renderer = CreateRendererFrontend(backend);

    // When we register multiple meshes
    RegisterMesh(&renderer, Mesh_Triangle, 1, 2, 3);
    RegisterMesh(&renderer, Mesh_Quad, 4, 5, 6);

    // Then we can retrieve them by that id
    Mesh *triangle = FindMesh(&renderer, Mesh_Triangle);
    Mesh *quad = FindMesh(&renderer, Mesh_Quad);
    TEST_ASSERT_NOT_NULL(triangle);
    TEST_ASSERT_NOT_NULL(quad);
    TEST_ASSERT_EQUAL(3, triangle->indexCount);
    TEST_ASSERT_EQUAL(6, quad->indexCount);
}

TEST(RendererFrontend, RegisterTexture)
{
    RendererBackend backend = {};
    backend.onFramebufferResize = &StubOnFramebufferResize;
    backend.processCommands = &StubProcessCommands;

    // Given a renderer
    RendererFrontend renderer = CreateRendererFrontend(backend);

    // When we register a texture
    u32 imageId = 12;
    HdrImage image = {};
    image.width = 2;
    image.height = 4;
    RegisterTexture(&renderer, imageId, image);

    // Then we generate separate create and update backend commands
    TEST_ASSERT_EQUAL(2, backendCmdCount);
    TEST_ASSERT_EQUAL(RendererBackendCommandType_CreateImage, backendCmds[0].type);
    TEST_ASSERT_EQUAL(imageId, backendCmds[0].createImage.id);
    TEST_ASSERT_EQUAL(2, backendCmds[0].createImage.width);
    TEST_ASSERT_EQUAL(4, backendCmds[0].createImage.height);

    TEST_ASSERT_EQUAL(RendererBackendCommandType_UpdateImage, backendCmds[1].type);
    TEST_ASSERT_EQUAL(imageId, backendCmds[1].updateImage.id);
    TEST_ASSERT_EQUAL(sizeof(f32) * 4 * 2 * 4, backendCmds[1].updateImage.length);
}

#if 0
TEST(RendererFrontend, TranslateDrawCmds)
{
    // Given a renderer with multiple meshes
    RendererBackend backend = {};
    RendererFrontend renderer = CreateRendererFrontend(backend);
    RegisterMesh(&renderer, Mesh_Triangle, 1, 2, 3);
    RegisterMesh(&renderer, Mesh_Quad, 4, 5, 6);

    // When we provide DrawMeshCommands
    u32 modelMatrixIndices[] = {1, 2};
    u32 pipelines[] = {3, 4};
    u32 renderPass = 5;
    DrawMeshCommand cmds[] = {
        {Mesh_Triangle, modelMatrixIndices[0], pipelines[0], renderPass},
        {Mesh_Quad, modelMatrixIndices[1], pipelines[1], renderPass},
    };

    RendererBackendCommand outputCmds[8] = {};
    u32 count = TranslateCommands(
        cmds, ARRAY_COUNT(cmds), outputCmds, ARRAY_COUNT(outputCmds));

    // Then it produces RendererBackendCommands
    TEST_ASSERT_EQUAL(RendererBackendCommandType_DrawMesh, outputCmds[0].type);
    TEST_ASSERT_EQUAL(renderPass, outputCmds[0].drawMesh.renderPass);
}
#endif

TEST_GROUP_RUNNER(RendererFrontend)
{
    RUN_TEST_CASE(RendererFrontend, CallsBackendFunctions);
    RUN_TEST_CASE(RendererFrontend, RegisterMesh);
    RUN_TEST_CASE(RendererFrontend, RegisterMultipleMeshes);
    RUN_TEST_CASE(RendererFrontend, RegisterTexture);
}
