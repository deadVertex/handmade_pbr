#version 450
#extension GL_GOOGLE_include_directive : require

#include "common.glsl"

layout(location = 0) out vec4 outputColor;

layout(binding = 3) uniform sampler2D albedoTexture;

layout(location=0) in vec2 fragTextureCoords;
layout(location=2) in vec3 fragNormal;

void main()
{
    vec3 textureColor = texture(albedoTexture, fragTextureCoords).rgb;
    vec3 normalColor = 0.5 * normalize(fragNormal) + 0.5;
    outputColor = vec4(textureColor, 1);
}
