#version 450
#extension GL_GOOGLE_include_directive : require

#include "common.glsl"
#include "common_vert.glsl"

void main()
{
    mat4 viewMatrix = viewMatrices[SKYBOX_CAMERA_INDEX];
    mat4 projectionMatrix = projectionMatrices[SKYBOX_CAMERA_INDEX];
    mat4 modelMatrix = modelMatrices[modelMatrixIndex];

    Vertex vertex = vertices[gl_VertexIndex + vertexIndexOffset];

    vec3 position = vec3(vertex.px, vertex.py, vertex.pz);
    vec2 textureCoords = vec2(vertex.tu, vertex.tv);

    fragTextureCoords = textureCoords;
    fragLocalPosition = position;

    gl_Position =
        projectionMatrix * viewMatrix * modelMatrix * vec4(position, 1.0);
}
