@echo off

REM Build config options
set BUILD_ASSET_LOADER=0

REM GLFW 3.3.7 (Visual Studio 2022 MT config)
set glfw_include_path="..\external\glfw-3.3.7\include"
set glfw_libs="..\external\glfw-3.3.7\lib-vc2022\glfw3_mt.lib" user32.lib gdi32.lib shell32.lib

REM Vulkan SDK (environment variable is setup by the SDK installer)
set vulkan_include_path="%VULKAN_SDK%\Include"
set vulkan_lib="%VULKAN_SDK%\Lib\vulkan-1.lib"

REM See build_unit_tests.bat for notes on compiler warnings

set base_compiler_flags=-nologo -MT -Gm- -GR- -EHa -FC -Z7
set compiler_flags=%base_compiler_flags% -Od -Wall -WX ^
    -wd4201 -wd4820 -wd4514 -wd5045 -wd4061 -wd5246 -wd5219 -wd4711 -wd4710 ^
    -wd4100 ^
    -I %glfw_include_path% ^
    -I %vulkan_include_path%

set linker_flags=-opt:ref -incremental:no

REM All files are created in the build directory
IF NOT EXIST build mkdir build
pushd build

REM Build asset loader library
if %BUILD_ASSET_LOADER%==1 (
    REM Build miniz library
    cl %base_compiler_flags% -O2 -I..\external\tinyexr -c ../external/tinyexr/miniz.c

    REM Build asset loader library
    REM NOTE: -DTINYEXR_USE_THREAD=1 uses std::thread to create threads!
    cl %base_compiler_flags% -O2 -I ../src -I ..\external\tinyexr -DTINYEXR_USE_THREAD=1 -c ../src/asset_loader/asset_loader.cpp
)

REM Monitor build time
ctime -begin ..\main.ctm

REM Compile and link our application
cl %compiler_flags% ..\src\main.cpp -link %linker_flags% ^
    asset_loader.obj ^
    miniz.obj ^
    %glfw_libs% ^
    %vulkan_lib%

set last_error=%ERRORLEVEL%

ctime -end ..\main.ctm %last_error%

popd
exit %last_error%
