inline vec2 Vec2(f32 x, f32 y)
{
    vec2 result = { x, y };
    return result;
}

inline vec2 Vec2(f32 x)
{
    vec2 result = { x, x };
    return result;
}

inline vec2 operator-(vec2 a)
{
    vec2 result = {-a.x, -a.y};
    return result;
}

inline vec2 operator+(vec2 a, vec2 b)
{
    vec2 result = {a.x + b.x, a.y + b.y};
    return result;
}

inline vec2 operator*(vec2 a, f32 b)
{
    vec2 result = {a.x * b, a.y * b};
    return result;
}

inline vec2 operator*(f32 a, vec2 b)
{
    vec2 result = b * a;
    return result;
}
