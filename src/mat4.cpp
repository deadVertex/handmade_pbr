inline mat4 Identity()
{
    mat4 result = {};
    result.data[0] = 1.0f;
    result.data[5] = 1.0f;
    result.data[10] = 1.0f;
    result.data[15] = 1.0f;

    return result;
};

inline mat4 Scale(vec3 scale)
{
    mat4 result = {};
    result.data[0] = scale.x;
    result.data[5] = scale.y;
    result.data[10] = scale.z;
    result.data[15] = 1.0f;

    return result;
}

inline mat4 Translate(vec3 translation)
{
    mat4 result = Identity();
    result.data[12] = translation.x;
    result.data[13] = translation.y;
    result.data[14] = translation.z;

    return result;
}

inline mat4 RotateX(f32 radians)
{
    mat4 result = Identity();

    result.columns[1].y = Cos(radians);
    result.columns[1].z = Sin(radians);
    result.columns[2].y = -Sin(radians);
    result.columns[2].z = Cos(radians);

    return result;
}

inline mat4 RotateY(f32 radians)
{
    mat4 result = Identity();

    result.columns[0].x = Cos(radians);
    result.columns[0].z = -Sin(radians);
    result.columns[2].x = Sin(radians);
    result.columns[2].z = Cos(radians);

    return result;
}

inline mat4 RotateZ(f32 radians)
{
    mat4 result = Identity();

    result.columns[0].x = Cos(radians);
    result.columns[0].y = Sin(radians);
    result.columns[1].x = -Sin(radians);
    result.columns[1].y = Cos(radians);

    return result;
}

inline mat4 Perspective(f32 fovy, f32 aspect, f32 zNear, f32 zFar)
{
    f32 radians = Radians(fovy);
    f32 scale = Tan(radians * 0.5f) * zNear;

    f32 right = aspect * scale;
    f32 left = -right;

    f32 top = scale;
    f32 bottom = -scale;

    mat4 result = {};
    result.columns[0].x = (2.0f * zNear) / (right - left);
    result.columns[1].y = (2.0f * zNear) / (top - bottom);
    result.columns[2].x = (right + left) / (right - left);
    result.columns[2].y = (top + bottom) / (top - bottom);
    result.columns[2].z = -(zFar + zNear) / (zFar - zNear);
    result.columns[2].w = -1.0f;
    result.columns[3].z = -(2.0f * zFar * zNear) / (zFar - zNear);

    return result;
}

inline mat4 operator*(mat4 a, mat4 b)
{
    mat4 result = {};

    for (u32 i = 0; i < 4; ++i)
    {
        // clang-format off
        vec4 row = Vec4(a.columns[0].data[i],
                        a.columns[1].data[i],
                        a.columns[2].data[i],
                        a.columns[3].data[i]);

        result.columns[0].data[i] = Dot(row, b.columns[0]);
        result.columns[1].data[i] = Dot(row, b.columns[1]);
        result.columns[2].data[i] = Dot(row, b.columns[2]);
        result.columns[3].data[i] = Dot(row, b.columns[3]);
        // clang-format on
    }

    return result;
}

inline vec4 operator*(mat4 a, vec4 b)
{
    vec4 result = {};

    for (u32 i = 0; i < 4; ++i)
    {
        // clang-format off
        vec4 row = Vec4(a.columns[0].data[i],
                        a.columns[1].data[i],
                        a.columns[2].data[i],
                        a.columns[3].data[i]);

        result.data[i] = Dot(row, b);
        // clang-format on
    }

    return result;
}

inline vec3 TransformVector(vec3 d, mat4 m)
{
    vec4 v = m * Vec4(d, 0.0);
    return v.xyz;
}

inline vec3 TransformPoint(vec3 p, mat4 m)
{
    vec4 v = m * Vec4(p, 1.0);
    return v.xyz;
}
