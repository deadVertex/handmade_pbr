#pragma once

//! Maximum limit for the number of GPUs queried on startup
#define MAX_SUPPORTED_GPUS 16

//! Maximum number of physical device queue families which will be queried
#define MAX_SUPPORTED_QUEUE_FAMILIES 64

//! Maximum limit on the number of extensions that can be requested when
//creating an instance
#define MAX_SUPPORTED_EXTENSIONS 16

//! Maximum number of images that can be present in a swapchain
#define MAX_SWAPCHAIN_IMAGES 2

//! Maxium number of vulkan descriptor sets that can be allocated
#define MAX_DESCRIPTOR_SETS 64

#define MAX_PIPELINES 64
#define MAX_TEXTURES 64
#define MAX_RENDER_PASSES 4

#define MAX_UNIFORM_BUFFER_DESCRIPTORS 64
#define MAX_STORAGE_BUFFER_DESCRIPTORS 64

#define VERTEX_BUFFER_SIZE KILOBYTES(64)
#define INDEX_BUFFER_SIZE KILOBYTES(64)
#define MODEL_MATRIX_BUFFER_SIZE KILOBYTES(1)
#define TEXTURE_UPLOAD_BUFFER_SIZE MEGABYTES(256)
#define MATERIAL_BUFFER_SIZE KILOBYTES(1)

//! Stores all of the resources which are associated with a VkBuffer object
struct BufferState
{
    VkBuffer handle;
    VkDeviceMemory memory;
    void *data;
    u64 capacity;
};

//! Stores all of the resources which are associated with a VkImage object
struct ImageState
{
    u32 id;
    VkImage handle;
    VkDeviceMemory memory;
    VkImageView view;
    u32 width;
    u32 height;
    u32 layerCount;
    u32 layout;
};

//! Stores all of the vulkan resources which are bound to a VkSwapchainKHR object
struct SwapchainState
{
    VkSwapchainKHR handle;
    VkImage images[MAX_SWAPCHAIN_IMAGES];
    VkImageView imageViews[MAX_SWAPCHAIN_IMAGES];
    ImageState depthImages[MAX_SWAPCHAIN_IMAGES];
    VkFramebuffer framebuffers[MAX_SWAPCHAIN_IMAGES];
    u32 imageWidth;
    u32 imageHeight;
    u32 imageCount;
    VkFormat imageFormat;
};

//! Stores all of the vulkan resources and id required for a RenderPass
struct RenderPassState
{
    u32 id;
    VkRenderPass handle;
    ImageState color;
    ImageState depth;
    VkFramebuffer framebuffer;
    u32 textureId;
    u32 width;
    u32 height;
    VkFormat format;
};

struct RenderPassDescription
{
    u32 width;
    u32 height;
    u32 textureId;
};

//! Stores all of the resources which are associated with a VkPipeline object
struct PipelineState
{
    u32 id;
    VkPipelineLayout layout;
    VkPipeline pipeline;
    VkDescriptorSet descriptorSet;
    VkDescriptorSetLayout descriptorSetLayout;

    u32 boundTexture;
};

//! Responsible for storing all of our vulkan state
struct VulkanRenderer
{
    VkInstance instance;
    VkPhysicalDevice physicalDevice;
    VkDevice device;
    VkSurfaceKHR surface;
    SwapchainState swapchainState;
    VkSemaphore acquireSemaphore;
    VkSemaphore releaseSemaphore;
    VkDescriptorPool descriptorPool;
    VkCommandPool commandPool;
    VkCommandBuffer commandBuffer;
    VkQueue graphicsQueue;
    VkQueue presentQueue;

    BufferState vertexBuffer;
    BufferState indexBuffer;
    BufferState matrixBuffer;
    BufferState modelMatrixBuffer;
    BufferState textureUploadBuffer;
    BufferState materialBuffer;
    VkRenderPass renderPass;
    VkPipelineCache pipelineCache;
    VkSampler defaultSampler;

    StackAllocator vertexAllocator;
    StackAllocator indexAllocator;
    StackAllocator textureUploadAllocator;

    PipelineState pipelines[MAX_PIPELINES];
    u32 pipelineCount;

    ImageState textures[MAX_TEXTURES];
    u32 textureCount;

    RenderPassState renderPasses[MAX_RENDER_PASSES];
    u32 renderPassCount;

    VkShaderModule irradianceComputeShader;
};

//! Create and initialize a vulkan renderer
VulkanRenderer createVulkanRenderer(GLFWwindow *window);

//! Draw triangle to the screen using the vulkan renderer
void renderFrame(VulkanRenderer *renderer, Camera camera);

//! Helper macro for asserting that a Vulkan API call returned VK_SUCCESS
#define VK_CHECK(RESULT) ASSERT_MACRO(RESULT == VK_SUCCESS)

