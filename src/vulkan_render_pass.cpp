VkFramebuffer CreateFramebuffer(VkDevice device, VkRenderPass renderPass,
    VkImageView *attachments, u32 attachmentCount, u32 width, u32 height)
{
    VkFramebufferCreateInfo createInfo = {
        VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO};
    createInfo.renderPass = renderPass;
    createInfo.attachmentCount = attachmentCount;
    createInfo.pAttachments = attachments;
    createInfo.width = width;
    createInfo.height = height;
    createInfo.layers = 1;

    VkFramebuffer framebuffer;
    VK_CHECK(vkCreateFramebuffer(device, &createInfo, NULL, &framebuffer));
    return framebuffer;
}

VkRenderPass CreateRenderPass(
    VkDevice device, VkFormat colorFormat, VkImageLayout colorLayout)
{
    // NOTE: Using v1 version since not using any extensions that require v2
    VkAttachmentDescription attachments[2] = {};
    u32 attachmentCount = 2;

    // Color attachment
    attachments[0].format = colorFormat;
    attachments[0].samples = VK_SAMPLE_COUNT_1_BIT;
    attachments[0].loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR;
    attachments[0].storeOp = VK_ATTACHMENT_STORE_OP_STORE;
    attachments[0].stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
    attachments[0].stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
    attachments[0].initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
    attachments[0].finalLayout = colorLayout;

    // Depth attachment
    attachments[1].format = VK_FORMAT_D32_SFLOAT;
    attachments[1].samples = VK_SAMPLE_COUNT_1_BIT;
    attachments[1].loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR;
    attachments[1].storeOp = VK_ATTACHMENT_STORE_OP_STORE;
    attachments[1].stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
    attachments[1].stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
    attachments[1].initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
    attachments[1].finalLayout =
        VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL;

    VkAttachmentReference colorAttachmentRef = {};
    colorAttachmentRef.attachment = 0;
    colorAttachmentRef.layout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;

    VkAttachmentReference depthAttachmentRef = {};
    depthAttachmentRef.attachment = 1;
    depthAttachmentRef.layout =
        VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL;

    VkSubpassDescription subpass = {};
    subpass.pipelineBindPoint = VK_PIPELINE_BIND_POINT_GRAPHICS;
    subpass.colorAttachmentCount = 1;
    subpass.pColorAttachments = &colorAttachmentRef;
    subpass.pDepthStencilAttachment = &depthAttachmentRef;

    VkRenderPassCreateInfo createInfo = {
        VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO};
    createInfo.attachmentCount = attachmentCount;
    createInfo.pAttachments = attachments;
    createInfo.subpassCount = 1;
    createInfo.pSubpasses = &subpass;

    VkRenderPass renderPass = VK_NULL_HANDLE;
    VK_CHECK(vkCreateRenderPass(device, &createInfo, NULL, &renderPass));
    return renderPass;
}

struct CreateRenderPassResult
{
    ImageState colorTexture;
    ImageState depthTexture;
    RenderPassState renderPass;
};

CreateRenderPassResult CreateRenderPass(
    VulkanRenderer *renderer, u32 width, u32 height, u32 texture)
{
    // Create render pass
    VkRenderPass handle =
        CreateRenderPass(renderer->device, VK_FORMAT_R32G32B32A32_SFLOAT,
            VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL);

    // Create color attachment
    ImageState color = CreateImageState(renderer->device,
        renderer->physicalDevice, width, height, VK_FORMAT_R32G32B32A32_SFLOAT,
        VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT | VK_IMAGE_USAGE_SAMPLED_BIT,
        VK_IMAGE_ASPECT_COLOR_BIT);

    // Optionally create depth attachment
    ImageState depth = CreateImageState(renderer->device,
        renderer->physicalDevice, width, height, VK_FORMAT_D32_SFLOAT,
        VK_IMAGE_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT |
            VK_IMAGE_USAGE_SAMPLED_BIT,
        VK_IMAGE_ASPECT_DEPTH_BIT);

    VkImageView attachments[] = {color.view, depth.view};

    // Create framebuffer from the attachments
    VkFramebuffer framebuffer = CreateFramebuffer(renderer->device, handle,
        attachments, ARRAY_COUNT(attachments), width, height);

    RenderPassState renderPass = {};
    renderPass.handle = handle;
    renderPass.color = color;
    renderPass.depth = depth;
    renderPass.framebuffer = framebuffer;
    renderPass.width = width;
    renderPass.height = height;
    renderPass.format = VK_FORMAT_R32G32B32A32_SFLOAT;
    renderPass.textureId = texture;

    CreateRenderPassResult result = {};
    result.colorTexture = color;
    result.depthTexture = depth;
    result.renderPass = renderPass;

    return result;

    //StoreRenderPass(renderer, id, renderPass);
    //StoreTexture(renderer, desc->textureId, color);
}

