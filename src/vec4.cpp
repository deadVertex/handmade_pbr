inline vec4 Vec4(f32 r, f32 g, f32 b, f32 a)
{
    vec4 result = {r, g, b, a};
    return result;
}

inline vec4 Vec4(f32 x)
{
    vec4 result = {x, x, x, x};
    return result;
}

inline vec4 Vec4(vec3 v, f32 w)
{
    vec4 result = {v.x, v.y, v.z, w};
    return result;
}

inline vec4 operator+(vec4 a, vec4 b)
{
    vec4 result = {a.x + b.x, a.y + b.y, a.z + b.z, a.w + b.w};
    return result;
}

inline vec4 operator-(vec4 a)
{
    vec4 result = {-a.x, -a.y, -a.z, -a.w};
    return result;
}

inline vec4 operator*(vec4 a, f32 b)
{
    vec4 result = {a.x * b, a.y * b, a.z * b, a.w * b};
    return result;
}

inline vec4 operator*(f32 a, vec4 b)
{
    vec4 result = b * a;
    return result;
}

inline f32 Dot(vec4 a, vec4 b)
{
    // clang-format off
    f32 result = a.data[0] * b.data[0] +
                 a.data[1] * b.data[1] +
                 a.data[2] * b.data[2] +
                 a.data[3] * b.data[3];
    // clang-format on

    return result;
}

inline vec4 Lerp(vec4 a, vec4 b, f32 t)
{
    vec4 result = a * (1.0f - t) + b * t;
    return result;
}
