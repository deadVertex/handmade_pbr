#pragma once

enum
{
    CUBE_MAP_POSITIVE_X = 0,
    CUBE_MAP_NEGATIVE_X = 1,
    CUBE_MAP_POSITIVE_Y = 2,
    CUBE_MAP_NEGATIVE_Y = 3,
    CUBE_MAP_POSITIVE_Z = 4,
    CUBE_MAP_NEGATIVE_Z = 5,
    MAX_CUBE_MAP_FACES = 6,
};

struct HdrCubeMap
{
    HdrImage images[MAX_CUBE_MAP_FACES];
};

struct BasisVectors
{
    vec3 forward;
    vec3 up;
    vec3 right;
};
