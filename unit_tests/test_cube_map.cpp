#include "core_macros.h"
#include "core_types.h"
#include "asset_loader/asset_loader.h"
#include "image.h"
#include "cube_map.h"
#include "cube_map.cpp"

TEST_GROUP(CubeMap);

TEST_SETUP(CubeMap)
{
}

TEST_TEAR_DOWN(CubeMap)
{
}

TEST(CubeMap, Create)
{
    u8 buffer[256];
    StackAllocator allocator = CreateStackAllocator(buffer, sizeof(buffer));
    HdrCubeMap cubeMap = CreateHdrCubeMap(&allocator, 4, 4);
    TEST_ASSERT_NOT_NULL(cubeMap.images[0].pixels);
    TEST_ASSERT_EQUAL(4, cubeMap.images[0].width);
}

TEST(CubeMap, MapCubeMapLayerIndexToVector)
{
    u32 layerIndices[] = {
        CUBE_MAP_POSITIVE_X,
        CUBE_MAP_NEGATIVE_X,
        CUBE_MAP_POSITIVE_Y,
        CUBE_MAP_NEGATIVE_Y,
        CUBE_MAP_POSITIVE_Z,
        CUBE_MAP_NEGATIVE_Z,
    };

    vec3 directions[] = {
        Vec3(1, 0, 0),
        Vec3(-1, 0, 0),
        Vec3(0, 1, 0),
        Vec3(0, -1, 0),
        Vec3(0, 0, 1),
        Vec3(0, 0, -1),
    };

    for (u32 i = 0; i < ARRAY_COUNT(layerIndices); ++i)
    {
        vec3 expected = directions[i];
        vec3 dir = MapCubeMapLayerIndexToVector(layerIndices[i]);
        TEST_ASSERT_VEC3_EQUAL(expected, dir);
    }
}

TEST(CubeMap, MapCubeMapLayerIndexToBasisVectors)
{
    u32 layerIndices[] = {
        CUBE_MAP_POSITIVE_X,
        CUBE_MAP_NEGATIVE_X,
        CUBE_MAP_POSITIVE_Y,
        CUBE_MAP_NEGATIVE_Y,
        CUBE_MAP_POSITIVE_Z,
        CUBE_MAP_NEGATIVE_Z,
    };

    // { forward, up, right } (normal, tangent, bitangent)?
    BasisVectors basisVectors[] = {
        {Vec3(1, 0, 0), Vec3(0, 1, 0), Vec3(0, 0, -1)}, // +X
        {Vec3(-1, 0, 0), Vec3(0, 1, 0), Vec3(0, 0, 1)}, // -X
        {Vec3(0, 1, 0), Vec3(0, 0, -1), Vec3(1, 0, 0)}, // +Y
        {Vec3(0, -1, 0), Vec3(0, 0, 1), Vec3(1, 0, 0)}, // -Y
        {Vec3(0, 0, 1), Vec3(0, 1, 0), Vec3(1, 0, 0)}, // +Z
        {Vec3(0, 0, -1), Vec3(0, 1, 0), Vec3(-1, 0, 0)}, // -Z
    };

    for (u32 i = 0; i < ARRAY_COUNT(layerIndices); ++i)
    {
        BasisVectors expected = basisVectors[i];
        BasisVectors actual =
            MapCubeMapLayerIndexToBasisVectors(layerIndices[i]);
        TEST_ASSERT_VEC3_EQUAL(expected.forward, actual.forward);
        TEST_ASSERT_VEC3_EQUAL(expected.up, actual.up);
        TEST_ASSERT_VEC3_EQUAL(expected.right, actual.right);
    }
}

TEST_GROUP_RUNNER(CubeMap)
{
    RUN_TEST_CASE(CubeMap, Create);
    RUN_TEST_CASE(CubeMap, MapCubeMapLayerIndexToVector);
    RUN_TEST_CASE(CubeMap, MapCubeMapLayerIndexToBasisVectors);
}
