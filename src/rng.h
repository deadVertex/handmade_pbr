#pragma once

struct RandomNumberGenerator
{
    u32 state;
};

// Reference implementation from https://en.wikipedia.org/wiki/Xorshift
inline u32 XorShift32(RandomNumberGenerator *rng)
{
    u32 x = rng->state;
    x ^= x << 13;
    x ^= x >> 17;
    x ^= x << 5;
    rng->state = x;
    return x;
}

inline f32 RandomUnilateral(RandomNumberGenerator *rng)
{
    // Clear the sign bit as SSE interprets integers as signed when converting
    // to packed single
    f32 numerator = (f32)(XorShift32(rng) >> 1);
    f32 denom = (f32)(U32_MAX >> 1);
    f32 result = numerator / denom;
    return result;
}

inline f32 RandomBilateral(RandomNumberGenerator *rng)
{
    f32 result = -1.0f + 2.0f * RandomUnilateral(rng);
    return result;
}
