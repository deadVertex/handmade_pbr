extern "C"
{
#include <unity.h>
#include <unity_fixture.h>
#include <fff.h>
}

#include "math_library.cpp"
#include "test_renderer_frontend.cpp"
#include "test_stack_allocator.cpp"
#include "test_image.cpp"
#include "test_rng.cpp"
#include "test_cube_map.cpp"
#include "test_vulkan_renderer_backend.cpp"

static void RunAllTests(void)
{
    RUN_TEST_GROUP(MathLibrary);
    RUN_TEST_GROUP(StackAllocator);
    RUN_TEST_GROUP(RendererFrontend);
    RUN_TEST_GROUP(Image);
    RUN_TEST_GROUP(RNG);
    RUN_TEST_GROUP(CubeMap);
    RUN_TEST_GROUP(VulkanRendererBackend);
}

int main(int argc, const char *argv[])
{
    return UnityMain(argc, argv, RunAllTests);
}
