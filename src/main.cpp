/* TODO:
- PBR material setup (fresnel, GGX, etc)
- Material input buffer for shaders
- Material definition
- Combine all build scripts into a single one
- Shader reloading
- Live code reloading
- Pipeline depth stuff for skybox rendering
- Bilinear filtering
- Cubemap rendering (cube with frontface culling)
- Fix Cube map distortion
- Proper logging system
- Debug drawing
- UV sphere mesh generation
- Exposure control from application
- Scene definition
- Render graph system
- Store vertex and index buffer in device local memory
- IMGUI integration for UI
- CPU path tracer
- Complete math library
- Resolve FIXMEs
- Enable cppcheck
- Caching generated cube maps on disk

LEFT IN HALF COMPLETE STATE
- Intermediate Representation (commands) for renderer frontend to backend comms
- Unit tests for vulkan renderer backend (needs FFF for heavy mocking)
*/

#include <cstdio>
#include <cstdlib>
#include <cassert>
#include <cstring>

#include <vulkan/vulkan.h>
#include <GLFW/glfw3.h>

#define ENABLE_ASSERTIONS
#include "core_types.h"
#include "core_macros.h"
#include "math_types.h"
#include "input.h"
#include "stack_allocator.h"
#include "image.h"
#include "cube_map.h"
#include "shader_types.h"
#include "rng.h"

#include "renderer_backend.h"
#include "renderer_frontend.h"
#include "vulkan_renderer_backend.h"
#include "asset_loader/asset_loader.h"

#include "math_functions.cpp"
#include "vec2.cpp"
#include "vec3.cpp"
#include "vec4.cpp"
#include "mat4.cpp"
#include "image.cpp"
#include "cube_map.cpp"
#include "renderer_frontend.cpp"
#include "vulkan_common.cpp"
#include "vulkan_renderer_backend.cpp"

void InputBeginFrame(GameInput *input)
{
    for (u32 buttonIndex = 0; buttonIndex < ARRAY_COUNT(input->buttonStates);
         ++buttonIndex)
    {
        input->buttonStates[buttonIndex].wasDown =
            input->buttonStates[buttonIndex].isDown;
    }

    input->mouseRelPosX = 0;
    input->mouseRelPosY = 0;
}

#define KEY_HELPER(NAME)                                                       \
    case GLFW_KEY_##NAME:                                                      \
        return KEY_##NAME;
i32 ConvertKey(int key)
{
    if (key >= GLFW_KEY_SPACE && key <= GLFW_KEY_GRAVE_ACCENT)
    {
        return key;
    }
    switch (key)
    {
        KEY_HELPER(BACKSPACE);
        KEY_HELPER(TAB);
        KEY_HELPER(INSERT);
        KEY_HELPER(HOME);
        KEY_HELPER(PAGE_UP);
    // Can't use KEY_HELPER( DELETE ) as windows has a #define for DELETE
    case GLFW_KEY_DELETE:
        return KEY_DELETE;
        KEY_HELPER(END);
        KEY_HELPER(PAGE_DOWN);
        KEY_HELPER(ENTER);

        KEY_HELPER(LEFT_SHIFT);
    case GLFW_KEY_LEFT_CONTROL:
        return KEY_LEFT_CTRL;
        KEY_HELPER(LEFT_ALT);
        KEY_HELPER(RIGHT_SHIFT);
    case GLFW_KEY_RIGHT_CONTROL:
        return KEY_RIGHT_CTRL;
        KEY_HELPER(RIGHT_ALT);

        KEY_HELPER(LEFT);
        KEY_HELPER(RIGHT);
        KEY_HELPER(UP);
        KEY_HELPER(DOWN);

        KEY_HELPER(ESCAPE);

        KEY_HELPER(F1);
        KEY_HELPER(F2);
        KEY_HELPER(F3);
        KEY_HELPER(F4);
        KEY_HELPER(F5);
        KEY_HELPER(F6);
        KEY_HELPER(F7);
        KEY_HELPER(F8);
        KEY_HELPER(F9);
        KEY_HELPER(F10);
        KEY_HELPER(F11);
        KEY_HELPER(F12);
    case GLFW_KEY_KP_0:
        return KEY_NUM0;
    case GLFW_KEY_KP_1:
        return KEY_NUM1;
    case GLFW_KEY_KP_2:
        return KEY_NUM2;
    case GLFW_KEY_KP_3:
        return KEY_NUM3;
    case GLFW_KEY_KP_4:
        return KEY_NUM4;
    case GLFW_KEY_KP_5:
        return KEY_NUM5;
    case GLFW_KEY_KP_6:
        return KEY_NUM6;
    case GLFW_KEY_KP_7:
        return KEY_NUM7;
    case GLFW_KEY_KP_8:
        return KEY_NUM8;
    case GLFW_KEY_KP_9:
        return KEY_NUM9;
    case GLFW_KEY_KP_DECIMAL:
        return KEY_NUM_DECIMAL;
    case GLFW_KEY_KP_DIVIDE:
        return KEY_NUM_DIVIDE;
    case GLFW_KEY_KP_MULTIPLY:
        return KEY_NUM_MULTIPLY;
    case GLFW_KEY_KP_SUBTRACT:
        return KEY_NUM_MINUS;
    case GLFW_KEY_KP_ADD:
        return KEY_NUM_PLUS;
    case GLFW_KEY_KP_ENTER:
        return KEY_NUM_ENTER;
    }
    return KEY_UNKNOWN;
}

void KeyCallback(
    GLFWwindow *window, int glfwKey, int scancode, int action, int mods)
{
    (void)mods;
    (void)scancode;

    GameInput *input =
            (GameInput *)glfwGetWindowUserPointer(window);

    i32 key = ConvertKey(glfwKey);
    if (key != KEY_UNKNOWN)
    {
        ASSERT_MACRO(key < MAX_KEYS);
        if (action == GLFW_PRESS)
        {
            input->buttonStates[key].isDown = true;
        }
        else if (action == GLFW_RELEASE)
        {
            input->buttonStates[key].isDown = false;
        }
        // else: ignore key repeat messages
    }
}

void MouseButtonCallback(
    GLFWwindow *window, int button, int action, int mods)
{
    (void)mods;

    GameInput *input =
            (GameInput *)glfwGetWindowUserPointer(window);

    if (button == GLFW_MOUSE_BUTTON_LEFT)
    {
        input->buttonStates[KEY_MOUSE_BUTTON_LEFT].isDown = (action == GLFW_PRESS);
    }
    if (button == GLFW_MOUSE_BUTTON_MIDDLE)
    {
        input->buttonStates[KEY_MOUSE_BUTTON_MIDDLE].isDown = (action == GLFW_PRESS);
    }
    if (button == GLFW_MOUSE_BUTTON_RIGHT)
    {
        input->buttonStates[KEY_MOUSE_BUTTON_RIGHT].isDown = (action == GLFW_PRESS);
    }
}

f64 g_PrevMousePosX;
f64 g_PrevMousePosY;

void CursorPositionCallback(GLFWwindow *window, f64 xPos, f64 yPos)
{
    GameInput *input = (GameInput *)glfwGetWindowUserPointer(window);

    // Unfortunately GLFW doesn't seem to provide a way to get mouse motion
    // rather than cursor position so we have to compute the relative motion
    // ourselves.
    f64 relX = xPos - g_PrevMousePosX;
    f64 relY = yPos - g_PrevMousePosY;
    g_PrevMousePosX = xPos;
    g_PrevMousePosY = yPos;

    input->mouseRelPosX = (i32)Floor((f32)relX);
    input->mouseRelPosY = (i32)Floor((f32)relY);
    input->mousePosX = (i32)Floor((f32)xPos);
    input->mousePosY = (i32)Floor((f32)yPos);
}

void ShowMouseCursor(GLFWwindow *window, b32 isVisible)
{
    glfwSetInputMode(window, GLFW_CURSOR,
        isVisible ? GLFW_CURSOR_NORMAL : GLFW_CURSOR_DISABLED);
}

vec3 UpdateCameraPosition(
    vec3 currentPosition, vec3 cameraRotation, GameInput *input, f32 dt)
{
    f32 forwardMove = 0.0f;
    f32 rightMove = 0.0f;
    if (input->buttonStates[KEY_W].isDown)
    {
        forwardMove = -1.0f;
    }

    if (input->buttonStates[KEY_S].isDown)
    {
        forwardMove = 1.0f;
    }

    if (input->buttonStates[KEY_A].isDown)
    {
        rightMove = -1.0f;
    }
    if (input->buttonStates[KEY_D].isDown)
    {
        rightMove = 1.0f;
    }

    mat4 rotationMatrix = RotateY(cameraRotation.y) * RotateX(cameraRotation.x);

    vec3 forward = TransformVector(Vec3(0, 0, 1), rotationMatrix);
    vec3 right = TransformVector(Vec3(1, 0, 0), rotationMatrix);
    vec3 targetDir = Normalize(forward * forwardMove + right * rightMove);

    f32 speed = 16.0f;
    vec3 newPosition = currentPosition + targetDir * speed;
    newPosition = Lerp(currentPosition, newPosition, dt);

    return newPosition;
}

vec3 UpdateCameraRotation(vec3 rotation, GameInput *input)
{
    f32 sens = 0.005f;
    f32 mouseX = -input->mouseRelPosY * sens;
    f32 mouseY = -input->mouseRelPosX * sens;

    vec3 newRotation = Vec3(mouseX, mouseY, 0.0f);
    rotation += newRotation;
    rotation.x = Clamp(rotation.x, -PI * 0.5f, PI * 0.5f);

    if (rotation.y > 2.0f * PI)
    {
        rotation.y -= 2.0f * PI;
    }
    if (rotation.y < 2.0f * -PI)
    {
        rotation.y += 2.0f * PI;
    }

    return rotation;
}

struct ApplicationState
{
    u32 framebufferWidth;
    u32 framebufferHeight;
};

b32 handleWindowResize(GLFWwindow *window, ApplicationState *appState)
{
    b32 result = false;
    i32 newFramebufferWidth = 0;
    i32 newFramebufferHeight = 0;
    glfwGetFramebufferSize(window, &newFramebufferWidth, &newFramebufferHeight);
    if ((u32)newFramebufferWidth != appState->framebufferWidth ||
        (u32)newFramebufferHeight != appState->framebufferHeight)
    {
        printf("Framebuffer resized to %d x %d\n", newFramebufferWidth,
            newFramebufferHeight);

        appState->framebufferWidth = (u32)newFramebufferWidth;
        appState->framebufferHeight = (u32)newFramebufferHeight;

        result = true;
    }

    return result;
}

struct MeshOffsets
{
    u32 vertexIndexOffset;
    u32 indexOffset;
    u32 indexCount;
};

inline u32 TruncateToU32(i64 x)
{
    ASSERT_MACRO(x >= 0);
    ASSERT_MACRO(x <= U32_MAX);

    u32 result = (u32)x;
    return result;
}

MeshOffsets CreateTriangleMesh(StackAllocator *vertexAllocator, StackAllocator *indexAllocator)
{
    Vertex *vertices = ALLOCATE_ARRAY(vertexAllocator, Vertex, 3);
    ASSERT_MACRO(vertices != NULL);
    u32 *indices = ALLOCATE_ARRAY(indexAllocator, u32, 3);
    ASSERT_MACRO(indices != NULL);

    // clang-format off
    Vertex triangleVertices[] = {
        {{-0.5f, -0.5f, 0.0f}, {0.0f, 0.0f, 1.0f}, {0.0f, 0.0f}},
        {{0.5f, -0.5f, 0.0f}, {0.0f, 0.0f, 1.0f}, {1.0f, 0.0f}},
        {{0.0f, 0.5f, 0.0f}, {0.0f, 0.0f, 1.0f}, {0.5f, 1.0f}},
    };
    // clang-format on

    COPY_MEMORY(vertices, triangleVertices, sizeof(triangleVertices));

    indices[0] = 0;
    indices[1] = 1;
    indices[2] = 2;

    MeshOffsets result = {};
    result.indexCount = 3;
    result.indexOffset = TruncateToU32(indices - (u32 *)indexAllocator->base);
    result.vertexIndexOffset =
        TruncateToU32(vertices - (Vertex *)vertexAllocator->base);
    return result;
}

MeshOffsets CreateQuadMesh(StackAllocator *vertexAllocator, StackAllocator *indexAllocator)
{
    Vertex *vertices = ALLOCATE_ARRAY(vertexAllocator, Vertex, 4);
    ASSERT_MACRO(vertices != NULL);
    u32 *indices = ALLOCATE_ARRAY(indexAllocator, u32, 6);
    ASSERT_MACRO(indices != NULL);

    Vertex quadVertices[] = {
        {{-0.5f, -0.5f, 0.0f}, {0.0f, 0.0f, 1.0f}, {0.0f, 0.0f}},
        {{0.5f, -0.5f, 0.0f}, {0.0f, 0.0f, 1.0f}, {1.0f, 0.0f}},
        {{0.5f, 0.5f, 0.0f}, {0.0f, 0.0f, 1.0f}, {1.0f, 1.0f}},
        {{-0.5f, 0.5f, 0.0f}, {0.0f, 0.0f, 1.0f}, {0.0f, 1.0f}},
    };

    COPY_MEMORY(vertices, quadVertices, sizeof(quadVertices));

    indices[0] = 0;
    indices[1] = 1;
    indices[2] = 2;
    indices[3] = 2;
    indices[4] = 3;
    indices[5] = 0;

    MeshOffsets result = {};
    result.indexCount = 6;
    result.indexOffset = TruncateToU32(indices - (u32 *)indexAllocator->base);
    result.vertexIndexOffset =
        TruncateToU32(vertices - (Vertex *)vertexAllocator->base);
    return result;
}

MeshOffsets CreateCubeMesh(
    StackAllocator *vertexAllocator, StackAllocator *indexAllocator)
{
    // clang-format off
    Vertex cubeVertices[] = {
        // Top
        {{-0.5f, 0.5f, -0.5f}, {0.0f, 1.0f, 0.0f}, {0.0f, 1.0f}},
        {{0.5f, 0.5f, -0.5f}, {0.0f, 1.0f, 0.0f}, {1.0f, 1.0f}},
        {{0.5f, 0.5f, 0.5f}, {0.0f, 1.0f, 0.0f}, {1.0f, 0.0f}},
        {{-0.5f, 0.5f, 0.5f}, {0.0f, 1.0f, 0.0f}, {0.0f, 0.0f}},

        // Bottom
        {{-0.5f, -0.5f, -0.5f}, {0.0f, -1.0f, 0.0f}, {0.0f, 1.0f}},
        {{0.5f, -0.5f, -0.5f}, {0.0f, -1.0f, 0.0f}, {1.0f, 1.0f}},
        {{0.5f, -0.5f, 0.5f}, {0.0f, -1.0f, 0.0f}, {1.0f, 0.0f}},
        {{-0.5f, -0.5f, 0.5f}, {0.0f, -1.0f, 0.0f}, {0.0f, 0.0f}},

        // Back
        {{-0.5f, 0.5f, -0.5f}, {0.0f, 0.0f, -1.0f}, {1.0f, 1.0f}},
        {{0.5f, 0.5f, -0.5f}, {0.0f, 0.0f, -1.0f}, {0.0f, 1.0f}},
        {{0.5f, -0.5f, -0.5f}, {0.0f, 0.0f, -1.0f}, {0.0f, 0.0f}},
        {{-0.5f, -0.5f, -0.5f}, {0.0f, 0.0f, -1.0f}, {1.0f, 0.0f}},

        // Front
        {{-0.5f, 0.5f, 0.5f}, {0.0f, 0.0f, 1.0f}, {0.0f, 1.0f}},
        {{0.5f, 0.5f, 0.5f}, {0.0f, 0.0f, 1.0f}, {1.0f, 1.0f}},
        {{0.5f, -0.5f, 0.5f}, {0.0f, 0.0f, 1.0f}, {1.0f, 0.0f}},
        {{-0.5f, -0.5f, 0.5f}, {0.0f, 0.0f, 1.0f}, {0.0f, 0.0f}},

        // Left
        {{-0.5f, 0.5f, -0.5f}, {-1.0f, 0.0f, 0.0f}, {0.0f, 1.0f}},
        {{-0.5f, -0.5f, -0.5f}, {-1.0f, 0.0f, 0.0f}, {0.0f, 0.0f}},
        {{-0.5f, -0.5f, 0.5f}, {-1.0f, 0.0f, 0.0f}, {1.0f, 0.0f}},
        {{-0.5f, 0.5f, 0.5f}, {-1.0f, 0.0f, 0.0f}, {1.0f, 1.0f}},

        // Right
        {{0.5f, 0.5f, -0.5f}, {1.0f, 0.0f, 0.0f}, {1.0f, 1.0f}},
        {{0.5f, -0.5f, -0.5f}, {1.0f, 0.0f, 0.0f}, {1.0f, 0.0f}},
        {{0.5f, -0.5f, 0.5f}, {1.0f, 0.0f, 0.0f}, {0.0f, 0.0f}},
        {{0.5f, 0.5f, 0.5f}, {1.0f, 0.0f, 0.0f}, {0.0f, 1.0f}},
    };

    u32 cubeIndices[] = {
        2, 1, 0,
        0, 3, 2,

        4, 5, 6,
        6, 7, 4,

        8, 9, 10,
        10, 11, 8,

        14, 13, 12,
        12, 15, 14,

        16, 17, 18,
        18, 19, 16,

        22, 21, 20,
        20, 23, 22
    };
    // clang-format on

    Vertex *vertices =
        ALLOCATE_ARRAY(vertexAllocator, Vertex, ARRAY_COUNT(cubeVertices));
    ASSERT_MACRO(vertices != NULL);
    u32 *indices =
        ALLOCATE_ARRAY(indexAllocator, u32, ARRAY_COUNT(cubeIndices));
    ASSERT_MACRO(indices != NULL);

    COPY_MEMORY(vertices, cubeVertices, sizeof(cubeVertices));
    COPY_MEMORY(indices, cubeIndices, sizeof(cubeIndices));

    MeshOffsets result = {};
    result.indexCount = ARRAY_COUNT(cubeIndices);
    result.indexOffset = TruncateToU32(indices - (u32 *)indexAllocator->base);
    result.vertexIndexOffset =
        TruncateToU32(vertices - (Vertex *)vertexAllocator->base);
    return result;
}

struct TriangleFace
{
    u32 i, j, k;
};

inline Vertex CreateUnitVertex(f32 x, f32 y, f32 z)
{
    Vertex result = {};
    result.position = Normalize(Vec3(x, y, z));
    result.normal = result.position;
    result.textureCoords = Vec2(result.position.x, result.position.y);
    return result;
}

struct MeshData
{
    Vertex *vertices;
    u32 *indices;
    u32 vertexCount;
    u32 indexCount;
};

// FIXME: Super inefficient with memory! Needs temp allocators? Pre-calculate
// number of vertices and indices for each tesselation level?
MeshData CreateIcosahedronMesh(u32 tesselationLevel, StackAllocator *arena)
{
    MeshData result = {};
    // TODO: Actually calculate how many vertices and indices
    u32 maxVertices = 2048;
    u32 maxIndices = 4096;
    result.vertices = ALLOCATE_ARRAY(arena, Vertex, maxVertices);
    result.indices = ALLOCATE_ARRAY(arena, u32, maxIndices);

    f32 t = (1.0f + Sqrt(5.0f)) * 0.5f;

    result.vertices[0] = CreateUnitVertex(-1, t, 0);
    result.vertices[1] = CreateUnitVertex(1, t, 0);
    result.vertices[2] = CreateUnitVertex(-1, -t, 0);
    result.vertices[3] = CreateUnitVertex(1, -t, 0);

    result.vertices[4] = CreateUnitVertex(0, -1, t);
    result.vertices[5] = CreateUnitVertex(0, 1, t);
    result.vertices[6] = CreateUnitVertex(0, -1, -t);
    result.vertices[7] = CreateUnitVertex(0, 1, -t);

    result.vertices[8] = CreateUnitVertex(t, 0, -1);
    result.vertices[9] = CreateUnitVertex(t, 0, 1);
    result.vertices[10] = CreateUnitVertex(-t, 0, -1);
    result.vertices[11] = CreateUnitVertex(-t, 0, 1);

    u32 vertexCount = 12;

    u32 initialFaceCount = 20;
    TriangleFace *initialFaces =
        ALLOCATE_ARRAY(arena, TriangleFace, initialFaceCount);

    initialFaces[0] = TriangleFace{0, 11, 5};
    initialFaces[1] = TriangleFace{0, 5, 1};
    initialFaces[2] = TriangleFace{0, 1, 7};
    initialFaces[3] = TriangleFace{0, 7, 10};
    initialFaces[4] = TriangleFace{0, 10, 11};

    initialFaces[5] = TriangleFace{1, 5, 9};
    initialFaces[6] = TriangleFace{5, 11, 4};
    initialFaces[7] = TriangleFace{11, 10, 2};
    initialFaces[8] = TriangleFace{10, 7, 6};
    initialFaces[9] = TriangleFace{7, 1, 8};

    initialFaces[10] = TriangleFace{3, 9, 4};
    initialFaces[11] = TriangleFace{3, 4, 2};
    initialFaces[12] = TriangleFace{3, 2, 6};
    initialFaces[13] = TriangleFace{3, 6, 8};
    initialFaces[14] = TriangleFace{3, 8, 9};

    initialFaces[15] = TriangleFace{4, 9, 5};
    initialFaces[16] = TriangleFace{2, 4, 11};
    initialFaces[17] = TriangleFace{6, 2, 10};
    initialFaces[18] = TriangleFace{8, 6, 7};
    initialFaces[19] = TriangleFace{9, 8, 1};

    u32 currentFaceCount = initialFaceCount;
    TriangleFace *currentFaces = initialFaces;
    for (u32 i = 0; i < tesselationLevel; ++i)
    {
        TriangleFace *newFaces =
            ALLOCATE_ARRAY(arena, TriangleFace, currentFaceCount * 4);
        u32 newFaceCount = 0;

        for (size_t j = 0; j < currentFaceCount; ++j)
        {
            TriangleFace currentFace = currentFaces[j];

            // 3 edges in total comprised of 2 indices each
            u32 edges[6] = {currentFace.i, currentFace.j, currentFace.j,
                currentFace.k, currentFace.k, currentFace.i};

            u32 indicesAdded[3];

            for (u32 k = 0; k < 3; ++k)
            {
                // Retrieve vertices for each edge
                u32 idx0 = edges[k * 2];
                u32 idx1 = edges[k * 2 + 1];
                Vertex v0 = result.vertices[idx0];
                Vertex v1 = result.vertices[idx1];

                // Add mid point vertex
                vec3 u = v0.position + v1.position;

                ASSERT_MACRO(vertexCount < maxVertices);
                result.vertices[vertexCount] = CreateUnitVertex(u.x, u.y, u.z);

                indicesAdded[k] = vertexCount++;
            }

            newFaces[newFaceCount++] =
                TriangleFace{currentFace.i, indicesAdded[0], indicesAdded[2]};
            newFaces[newFaceCount++] =
                TriangleFace{currentFace.j, indicesAdded[1], indicesAdded[0]};
            newFaces[newFaceCount++] =
                TriangleFace{currentFace.k, indicesAdded[2], indicesAdded[1]};
            newFaces[newFaceCount++] =
                TriangleFace{indicesAdded[0], indicesAdded[1], indicesAdded[2]};
        }

        currentFaces = newFaces;
        currentFaceCount = newFaceCount;
    }

    ASSERT_MACRO(currentFaceCount * 3 < maxIndices);
    COPY_MEMORY(result.indices, currentFaces, currentFaceCount * sizeof(TriangleFace));

    // Also frees the allocated faces for all tesselation levels
    //MemoryArenaFree(arena, initialFaces);

    result.vertexCount = vertexCount;
    result.indexCount = currentFaceCount * 3;

    //CalculateTangents(result);

    return result;
}

MeshOffsets CreateSphereMesh(
    StackAllocator *vertexAllocator, StackAllocator *indexAllocator)
{
    u32 tempMemorySize = KILOBYTES(256);
    void *tempMemory = malloc(tempMemorySize);
    ASSERT_MACRO(tempMemory);

    StackAllocator tempAllocator =
        CreateStackAllocator(tempMemory, tempMemorySize);
    MeshData meshData = CreateIcosahedronMesh(2, &tempAllocator);

    Vertex *vertices =
        ALLOCATE_ARRAY(vertexAllocator, Vertex, meshData.vertexCount);
    ASSERT_MACRO(vertices != NULL);
    u32 *indices = ALLOCATE_ARRAY(indexAllocator, u32, meshData.indexCount);
    ASSERT_MACRO(indices != NULL);

    COPY_MEMORY(
        vertices, meshData.vertices, sizeof(Vertex) * meshData.vertexCount);
    COPY_MEMORY(indices, meshData.indices, sizeof(u32) * meshData.indexCount);

    free(tempMemory);

    MeshOffsets result = {};
    result.indexCount = meshData.indexCount;
    result.indexOffset = TruncateToU32(indices - (u32 *)indexAllocator->base);
    result.vertexIndexOffset =
        TruncateToU32(vertices - (Vertex *)vertexAllocator->base);

    return result;
}

inline BackendDrawMeshCommand CreateMeshDrawCommand(
    MeshOffsets offsets, u32 modelMatrixIndex)
{
    BackendDrawMeshCommand cmd = {};
    cmd.vertexIndexOffset = offsets.vertexIndexOffset;
    cmd.modelMatrixIndex = modelMatrixIndex;
    cmd.indexOffset = offsets.indexOffset;
    cmd.indexCount = offsets.indexCount;
    return cmd;
}

HdrImage CreateTestImage(StackAllocator *allocator)
{
    // Generate some pixel data
    // clang-format off
    f32 pixels[] = {
        1.0f, 0.0f, 1.0f, 1.0f,     0.0f, 0.0f, 0.0f, 1.0f,
        0.0f, 0.0f, 0.0f, 0.0f,     1.0f, 0.0f, 1.0f, 1.0f,
    };
    // clang-format on
    HdrImage image = CreateHdrImage(allocator, 2, 2);
    COPY_MEMORY(
        image.pixels, pixels, sizeof(f32) * 4 * image.width * image.height);

    return image;
}

HdrCubeMap CreateTestCubeMap(StackAllocator *allocator)
{
    // clang-format off
    f32 pixels[] = {
        1.0f, 0.0f, 0.0f, 1.0f, // +X
        1.0f, 0.0f, 0.0f, 1.0f, // -X
        0.0f, 1.0f, 0.0f, 1.0f, // +Y
        0.0f, 1.0f, 0.0f, 1.0f, // -Y
        0.0f, 0.0f, 1.0f, 1.0f, // +Z
        0.0f, 0.0f, 1.0f, 1.0f, // -Z
    };
    // clang-format on

    HdrCubeMap cubeMap = CreateHdrCubeMap(allocator, 1, 1);
    for (u32 i = 0; i < MAX_CUBE_MAP_FACES; ++i)
    {
        COPY_MEMORY(
            cubeMap.images[i].pixels, pixels + (i * 4), sizeof(f32) * 4);
    }

    return cubeMap;
}

void VulkanBackendOnFrameBufferResize(
    void *renderer, u32 newWidth, u32 newHeight)
{
    OnFramebufferResize((VulkanRenderer *)renderer, newWidth, newHeight);
}

void VulkanProcessCommands(void *renderer, RendererBackendCommand *cmds, u32 count)
{
    ProcessCommands((VulkanRenderer *)renderer, cmds, count);
}

void UpdatePipelineResources(VulkanRenderer *renderer, u32 id)
{
    RendererBackendCommand command = {};
    command.type = RendererBackendCommandType_UpdateGraphicsPipelineResources;
    command.updateGraphicsPipelineResources.id = id;

    ProcessCommands(renderer, &command, 1);
}

// FIXME: Move this out of here!!!!
struct FileData
{
    u32 length;
    void *contents;
};

#pragma warning(push)
#pragma warning(disable:4996)
FileData mapFileIntoMemory(const char *path)
{
    FileData result = {};
    FILE *file = fopen(path, "rb");
    if (file != NULL)
    {
        fseek(file, 0, SEEK_END);
        u32 length = (u32)ftell(file);
        fseek(file, 0, SEEK_SET);

        void *contents = malloc(length);
        if (contents != NULL)
        {
            if (fread(contents, length, 1, file) == 1)
            {
                // Success
                result.contents = contents;
                result.length = length;
            }
            else
            {
                printf("Failed to read %u bytes from file %s\n", length, path);
                free(contents); // Free memory as we won't return it if the read
                                // fails
            }
        }
        else
        {
            printf("Failed to allocate %u bytes for file %s\n", length, path);
        }

        fclose(file);
    }
    else
    {
        printf("Failed to open file %s\n", path);
    }
    return result;
}

void freeFileDataFromMemory(FileData fileData)
{
    ASSERT_MACRO(fileData.contents != NULL);
    free(fileData.contents);
}
#pragma warning(pop)

// TODO: Move this into the rendererFrontend
void MakePipeline(VulkanRenderer *renderer, u32 id, const char *vertexShaderPath,
    const char *fragmentShaderPath, u32 renderPassId)
{
    RendererBackendCommand command = {};
    command.type = RendererBackendCommandType_CreateGraphicsPipeline;
    command.createGraphicsPipeline.id = id;
    command.createGraphicsPipeline.renderPass = renderPassId;

    FileData vertexShaderFile = mapFileIntoMemory(vertexShaderPath);

    // Validate the file input
    ASSERT_MACRO(vertexShaderFile.contents != NULL);
    ASSERT_MACRO(vertexShaderFile.length % 4 == 0);

    FileData fragmentShaderFile = mapFileIntoMemory(fragmentShaderPath);

    // Validate the file input
    ASSERT_MACRO(fragmentShaderFile.contents != NULL);
    ASSERT_MACRO(fragmentShaderFile.length % 4 == 0);

    command.createGraphicsPipeline.vertexShaderCode = vertexShaderFile.contents;
    command.createGraphicsPipeline.vertexShaderCodeLength =
        vertexShaderFile.length;
    command.createGraphicsPipeline.fragmentShaderCode =
        fragmentShaderFile.contents;
    command.createGraphicsPipeline.fragmentShaderCodeLength =
        fragmentShaderFile.length;

    ProcessCommands(renderer, &command, 1);

    freeFileDataFromMemory(vertexShaderFile);
    freeFileDataFromMemory(fragmentShaderFile);

    UpdatePipelineResources(renderer, id);
}

struct PipelineDescription
{
    u32 id;
    const char *vertexShaderPath;
    const char *fragmentShaderPath;
    u32 renderPass;
};

void MakePipelines(VulkanRenderer *renderer, PipelineDescription *descriptions, u32 count)
{
    for (u32 i = 0; i < count; ++i)
    {
        PipelineDescription *desc = descriptions + i;
        MakePipeline(renderer, desc->id, desc->vertexShaderPath,
            desc->fragmentShaderPath, desc->renderPass);
    }
}

void CreateCubeMapFromComputeShader(VulkanRenderer *renderer, u32 pipelineId,
    const char *path, u32 inputTextureId, u32 outputTextureId, u32 cubeMapWidth)
{
    FileData computeShaderFile = mapFileIntoMemory(path);

    // Validate the file input
    ASSERT_MACRO(computeShaderFile.contents != NULL);
    ASSERT_MACRO(computeShaderFile.length % 4 == 0);

    RendererBackendCommand commands[4] = {};
    commands[0].type = RendererBackendCommandType_CreateCubeMap;
    commands[0].createCubeMap.id = outputTextureId;
    commands[0].createCubeMap.width = cubeMapWidth;
    commands[0].createCubeMap.flags = CreateCubeMapFlag_ComputeShaderOutput;
    commands[1].type = RendererBackendCommandType_CreateComputePipeline;
    commands[1].createComputePipeline.id = pipelineId;

    commands[1].createComputePipeline.shaderCode = computeShaderFile.contents;
    commands[1].createComputePipeline.shaderCodeLength = computeShaderFile.length;

    commands[2].type = RendererBackendCommandType_UpdateComputePipelineResources;
    commands[2].updateComputePipelineResources.id = pipelineId;
    commands[2].updateComputePipelineResources.resources[0].type = PipelineResourceType_Texture;
    commands[2].updateComputePipelineResources.resources[0].id = inputTextureId;
    commands[2].updateComputePipelineResources.resources[1].type = PipelineResourceType_Texture;
    commands[2].updateComputePipelineResources.resources[1].id = outputTextureId;
    commands[2].updateComputePipelineResources.resourceCount = 2;

    commands[3].type = RendererBackendCommandType_DispatchComputePipeline;
    commands[3].dispatchComputePipeline.id = pipelineId;
    commands[3].dispatchComputePipeline.groupCountX = cubeMapWidth;
    commands[3].dispatchComputePipeline.groupCountY = cubeMapWidth;
    commands[3].dispatchComputePipeline.groupCountZ = MAX_CUBE_MAP_FACES;

    ProcessCommands(renderer, commands, ARRAY_COUNT(commands));
}

void CreateSceneRenderPass(VulkanRenderer *renderer, ApplicationState appState)
{
    RendererBackendCommand cmd = {};
    cmd.type = RendererBackendCommandType_CreateRenderPass;
    cmd.createRenderPass.id = RenderPass_Scene;
    cmd.createRenderPass.width = appState.framebufferWidth;
    cmd.createRenderPass.height = appState.framebufferHeight;
    cmd.createRenderPass.texture = Texture_HdrRenderPass;

    ProcessCommands(renderer, &cmd, 1);
}

int main(int argc, char **argv)
{
    (void)argc;
    (void)argv;

    ApplicationState appState = {};
    appState.framebufferWidth = 1024;
    appState.framebufferHeight = 768;

    printf("Hello handmade_pbr!\n");

    if (!glfwInit())
    {
        printf("Failed to initialize GLFW\n");
        return -1;
    }

    glfwWindowHint(GLFW_CLIENT_API, GLFW_NO_API);
    GLFWwindow *window = glfwCreateWindow((i32)appState.framebufferWidth,
        (i32)appState.framebufferHeight, "handmade_pbr", NULL, NULL);

    if (!window)
    {
        glfwTerminate();
        return -1;
    }

    GameInput input = {};
    glfwSetWindowUserPointer(window, &input);
    glfwSetKeyCallback(window, KeyCallback);
    glfwSetMouseButtonCallback(window, MouseButtonCallback);
    glfwSetCursorPosCallback(window, CursorPositionCallback);

    DumpAvailableLayers();
    DumpAvailableExtensions();
    VulkanRenderer renderer = createVulkanRenderer(window);

    RendererBackend backend = {};
    backend.renderer = &renderer;
    backend.onFramebufferResize = &VulkanBackendOnFrameBufferResize;
    backend.processCommands = &VulkanProcessCommands;
    RendererFrontend rendererFrontend = CreateRendererFrontend(backend);

    CreateSceneRenderPass(&renderer, appState);

    // clang-format off
    PipelineDescription pipelineDescriptions[] = {
        // id, vertex shader path, fragment shader path, renderpass
        {Pipeline_Default, "mesh.vert.spv", "mesh.frag.spv", RenderPass_Scene},
        {Pipeline_Green, "mesh.vert.spv", "mesh_green.frag.spv", RenderPass_Scene},
        {Pipeline_Lighting, "mesh.vert.spv", "mesh_lighting.frag.spv", RenderPass_Scene},
        {Pipeline_PbrMaterial, "mesh.vert.spv", "pbr_material.frag.spv", RenderPass_Scene},
        {Pipeline_Skybox, "skybox.vert.spv", "skybox.frag.spv", RenderPass_Scene},
        {Pipeline_PostProcessing, "post_processing.vert.spv", "post_processing.frag.spv", RenderPass_PostProcessing},
    };
    // clang-format on

    MakePipelines(
        &renderer, pipelineDescriptions, ARRAY_COUNT(pipelineDescriptions));

    u32 imageAllocatorSize = MEGABYTES(64);
    StackAllocator imageAllocator =
        CreateStackAllocator(malloc(imageAllocatorSize), imageAllocatorSize);

    {
        HdrImage hdrImage = {};
        if (LoadExrImage(
                &hdrImage, "../assets/photo_studio_loft_hall_4k.exr") == 0)
        {

            RegisterTexture(&rendererFrontend, Texture_Equirectangular, hdrImage);
            free(hdrImage.pixels);
        }
    }

    /* Renderer IR
    [X] Create image : ImageID, width, height, format
    [X] Update image : ImageID, memory address, offset, size
    Destroy image : ImageID

    [X] Create cube map : ImageID, width, format
    [X] Update cube map : ImageID, memory address, offset, size
    Destroy cube map : ImageID

    [X] Create compute pipeline : ...
    [X] Update compute pipeline resources : ...
    [X] Dispatch compute pipeline : ComputePipelineID ...
    Destroy compute pipeline : ComputePipelineID

    [X] Create graphics pipeline : PipelineID, resources description (inc. index buffer), memory addresses for shader byte code
    Update graphics pipeline resources : PipelineID, ...
    Dispatch graphics pipeline : PipelineID, ...
    Destroy graphics pipeline : PipelineID

    [X] Create render pass : RenderPassID, ...
    Start render pass : RenderPassID, clearValues
    End render pass : RenderPassID,
    Destroy render pass : RenderPassID,

    Begin frame :  ...
    End frame : ...

    Create buffer from memory : BufferID, ...
    */

    HdrCubeMap testCubeMap = CreateTestCubeMap(&imageAllocator);
    RegisterCubeMap(&rendererFrontend, Texture_CubeMapTest, testCubeMap);

    HdrImage testImage = CreateTestImage(&imageAllocator);
    RegisterTexture(&rendererFrontend, Texture_Pink, testImage);

    // TODO: Decompose into smaller ops
    CreateCubeMapFromComputeShader(&renderer, Pipeline_ConvertEquirectangular,
        "convert_equirectangular.comp.spv", Texture_Equirectangular,
        Texture_CubeMapFromFile, 512);
    CreateCubeMapFromComputeShader(&renderer, Pipeline_ComputeIrradiance,
        "irradiance_cubemap.comp.spv", Texture_CubeMapFromFile,
        Texture_IrradianceCubeMap, 32);

    BindTextureToPipeline(&renderer, Pipeline_Default, Texture_Pink);
    BindTextureToPipeline(&renderer, Pipeline_Lighting, Texture_IrradianceCubeMap);
    BindTextureToPipeline(&renderer, Pipeline_PbrMaterial, Texture_IrradianceCubeMap);
    BindTextureToPipeline(&renderer, Pipeline_Skybox, Texture_CubeMapFromFile);
    BindTextureToPipeline(&renderer, Pipeline_PostProcessing, Texture_HdrRenderPass);

    MeshOffsets triangleMesh = CreateTriangleMesh(
        &renderer.vertexAllocator, &renderer.indexAllocator);
    MeshOffsets quadMesh = CreateQuadMesh(
        &renderer.vertexAllocator, &renderer.indexAllocator);
    MeshOffsets cubeMesh =
        CreateCubeMesh(&renderer.vertexAllocator, &renderer.indexAllocator);
    MeshOffsets sphereMesh =
        CreateSphereMesh(&renderer.vertexAllocator, &renderer.indexAllocator);

    RegisterMesh(&rendererFrontend, Mesh_Triangle, triangleMesh.vertexIndexOffset,
        triangleMesh.indexOffset, triangleMesh.indexCount);
    RegisterMesh(&rendererFrontend, Mesh_Quad, quadMesh.vertexIndexOffset,
        quadMesh.indexOffset, quadMesh.indexCount);
    RegisterMesh(&rendererFrontend, Mesh_Cube, cubeMesh.vertexIndexOffset,
        cubeMesh.indexOffset, cubeMesh.indexCount);
    RegisterMesh(&rendererFrontend, Mesh_Sphere, sphereMesh.vertexIndexOffset,
        sphereMesh.indexOffset, sphereMesh.indexCount);

    Material *materials = (Material *)renderer.materialBuffer.data;
    materials[0].albedo = Vec3(0.18f, 0.18f, 0.18f);
    materials[0].roughness = 0.1f;

    mat4 *modelMatrices = (mat4 *)renderer.modelMatrixBuffer.data;
    modelMatrices[0] = Identity();
    modelMatrices[1] = Translate(Vec3(3, 0, 0));
    modelMatrices[2] = Scale(Vec3(100.0f));
    modelMatrices[3] = Translate(Vec3(3, 0, -3));
    modelMatrices[4] = Translate(Vec3(6, 0, 0));

    f64 startupTime = glfwGetTime();
    printf("START-UP TIME: %g seconds\n", startupTime);

    vec3 cameraPosition = Vec3(0, 0, 1);
    vec3 cameraRotation = Vec3(0, 0, 0);
    while (!glfwWindowShouldClose(window))
    {
        InputBeginFrame(&input);
        glfwPollEvents();

        if (handleWindowResize(window, &appState))
        {
            OnFramebufferResize(&rendererFrontend, appState.framebufferWidth,
                appState.framebufferHeight);
        }

        f32 dt = 1.0f / 60.0f;
        if (input.buttonStates[KEY_MOUSE_BUTTON_RIGHT].isDown)
        {
            cameraRotation = UpdateCameraRotation(cameraRotation, &input);
        }
        cameraPosition =
            UpdateCameraPosition(cameraPosition, cameraRotation, &input, dt);

        // TODO: Don't use ubo ID here, frontend should take actual model matrix as input
        DrawMeshCommand cmds[] = {
            {Mesh_Triangle, 0, Pipeline_Default, RenderPass_Scene},
            {Mesh_Quad, 1, Pipeline_Default, RenderPass_Scene},
            {Mesh_Cube, 2, Pipeline_Skybox, RenderPass_Scene},
            {Mesh_Quad, 3, Pipeline_Green, RenderPass_Scene},
            {Mesh_Sphere, 4, Pipeline_PbrMaterial, RenderPass_Scene},
            {Mesh_Quad, 0, Pipeline_PostProcessing, RenderPass_PostProcessing},
        };

        // Frontend translates high level draw cmds to backend draw commands
        RendererBackendCommand outputCmds[64] = {};
        u32 outputCount = TranslateCommands(&rendererFrontend, cmds,
            ARRAY_COUNT(cmds), outputCmds, ARRAY_COUNT(outputCmds));

        Camera camera = {};
        camera.position = cameraPosition;
        camera.rotation = cameraRotation;

        SetupMatrixBuffer(&renderer, camera);
        u32 imageIndex = StartFrame(&renderer);

        ExecuteRenderPass(&renderer, RenderPass_Scene, outputCmds, outputCount);
        ExecutePostProcessingRenderPass(&renderer, imageIndex);
        EndFrame(&renderer, imageIndex);

        //RenderFrame(&rendererFrontend, camera, cmds, ARRAY_COUNT(cmds));
    }

    glfwTerminate();

    return 0;
}
