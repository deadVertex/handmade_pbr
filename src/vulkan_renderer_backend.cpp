VkCommandBuffer CreateTemporaryCommandBuffer(
    VkDevice device, VkCommandPool commandPool);
void SubmitTemporaryCommandBuffer(VkDevice device,
    VkCommandBuffer commandBuffer, VkCommandPool commandPool, VkQueue queue);
//#include "vulkan_common.cpp"
#include "vulkan_images.cpp"
#include "vulkan_graphics_pipeline.cpp"
#include "vulkan_render_pass.cpp"

void StoreTexture(VulkanRenderer *renderer, u32 id, ImageState image)
{
    ASSERT_MACRO(renderer->textureCount < ARRAY_COUNT(renderer->textures));
    ImageState *entry = renderer->textures + renderer->textureCount++;
    *entry = image;
    entry->id = id;
}

ImageState *FindTexture(VulkanRenderer *renderer, u32 id)
{
    ImageState *result = NULL;
    for (u32 i = 0; i < renderer->textureCount; ++i)
    {
        if (renderer->textures[i].id == id)
        {
            result = renderer->textures + i;
            break;
        }
    }

    return result;
}

void StoreGraphicsPipeline(
    VulkanRenderer *renderer, u32 id, PipelineState state)
{
    ASSERT_MACRO(renderer->pipelineCount < ARRAY_COUNT(renderer->pipelines));
    PipelineState *entry = renderer->pipelines + renderer->pipelineCount++;
    *entry = state;
    entry->id = id;
}

PipelineState *FindGraphicsPipeline(VulkanRenderer *renderer, u32 id)
{
    PipelineState *result = NULL;
    for (u32 i = 0; i < renderer->pipelineCount; i++)
    {
        if (renderer->pipelines[i].id == id)
        {
            result = renderer->pipelines + i;
            break;
        }
    }

    return result;
}

void StoreRenderPass(VulkanRenderer *renderer, u32 id, RenderPassState renderPass)
{
    ASSERT_MACRO(renderer->renderPassCount < ARRAY_COUNT(renderer->renderPasses));
    RenderPassState *entry = renderer->renderPasses + renderer->renderPassCount++;
    *entry = renderPass;
    entry->id = id;
}

RenderPassState *FindRenderPass(VulkanRenderer *renderer, u32 id)
{
    RenderPassState *result = NULL;
    for (u32 i = 0; i < renderer->renderPassCount; ++i)
    {
        if (renderer->renderPasses[i].id == id)
        {
            result = renderer->renderPasses + i;
            break;
        }
    }

    return result;
}

void HandleCreateImage(VulkanRenderer *renderer, u32 id, u32 width, u32 height)
{
    ImageState imageState =
        CreateImageState(renderer->device, renderer->physicalDevice, width,
            height, VK_FORMAT_R32G32B32A32_SFLOAT,
            VK_IMAGE_USAGE_TRANSFER_DST_BIT | VK_IMAGE_USAGE_SAMPLED_BIT,
            VK_IMAGE_ASPECT_COLOR_BIT);
    StoreTexture(renderer, id, imageState);
}

void HandleUpdateImage(
    VulkanRenderer *renderer, u32 id, void *pixels, u32 length)
{
    ImageState *imageState = FindTexture(renderer, id);
    ASSERT_MACRO(imageState != NULL);
    UploadPixelDataToGpu(renderer, imageState, pixels, length);
}

void HandleCreateGraphicsPipeline(
    VulkanRenderer *renderer, CreateGraphicsPipelineCommand *cmd)
{
    RenderPassState *renderPass = FindRenderPass(renderer, cmd->renderPass);
    ASSERT_MACRO(renderPass != NULL);

    GraphicsPipelineDescription desc = {};
    desc.vertexShaderByteCode = (u32 *)cmd->vertexShaderCode;
    desc.vertexShaderLength = cmd->vertexShaderCodeLength;
    desc.fragmentShaderByteCode = (u32 *)cmd->fragmentShaderCode;
    desc.fragmentShaderLength = cmd->fragmentShaderCodeLength;

    PipelineState pipelineState =
        CreateGraphicsPipeline(renderer->device, renderer->pipelineCache,
            renderPass->handle, renderer->descriptorPool, desc);

    StoreGraphicsPipeline(renderer, cmd->id, pipelineState);
}

void HandleUpdateGraphicsPipelineResources(VulkanRenderer *renderer, u32 id)
{
    PipelineState *pipelineState = FindGraphicsPipeline(renderer, id);
    ASSERT_MACRO(pipelineState);

    writeDescriptorSet(renderer->device, pipelineState->descriptorSet,
        renderer->vertexBuffer.handle, renderer->matrixBuffer.handle,
        renderer->modelMatrixBuffer.handle, renderer->materialBuffer.handle);
}

void HandleCreateCubeMap(VulkanRenderer *renderer, CreateCubeMapCommand cmd)
{
    u32 usage = VK_IMAGE_USAGE_SAMPLED_BIT;
    if (cmd.flags & CreateCubeMapFlag_ComputeShaderOutput)
    {
        usage |= VK_IMAGE_USAGE_STORAGE_BIT;
    }
    else
    {
        // Assume image data will be transfered to it
        usage |= VK_IMAGE_USAGE_TRANSFER_DST_BIT;
    }

    ImageState image = CreateCubeMapImageState(renderer->device,
        renderer->physicalDevice, cmd.width, cmd.width,
        VK_FORMAT_R32G32B32A32_SFLOAT, usage, VK_IMAGE_ASPECT_COLOR_BIT);

    if (cmd.flags & CreateCubeMapFlag_ComputeShaderOutput)
    {
        // Change image layout so compute shader can write to it
        TransitionImageLayout(renderer->device, image.handle,
            VK_IMAGE_LAYOUT_UNDEFINED, VK_IMAGE_LAYOUT_GENERAL,
            renderer->commandPool, renderer->graphicsQueue);
        image.layout = VK_IMAGE_LAYOUT_GENERAL;
    }

    StoreTexture(renderer, cmd.id, image);
}

void HandleUpdateCubeMap(VulkanRenderer *renderer, UpdateCubeMapCommand *cmd)
{
    ImageState *image = FindTexture(renderer, cmd->id);
    ASSERT_MACRO(image != NULL);

    u32 totalSize = cmd->faceNumBytes * MAX_CUBE_MAP_FACES;
    u8 *dst = (u8 *)BeginTextureUpload(renderer, totalSize);
    for (u32 i = 0; i < MAX_CUBE_MAP_FACES; ++i)
    {
        COPY_MEMORY(dst, cmd->pixels[i], cmd->faceNumBytes);
        dst += cmd->faceNumBytes;
    }
    EndTextureUpload(renderer, image);
}

void HandleCreateComputePipeline(
    VulkanRenderer *renderer, CreateComputePipelineCommand cmd)
{
    PipelineState pipelineState = CreateComputePipeline(renderer->device,
        renderer->pipelineCache, renderer->descriptorPool,
        (u32 *)cmd.shaderCode, cmd.shaderCodeLength);

    // FIXME: Rename this to store pipeline?
    StoreGraphicsPipeline(renderer, cmd.id, pipelineState);
}

void HandleUpdateComputePipelineResources(
    VulkanRenderer *renderer, UpdateComputePipelineResourcesCommand cmd)
{
    ASSERT_MACRO(cmd.resourceCount == 2);
    ASSERT_MACRO(cmd.resources[0].type == PipelineResourceType_Texture);
    ASSERT_MACRO(cmd.resources[1].type == PipelineResourceType_Texture);

    ImageState *inputImage = FindTexture(renderer, cmd.resources[0].id);
    ASSERT_MACRO(inputImage != NULL);

    // FIXME: Bit yucky
    if (inputImage->layout == VK_IMAGE_LAYOUT_GENERAL)
    {
        TransitionImageLayout(renderer->device, inputImage->handle,
            VK_IMAGE_LAYOUT_GENERAL, VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL,
            renderer->commandPool, renderer->graphicsQueue);
        inputImage->layout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
    }

    ImageState *outputImage = FindTexture(renderer, cmd.resources[1].id);
    ASSERT_MACRO(outputImage != NULL);

    PipelineState *pipelineState = FindGraphicsPipeline(renderer, cmd.id);
    ASSERT_MACRO(pipelineState != NULL);

    WriteComputePipelineDescriptorSets(renderer->device,
        pipelineState->descriptorSet, inputImage->view,
        renderer->defaultSampler, outputImage->view);
}

void HandleDispatchComputeShader(
    VulkanRenderer *renderer, DispatchComputePipelineCommand cmd)
{
    PipelineState *pipelineState = FindGraphicsPipeline(renderer, cmd.id);
    ASSERT_MACRO(pipelineState != NULL);

    // FIXME: We are assuming that our graphicsQueue also supports compute, I
    // don't think we've ever checked this!
    DispatchComputeShader(renderer->device, renderer->commandPool,
        renderer->graphicsQueue, pipelineState->pipeline, pipelineState->layout,
        pipelineState->descriptorSet, cmd.groupCountX, cmd.groupCountY,
        cmd.groupCountZ);
}

void HandleCreateRenderPass(VulkanRenderer *renderer, CreateRenderPassCommand cmd)
{
    CreateRenderPassResult result =
        CreateRenderPass(renderer, cmd.width, cmd.height, cmd.texture);

    StoreRenderPass(renderer, cmd.id, result.renderPass);
    StoreTexture(renderer, cmd.texture, result.colorTexture);
}

void ProcessCommands(
    VulkanRenderer *renderer, RendererBackendCommand *cmds, u32 count)
{
    for (u32 i = 0; i < count; ++i)
    {
        RendererBackendCommand *cmd = cmds + i;
        switch (cmd->type)
        {
            case RendererBackendCommandType_CreateImage:
                HandleCreateImage(renderer, cmd->createImage.id,
                    cmd->createImage.width, cmd->createImage.height);
                break;
            case RendererBackendCommandType_UpdateImage:
                HandleUpdateImage(renderer, cmd->updateImage.id,
                        cmd->updateImage.pixels,
                        cmd->updateImage.length);
                break;
            case RendererBackendCommandType_CreateGraphicsPipeline:
                HandleCreateGraphicsPipeline(
                    renderer, &cmd->createGraphicsPipeline);
                break;
            case RendererBackendCommandType_UpdateGraphicsPipelineResources:
                HandleUpdateGraphicsPipelineResources(
                    renderer, cmd->updateGraphicsPipelineResources.id);
                break;
            case RendererBackendCommandType_CreateCubeMap:
                HandleCreateCubeMap(renderer, cmd->createCubeMap);
                break;
            case RendererBackendCommandType_UpdateCubeMap:
                HandleUpdateCubeMap(renderer, &cmd->updateCubeMap);
                break;
            case RendererBackendCommandType_CreateComputePipeline:
                HandleCreateComputePipeline(
                    renderer, cmd->createComputePipeline);
                break;
            case RendererBackendCommandType_UpdateComputePipelineResources:
                HandleUpdateComputePipelineResources(
                    renderer, cmd->updateComputePipelineResources);
                break;
            case RendererBackendCommandType_DispatchComputePipeline:
                HandleDispatchComputeShader(
                    renderer, cmd->dispatchComputePipeline);
                break;
            case RendererBackendCommandType_CreateRenderPass:
                HandleCreateRenderPass(renderer, cmd->createRenderPass);
                break;
            default:
                INVALID_CODE_PATH();
                break;
        }
    }
}

#ifndef IGNORE_FOR_UNIT_TESTING_FOR_NOW
//! Concatenate the list of requested extensions with the list of extensions
// required by GLFW
u32 buildRequestedExtensionsList(const char **requestedExtensions,
    u32 requestedCount, const char **extensions, u32 capacity)
{
    u32 glfwExtensionsCount = 0;
    const char **glfwExtensions =
        glfwGetRequiredInstanceExtensions(&glfwExtensionsCount);

    u32 count = 0;
    for (u32 i = 0; i < requestedCount; ++i)
    {
        ASSERT_MACRO(count < capacity);
        extensions[count++] = requestedExtensions[i];
    }

    for (u32 i = 0; i < glfwExtensionsCount; ++i)
    {
        ASSERT_MACRO(count < capacity);
        extensions[count++] = glfwExtensions[i];
    }

    return count;
}

void DumpAvailableExtensions()
{
    u32 count = 0;
    VK_CHECK(vkEnumerateInstanceExtensionProperties(NULL, &count, NULL));

    VkExtensionProperties properties[64] = {};
    ASSERT_MACRO(count < ARRAY_COUNT(properties));
    count = ARRAY_COUNT(properties);
    VK_CHECK(vkEnumerateInstanceExtensionProperties(NULL, &count, properties));

    printf("Available extensions: \n");
    for (u32 i = 0; i < count; ++i)
    {
        printf("%s\n", properties[i].extensionName);
    }
}

void DumpAvailableLayers()
{
    u32 count = 0;
    VK_CHECK(vkEnumerateInstanceLayerProperties(&count, NULL));

    VkLayerProperties properties[64] = {};
    ASSERT_MACRO(count < ARRAY_COUNT(properties));
    count = ARRAY_COUNT(properties);
    VK_CHECK(vkEnumerateInstanceLayerProperties(&count, properties));

    printf("Available layers: \n");
    for (u32 i = 0; i < count; ++i)
    {
        printf("%s - %s\n", properties[i].layerName, properties[i].description);
    }

}

//! Create a Vulkan 1.2 instance
VkInstance createInstance()
{
    // TODO: Check that this layer is supported on our hardware
    const char *validationLayers[] = {"VK_LAYER_KHRONOS_validation"};
    const char *extensions[MAX_SUPPORTED_EXTENSIONS] = {};

    // FUTURE: Parameterize this?
    const char *requestedExtensions[] = {VK_EXT_DEBUG_UTILS_EXTENSION_NAME};
    u32 extensionCount = buildRequestedExtensionsList(requestedExtensions,
        ARRAY_COUNT(requestedExtensions), extensions, ARRAY_COUNT(extensions));

    VkApplicationInfo appInfo = {VK_STRUCTURE_TYPE_APPLICATION_INFO};
    appInfo.apiVersion = VK_API_VERSION_1_2;

    VkInstanceCreateInfo createInfo = {VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO};
    createInfo.pApplicationInfo = &appInfo;
    createInfo.enabledExtensionCount = extensionCount;
    createInfo.ppEnabledExtensionNames = extensions;
    createInfo.enabledLayerCount = ARRAY_COUNT(validationLayers);
    createInfo.ppEnabledLayerNames = validationLayers;

    VkInstance instance = VK_NULL_HANDLE;
    VK_CHECK(vkCreateInstance(&createInfo, NULL, &instance));

    return instance;
}

//! Query the GPUs available on the system and pick the first one
VkPhysicalDevice pickPhysicalDevice(VkInstance instance)
{
    u32 gpuCount = MAX_SUPPORTED_GPUS;
    VkPhysicalDevice physicalDevices[MAX_SUPPORTED_GPUS];

    VK_CHECK(vkEnumeratePhysicalDevices(instance, &gpuCount, physicalDevices));

    // FUTURE: Allow user to specify which GPU to use
    return physicalDevices[0];
}

VkDevice createLogicalDevice(
    VkPhysicalDevice physicalDevice, u32 graphicsQueueFamilyIndex)
{
    f32 queuePriorities[] = {1.0f};

    VkDeviceQueueCreateInfo queueCreateInfo = {
        VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO};
    queueCreateInfo.queueFamilyIndex = graphicsQueueFamilyIndex;
    queueCreateInfo.queueCount = 1;
    queueCreateInfo.pQueuePriorities = queuePriorities;

    // TODO: May need to create multiple queues in the event that the graphics
    // queue family does not support presenting to the surface we created

    const char *extensions[] = {VK_KHR_SWAPCHAIN_EXTENSION_NAME};

    VkDeviceCreateInfo createInfo = {VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO};
    createInfo.queueCreateInfoCount = 1;
    createInfo.pQueueCreateInfos = &queueCreateInfo;
    createInfo.ppEnabledExtensionNames = extensions;
    createInfo.enabledExtensionCount = ARRAY_COUNT(extensions);

    VkDevice device;
    VK_CHECK(vkCreateDevice(physicalDevice, &createInfo, NULL, &device));
    return device;
}

VkSurfaceKHR createSurface(VkInstance instance, GLFWwindow *window)
{
    VkSurfaceKHR surface = VK_NULL_HANDLE;
    VK_CHECK(glfwCreateWindowSurface(instance, window, NULL, &surface));
    return surface;
}

u32 findGraphicsQueueFamily(VkPhysicalDevice physicalDevice)
{
    u32 familyIndex = U32_MAX;

    // NOTE: Using V1_0 version of vkGetPhysicalDeviceQueueFamilyProperties
    // here because we don't use any extensions that require us to use the
    // newer more verbose version of this function.
    u32 count = MAX_SUPPORTED_QUEUE_FAMILIES;
    VkQueueFamilyProperties families[MAX_SUPPORTED_QUEUE_FAMILIES] = {};
    vkGetPhysicalDeviceQueueFamilyProperties(physicalDevice, &count, families);
    for (u32 i = 0; i < count; i++)
    {
        // FUTURE: Check for other queue flags
        if (families[i].queueFlags & VK_QUEUE_GRAPHICS_BIT)
        {
            familyIndex = i;
            break;
        }
    }

    ASSERT_MACRO(familyIndex != U32_MAX);

    return familyIndex;
}

VkQueue getQueue(VkDevice device, u32 queueFamilyIndex, u32 queueIndex)
{
    VkDeviceQueueInfo2 queueInfo = {VK_STRUCTURE_TYPE_DEVICE_QUEUE_INFO_2};
    queueInfo.queueFamilyIndex = queueFamilyIndex;
    queueInfo.queueIndex = queueIndex;

    VkQueue queue;
    vkGetDeviceQueue2(device, &queueInfo, &queue);
    return queue;
}

bool checkSurfaceIsSupported(VkPhysicalDevice physicalDevice,
    VkSurfaceKHR surface, u32 graphicsQueueFamilyIndex)
{
    // FIXME: Query surface formats and present modes
    VkSurfaceCapabilitiesKHR surfaceCapabilities = {
        VK_STRUCTURE_TYPE_SURFACE_CAPABILITIES_2_KHR};
    VK_CHECK(vkGetPhysicalDeviceSurfaceCapabilitiesKHR(
        physicalDevice, surface, &surfaceCapabilities));

    VkSurfaceFormatKHR surfaceFormats[8];
    u32 surfaceFormatCount = ARRAY_COUNT(surfaceFormats);
    VK_CHECK(vkGetPhysicalDeviceSurfaceFormatsKHR(
        physicalDevice, surface, &surfaceFormatCount, surfaceFormats));

    b32 queueSupported = false;
    VK_CHECK(vkGetPhysicalDeviceSurfaceSupportKHR(
        physicalDevice, graphicsQueueFamilyIndex, surface, &queueSupported));

    return (
        (surfaceCapabilities.currentTransform &
            VK_SURFACE_TRANSFORM_IDENTITY_BIT_KHR) &&
        (surfaceFormats[0].format == VK_FORMAT_B8G8R8A8_UNORM) &&
        (surfaceFormats[0].colorSpace == VK_COLOR_SPACE_SRGB_NONLINEAR_KHR) &&
        queueSupported);
}

VkSwapchainKHR createSwapchain(VkDevice device, VkSurfaceKHR surface,
    u32 imageCount, VkFormat imageFormat, VkColorSpaceKHR colorSpace,
    u32 imageWidth, u32 imageHeight, VkPresentModeKHR presentMode)
{
    VkSwapchainCreateInfoKHR createInfo = {
        VK_STRUCTURE_TYPE_SWAPCHAIN_CREATE_INFO_KHR};
    createInfo.surface = surface;
    createInfo.minImageCount = imageCount;
    createInfo.imageFormat = imageFormat;
    createInfo.imageColorSpace = colorSpace;
    createInfo.imageExtent.width = imageWidth;
    createInfo.imageExtent.height = imageHeight;
    createInfo.imageArrayLayers = 1;
    createInfo.imageUsage = VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT;

    // FIXME: Don't assume graphicsQueue and presentQueue are the same, if they
    // differ we need to setup iamge sharing mode
    createInfo.presentMode = presentMode;
    createInfo.preTransform = VK_SURFACE_TRANSFORM_IDENTITY_BIT_KHR;
    createInfo.compositeAlpha = VK_COMPOSITE_ALPHA_OPAQUE_BIT_KHR;

    VkSwapchainKHR swapchain = VK_NULL_HANDLE;
    VK_CHECK(vkCreateSwapchainKHR(device, &createInfo, NULL, &swapchain));
    return swapchain;
}

void DestroyImageState(VkDevice device, ImageState image)
{
    vkDestroyImageView(device, image.view, NULL);
    vkDestroyImage(device, image.handle, NULL);
    vkFreeMemory(device, image.memory, NULL);
}

SwapchainState configureSwapchain(VkDevice device, VkSurfaceKHR surface,
    VkPhysicalDevice physicalDevice, VkRenderPass renderPass, u32 imageCount,
    VkFormat imageFormat, u32 imageWidth, u32 imageHeight)
{
    SwapchainState state = {};
    state.imageWidth = imageWidth;
    state.imageHeight = imageHeight;
    state.imageCount = imageCount;
    state.imageFormat = imageFormat;

    VkSwapchainKHR swapchain = createSwapchain(device, surface, imageCount,
        imageFormat, VK_COLOR_SPACE_SRGB_NONLINEAR_KHR, imageWidth, imageHeight,
        VK_PRESENT_MODE_FIFO_KHR);
    state.handle = swapchain;

    // Check that we got as many images for our swapchain as we requested
    // because the swapchain allocates the images for us to use which depending
    // on the present mode might be more or less than what we requested.
    u32 availableImageCount = 0;
    VK_CHECK(
        vkGetSwapchainImagesKHR(device, swapchain, &availableImageCount, NULL));
    ASSERT_MACRO(availableImageCount == imageCount);

    // Retrieve the images allocated by the swapchain for the color buffer
    VK_CHECK(
        vkGetSwapchainImagesKHR(device, swapchain, &imageCount, state.images));

    // Create the framebuffer for each image in our swapchain using the images
    // that were allocated for us when we created the swapchain
    for (u32 imageIndex = 0; imageIndex < imageCount; imageIndex++)
    {
        // A frame buffer requires an image view as an attachment which defines
        // how the framebuffer will write to the memory bound to the image.
        state.imageViews[imageIndex] = CreateImageView(device,
            state.images[imageIndex], imageFormat, VK_IMAGE_ASPECT_COLOR_BIT);

        state.depthImages[imageIndex] = CreateImageState(device, physicalDevice,
            imageWidth, imageHeight, VK_FORMAT_D32_SFLOAT,
            VK_IMAGE_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT,
            VK_IMAGE_ASPECT_DEPTH_BIT);

        VkImageView attachments[] = {
            state.imageViews[imageIndex], state.depthImages[imageIndex].view};

        state.framebuffers[imageIndex] = CreateFramebuffer(device, renderPass,
            attachments, ARRAY_COUNT(attachments), imageWidth, imageHeight);
    }

    return state;
}

// FIXME: Leaking memory for depth buffer
void unconfigureSwapchain(SwapchainState swapchainState, VkDevice device)
{
    // Release framebuffers and image views
    for (u32 i = 0; i < swapchainState.imageCount; i++)
    {
        vkDestroyFramebuffer(device, swapchainState.framebuffers[i], NULL);
        vkDestroyImageView(device, swapchainState.imageViews[i], NULL);
        DestroyImageState(device, swapchainState.depthImages[i]);
    }
    vkDestroySwapchainKHR(device, swapchainState.handle, NULL);
}

VkSemaphore createSemaphore(VkDevice device)
{
    VkSemaphoreCreateInfo createInfo = {
        VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO};
    VkSemaphore semaphore = VK_NULL_HANDLE;
    VK_CHECK(vkCreateSemaphore(device, &createInfo, NULL, &semaphore));
    return semaphore;
}

VkDescriptorPool createDescriptorPool(VkDevice device)
{
    VkDescriptorPoolSize poolSizes[] = {
        {VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER, MAX_UNIFORM_BUFFER_DESCRIPTORS},
        {VK_DESCRIPTOR_TYPE_STORAGE_BUFFER, MAX_STORAGE_BUFFER_DESCRIPTORS},
    };

    VkDescriptorPoolCreateInfo createInfo = {
        VK_STRUCTURE_TYPE_DESCRIPTOR_POOL_CREATE_INFO};
    createInfo.poolSizeCount = ARRAY_COUNT(poolSizes);
    createInfo.pPoolSizes = poolSizes;
    createInfo.maxSets = MAX_DESCRIPTOR_SETS;

    VkDescriptorPool pool = VK_NULL_HANDLE;
    VK_CHECK(vkCreateDescriptorPool(device, &createInfo, NULL, &pool));

    return pool;
}

VkCommandPool createCommandPool(VkDevice device, u32 queueFamilyIndex)
{
    VkCommandPoolCreateInfo createInfo = {
        VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO};

    // NOTE: Don't believe its correct to use say that command buffers
    // allocated from this pool are transient since we allocate them when we
    // create the renderer and hold onto them until we terminate the
    // application.
    // createInfo.flags = VK_COMMAND_POOL_CREATE_TRANSIENT_BIT;

    createInfo.queueFamilyIndex = queueFamilyIndex;

    VkCommandPool commandPool;
    VK_CHECK(vkCreateCommandPool(device, &createInfo, NULL, &commandPool));
    return commandPool;
}

VkCommandBuffer createCommandBuffer(VkDevice device, VkCommandPool commandPool)
{
    VkCommandBufferAllocateInfo allocateInfo = {
        VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO};
    allocateInfo.commandPool = commandPool;
    allocateInfo.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
    allocateInfo.commandBufferCount = 1;

    VkCommandBuffer commandBuffer;
    vkAllocateCommandBuffers(device, &allocateInfo, &commandBuffer);

    return commandBuffer;
}

void dumpMemoryTypes(VkPhysicalDevice physicalDevice)
{
    VkPhysicalDeviceMemoryProperties memoryProperties = {};
    vkGetPhysicalDeviceMemoryProperties(physicalDevice, &memoryProperties);

    printf("---- Memory Types ----\n");
    for (u32 i = 0; i < memoryProperties.memoryTypeCount; ++i)
    {
        VkMemoryPropertyFlags flags =
            memoryProperties.memoryTypes[i].propertyFlags;

        printf("%u: heapIndex: %u propertyFlags:\n", i, memoryProperties.memoryTypes[i].heapIndex);
        if (flags & VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT)
        {
            printf("\tVK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT\n");
        }
        if (flags & VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT)
        {
            printf("\tVK_MEMORY_PROPERTY_HOST_VISIBLE_BIT\n");
        }
        if (flags & VK_MEMORY_PROPERTY_HOST_COHERENT_BIT)
        {
            printf("\tVK_MEMORY_PROPERTY_HOST_COHERENT_BIT\n");
        }
        if (flags & VK_MEMORY_PROPERTY_HOST_CACHED_BIT)
        {
            printf("\tVK_MEMORY_PROPERTY_HOST_CACHED_BIT\n");
        }
        if (flags & VK_MEMORY_PROPERTY_LAZILY_ALLOCATED_BIT)
        {
            printf("\tVK_MEMORY_PROPERTY_LAZILY_ALLOCATED_BIT\n");
        }

        printf("\n\n");
    }
    printf("---- Memory Heaps ----\n");
    for (u32 i = 0; i < memoryProperties.memoryHeapCount; ++i)
    {
        VkMemoryHeap heap = memoryProperties.memoryHeaps[i];
        printf("%u: size: %llu Kb deviceLocal: %s\n", i, heap.size / 1024,
            (heap.flags & VK_MEMORY_HEAP_DEVICE_LOCAL_BIT) ? "true" : "false");
    }
    printf("\n\n");
}

BufferState createBuffer(VkDevice device, VkPhysicalDevice physicalDevice,
    u32 size, VkBufferUsageFlags usage)
{
    VkMemoryPropertyFlags memoryProperties =
        VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT |
        VK_MEMORY_PROPERTY_HOST_COHERENT_BIT;

    // Create the VkBuffer which will store the meta data
    VkBufferCreateInfo createInfo = {VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO};
    createInfo.size = size;
    createInfo.usage = usage;
    createInfo.sharingMode = VK_SHARING_MODE_EXCLUSIVE;

    VkBuffer buffer = VK_NULL_HANDLE;
    VK_CHECK(vkCreateBuffer(device, &createInfo, NULL, &buffer));

    // Choose which memory heap we will allocate this buffer from
    VkMemoryRequirements memoryRequirements = {};
    vkGetBufferMemoryRequirements(device, buffer, &memoryRequirements);

    u32 memoryTypeIndex = findMemoryType(
        physicalDevice, memoryRequirements.memoryTypeBits, memoryProperties);

    // Allocate the memory
    VkMemoryAllocateInfo allocInfo = {VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO};
    allocInfo.allocationSize = memoryRequirements.size;
    allocInfo.memoryTypeIndex = memoryTypeIndex;

    VkDeviceMemory bufferMemory = VK_NULL_HANDLE;
    VK_CHECK(vkAllocateMemory(device, &allocInfo, NULL, &bufferMemory));

    // Bind the memory to our buffer
    vkBindBufferMemory(device, buffer, bufferMemory, 0);

    // ASSUMPTION: all buffers use host-visible memory for now so we can map it
    ASSERT_MACRO(memoryProperties & VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT);

    void *data = NULL;
    VK_CHECK(vkMapMemory(device, bufferMemory, 0, VK_WHOLE_SIZE, 0, &data));

    BufferState result = {};
    result.handle = buffer;
    result.memory = bufferMemory;
    result.data = data;
    result.capacity = size;

    return result;
}

VkSampler CreateDefaultSampler(VkDevice device)
{
    VkSamplerCreateInfo samplerCreateInfo = {
        VK_STRUCTURE_TYPE_SAMPLER_CREATE_INFO};
    samplerCreateInfo.magFilter = VK_FILTER_LINEAR;
    samplerCreateInfo.minFilter = VK_FILTER_LINEAR;
    samplerCreateInfo.addressModeU = VK_SAMPLER_ADDRESS_MODE_REPEAT;
    samplerCreateInfo.addressModeV = VK_SAMPLER_ADDRESS_MODE_REPEAT;
    samplerCreateInfo.addressModeW = VK_SAMPLER_ADDRESS_MODE_REPEAT;
    //samplerCreateInfo.anisotropyEnable = VK_TRUE;
    samplerCreateInfo.maxAnisotropy = 16.0f;
    samplerCreateInfo.borderColor = VK_BORDER_COLOR_INT_OPAQUE_BLACK;
    samplerCreateInfo.unnormalizedCoordinates = VK_FALSE;
    samplerCreateInfo.compareEnable = VK_FALSE;
    samplerCreateInfo.compareOp = VK_COMPARE_OP_ALWAYS;
    samplerCreateInfo.mipmapMode = VK_SAMPLER_MIPMAP_MODE_LINEAR;
    samplerCreateInfo.mipLodBias = 0.0f;
    samplerCreateInfo.minLod = 0.0f;
    samplerCreateInfo.maxLod = 1000.0f;

    VkSampler sampler = VK_NULL_HANDLE;
    VK_CHECK(vkCreateSampler(device, &samplerCreateInfo, NULL, &sampler));
    return sampler;
}

void writeDescriptorSetsForTexture(VkDevice device, VkDescriptorSet descriptorSet,
        VkImageView view, VkSampler sampler)
{
    VkDescriptorImageInfo imageInfo = {};
    imageInfo.sampler = sampler;
    imageInfo.imageView = view;
    imageInfo.imageLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;

    VkWriteDescriptorSet descriptorWrites[1] = {};
    {
        descriptorWrites[0].sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
        descriptorWrites[0].dstSet = descriptorSet;
        descriptorWrites[0].dstBinding = 3;
        descriptorWrites[0].descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
        descriptorWrites[0].descriptorCount = 1;
        descriptorWrites[0].pImageInfo = &imageInfo;
    }

    vkUpdateDescriptorSets(
        device, ARRAY_COUNT(descriptorWrites), descriptorWrites, 0, NULL);
}

void BindTextureToPipeline(VulkanRenderer *renderer, u32 pipeline, u32 textureId)
{
    // Update descriptor sets
    PipelineState *pipelineState = FindGraphicsPipeline(renderer, pipeline);
    ASSERT_MACRO(pipelineState != NULL);

    ImageState *imageState = FindTexture(renderer, textureId);
    ASSERT_MACRO(imageState != NULL);

    // FIXME: Bit of a gross hack
    if (imageState->layout == VK_IMAGE_LAYOUT_GENERAL)
    {
        TransitionImageLayout(renderer->device, imageState->handle,
            VK_IMAGE_LAYOUT_GENERAL, VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL,
            renderer->commandPool, renderer->graphicsQueue);
        imageState->layout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
    }

    writeDescriptorSetsForTexture(renderer->device,
        pipelineState->descriptorSet, imageState->view,
        renderer->defaultSampler);

    // TODO: Support multiple textures
    pipelineState->boundTexture = textureId;
}

//! Create and initialize a vulkan renderer struct
VulkanRenderer createVulkanRenderer(GLFWwindow *window)
{
    VkInstance instance = createInstance();
    VkPhysicalDevice physicalDevice = pickPhysicalDevice(instance);
    dumpMemoryTypes(physicalDevice);

    VkSurfaceKHR surface = createSurface(instance, window);

    u32 graphicsQueueFamilyIndex = findGraphicsQueueFamily(physicalDevice);
    ASSERT_MACRO(checkSurfaceIsSupported(
        physicalDevice, surface, graphicsQueueFamilyIndex));
    // TODO: Find presentation queue family

    VkDevice device =
        createLogicalDevice(physicalDevice, graphicsQueueFamilyIndex);

    // TODO: Don't assume graphics and present queue are from the same family
    VkQueue graphicsQueue = getQueue(device, graphicsQueueFamilyIndex, 0);
    VkQueue presentQueue = graphicsQueue;

    VkRenderPass renderPass = CreateRenderPass(device, VK_FORMAT_B8G8R8A8_UNORM,
        VK_IMAGE_LAYOUT_PRESENT_SRC_KHR);

    VkSurfaceCapabilitiesKHR surfaceCapabilities = {
        VK_STRUCTURE_TYPE_SURFACE_CAPABILITIES_2_KHR};
    VK_CHECK(vkGetPhysicalDeviceSurfaceCapabilitiesKHR(
        physicalDevice, surface, &surfaceCapabilities));

    u32 imageCount = 2;
    u32 imageWidth = surfaceCapabilities.currentExtent.width;
    u32 imageHeight = surfaceCapabilities.currentExtent.height;
    VkFormat imageFormat = VK_FORMAT_B8G8R8A8_UNORM;

    SwapchainState swapchainState =
        configureSwapchain(device, surface, physicalDevice, renderPass,
            imageCount, imageFormat, imageWidth, imageHeight);

    VkDescriptorPool descriptorPool = createDescriptorPool(device);

    VkCommandPool commandPool =
        createCommandPool(device, graphicsQueueFamilyIndex);

    VkCommandBuffer commandBuffer = createCommandBuffer(device, commandPool);

    // Create buffers
    BufferState vertexBuffer = createBuffer(device, physicalDevice,
        VERTEX_BUFFER_SIZE, VK_BUFFER_USAGE_STORAGE_BUFFER_BIT);
    StackAllocator vertexAllocator =
        CreateStackAllocator(vertexBuffer.data, vertexBuffer.capacity);

    BufferState matrixBuffer = createBuffer(device, physicalDevice,
        sizeof(MatrixBuffer), VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT);

    BufferState modelMatrixBuffer = createBuffer(device, physicalDevice,
        MODEL_MATRIX_BUFFER_SIZE, VK_BUFFER_USAGE_STORAGE_BUFFER_BIT);

    BufferState indexBuffer = createBuffer(device, physicalDevice,
            INDEX_BUFFER_SIZE, VK_BUFFER_USAGE_INDEX_BUFFER_BIT);
    StackAllocator indexAllocator =
        CreateStackAllocator(indexBuffer.data, indexBuffer.capacity);

    BufferState textureUploadBuffer = createBuffer(device, physicalDevice,
        TEXTURE_UPLOAD_BUFFER_SIZE, VK_BUFFER_USAGE_TRANSFER_SRC_BIT);
    StackAllocator textureUploadAllocator = CreateStackAllocator(
        textureUploadBuffer.data, textureUploadBuffer.capacity);

    BufferState materialBuffer = createBuffer(device, physicalDevice,
        MATERIAL_BUFFER_SIZE, VK_BUFFER_USAGE_STORAGE_BUFFER_BIT);

    VkPipelineCache pipelineCache = CreatePipelineCache(device);

    VulkanRenderer renderer = {};
    renderer.instance = instance;
    renderer.physicalDevice = physicalDevice;
    renderer.device = device;
    renderer.surface = surface;
    renderer.swapchainState = swapchainState;
    renderer.acquireSemaphore = createSemaphore(device);
    renderer.releaseSemaphore = createSemaphore(device);
    renderer.descriptorPool = descriptorPool;
    renderer.commandPool = commandPool;
    renderer.commandBuffer = commandBuffer;
    renderer.graphicsQueue = graphicsQueue;
    renderer.presentQueue = presentQueue;
    renderer.vertexBuffer = vertexBuffer;
    renderer.indexBuffer = indexBuffer;
    renderer.matrixBuffer = matrixBuffer;
    renderer.modelMatrixBuffer = modelMatrixBuffer;
    renderer.textureUploadBuffer = textureUploadBuffer;
    renderer.materialBuffer = materialBuffer;
    renderer.renderPass = renderPass;
    renderer.pipelineCache = pipelineCache;
    renderer.defaultSampler = CreateDefaultSampler(device);

    renderer.vertexAllocator = vertexAllocator;
    renderer.indexAllocator = indexAllocator;
    renderer.textureUploadAllocator = textureUploadAllocator;

    // Add dummy render pass for Post Processing
    RenderPassState postProcessingRenderPass = {};
    postProcessingRenderPass.handle = renderPass;
    postProcessingRenderPass.framebuffer = VK_NULL_HANDLE;
    StoreRenderPass(
        &renderer, RenderPass_PostProcessing, postProcessingRenderPass);

    return renderer;
}

void PopulateMatrixBuffer(
    MatrixBuffer *matrixBuffer, f32 framebufferWidth, f32 framebufferHeight,
    Camera camera)
{
    // Vulkan specific correction matrix
    mat4 correctionMatrix = {};
    correctionMatrix.columns[0] = Vec4(1, 0, 0, 0);
    correctionMatrix.columns[1] = Vec4(0, -1, 0, 0);
    correctionMatrix.columns[2] = Vec4(0, 0, 0.5f, 0);
    correctionMatrix.columns[3] = Vec4(0, 0, 0.5f, 1);

    f32 cameraFov = 80.0f;
    f32 nearClip = 0.001f;
    f32 farClip = 100.0f;
    f32 aspect = framebufferWidth / framebufferHeight;

    // Setup view and projection matrix
    matrixBuffer->viewMatrix[DEFAULT_CAMERA_INDEX] =
        RotateX(-camera.rotation.x) * RotateY(-camera.rotation.y) *
        Translate(-camera.position);
    matrixBuffer->projectionMatrix[DEFAULT_CAMERA_INDEX] =
        correctionMatrix * Perspective(cameraFov, aspect, nearClip, farClip);

    matrixBuffer->viewMatrix[SKYBOX_CAMERA_INDEX] =
        RotateX(-camera.rotation.x) * RotateY(-camera.rotation.y);
    matrixBuffer->projectionMatrix[SKYBOX_CAMERA_INDEX] =
        correctionMatrix * Perspective(cameraFov, aspect, nearClip, farClip);

    matrixBuffer->cameraPositions[DEFAULT_CAMERA_INDEX] = camera.position;
}

u32 ExtractAndFilterDrawMeshCommands(u32 renderPass,
    RendererBackendCommand *inputCmds, u32 inputCount, BackendDrawMeshCommand *outputCmds,
    u32 outputCapacity)
{
    u32 outputCount = 0;
    for (u32 i = 0; i < inputCount; ++i)
    {
        RendererBackendCommand *cmd = inputCmds + i;
        if (cmd->type == RendererBackendCommandType_DrawMesh)
        {
            if (cmd->drawMesh.renderPass == renderPass)
            {
                ASSERT_MACRO(outputCount < outputCapacity);
                BackendDrawMeshCommand *out = outputCmds + outputCount++;
                *out = cmd->drawMesh;
            }
        }
    }

    return outputCount;
}

void ExecuteRenderPass(VulkanRenderer *renderer, u32 renderPassId,
        RendererBackendCommand *cmds, u32 cmdCount)
{
    RenderPassState *renderPass = FindRenderPass(renderer, renderPassId);
    ASSERT_MACRO(renderPass != NULL);

    BackendDrawMeshCommand drawMeshCmds[64] = {};
    u32 drawMeshCmdsCount = ExtractAndFilterDrawMeshCommands(renderPassId,
        cmds, cmdCount, drawMeshCmds, ARRAY_COUNT(drawMeshCmds));

    // Begin HDR render pass
    {
        VkClearValue clearValues[2] = {};
        clearValues[0].color = {0.08f, 0.01f, 0.04f, 1.0f};
        clearValues[1].depthStencil = {1.0f, 0};

        VkRenderPassBeginInfo renderPassBegin = {
            VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO};
        renderPassBegin.renderPass = renderPass->handle;
        renderPassBegin.framebuffer = renderPass->framebuffer;
        renderPassBegin.renderArea.extent.width = renderPass->width;
        renderPassBegin.renderArea.extent.height = renderPass->height;
        renderPassBegin.clearValueCount = ARRAY_COUNT(clearValues);
        renderPassBegin.pClearValues = clearValues;
        vkCmdBeginRenderPass(renderer->commandBuffer, &renderPassBegin,
            VK_SUBPASS_CONTENTS_INLINE);

        // Set dynamic pipeline state
        VkViewport viewport = {
            0, 0, (f32)renderPass->width, (f32)renderPass->height, 0.0f, 1.0f};
        VkRect2D scissor = {0, 0, renderPass->width, renderPass->height};
        vkCmdSetViewport(renderer->commandBuffer, 0, 1, &viewport);
        vkCmdSetScissor(renderer->commandBuffer, 0, 1, &scissor);
    }

    vkCmdBindIndexBuffer(renderer->commandBuffer, renderer->indexBuffer.handle,
        0, VK_INDEX_TYPE_UINT32);

    for (u32 cmdIndex = 0; cmdIndex < drawMeshCmdsCount; cmdIndex++)
    {
        BackendDrawMeshCommand cmd = drawMeshCmds[cmdIndex];

        // TODO: Shaders need to keep track of which render pass they support

        PipelineState *pipelineState =
            FindGraphicsPipeline(renderer, cmd.pipeline);
        ASSERT_MACRO(pipelineState != NULL);

        vkCmdBindDescriptorSets(renderer->commandBuffer,
            VK_PIPELINE_BIND_POINT_GRAPHICS, pipelineState->layout, 0, 1,
            &pipelineState->descriptorSet, 0, NULL);

        vkCmdBindPipeline(renderer->commandBuffer,
            VK_PIPELINE_BIND_POINT_GRAPHICS, pipelineState->pipeline);

        PushConstants pushConstants = {};
        pushConstants.vertexIndexOffset = cmd.vertexIndexOffset;
        pushConstants.modelMatrixIndex = cmd.modelMatrixIndex;
        pushConstants.materialIndex = 0;
        vkCmdPushConstants(renderer->commandBuffer, pipelineState->layout,
            VK_SHADER_STAGE_VERTEX_BIT | VK_SHADER_STAGE_FRAGMENT_BIT, 0,
            sizeof(pushConstants), &pushConstants);
        vkCmdDrawIndexed(
            renderer->commandBuffer, cmd.indexCount, 1, cmd.indexOffset, 0, 0);
    }

    vkCmdEndRenderPass(renderer->commandBuffer);
}

void SetupMatrixBuffer(VulkanRenderer *renderer, Camera camera)
{
    u32 framebufferWidth = renderer->swapchainState.imageWidth;
    u32 framebufferHeight = renderer->swapchainState.imageHeight;

    MatrixBuffer *matrixBuffer = (MatrixBuffer *)renderer->matrixBuffer.data;
    PopulateMatrixBuffer(
        matrixBuffer, (f32)framebufferWidth, (f32)framebufferHeight, camera);
}

u32 StartFrame(VulkanRenderer *renderer)
{
    u32 imageIndex = 0;
    VK_CHECK(
        vkAcquireNextImageKHR(renderer->device, renderer->swapchainState.handle,
            0, renderer->acquireSemaphore, VK_NULL_HANDLE, &imageIndex));

    VK_CHECK(vkResetCommandPool(renderer->device, renderer->commandPool, 0));

    VkCommandBufferBeginInfo beginInfo = {
        VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO};
    beginInfo.flags = VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT;
    VK_CHECK(vkBeginCommandBuffer(renderer->commandBuffer, &beginInfo));

    return imageIndex;
}

void ExecutePostProcessingRenderPass(VulkanRenderer *renderer, u32 imageIndex)
{
    u32 framebufferWidth = renderer->swapchainState.imageWidth;
    u32 framebufferHeight = renderer->swapchainState.imageHeight;

    // TODO: Create render pass object for this too!
    // Begin Post processing render pass
    {
        VkClearValue clearValues[2] = {};
        clearValues[0].color = {0.0f, 0.0f, 0.0f, 1.0f};
        clearValues[1].depthStencil = {1.0f, 0};

        VkRenderPassBeginInfo renderPassBegin = {
            VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO};
        renderPassBegin.renderPass = renderer->renderPass;
        renderPassBegin.framebuffer = renderer->swapchainState.framebuffers[imageIndex];
        renderPassBegin.renderArea.extent.width = framebufferWidth;
        renderPassBegin.renderArea.extent.height = framebufferHeight;
        renderPassBegin.clearValueCount = ARRAY_COUNT(clearValues);
        renderPassBegin.pClearValues = clearValues;
        vkCmdBeginRenderPass(renderer->commandBuffer, &renderPassBegin,
            VK_SUBPASS_CONTENTS_INLINE);

        // Set dynamic pipeline state
        VkViewport viewport = {
            0, 0, (f32)framebufferWidth, (f32)framebufferHeight, 0.0f, 1.0f};
        VkRect2D scissor = {0, 0, framebufferWidth, framebufferHeight};
        vkCmdSetViewport(renderer->commandBuffer, 0, 1, &viewport);
        vkCmdSetScissor(renderer->commandBuffer, 0, 1, &scissor);
    }

    // Bind post processing pipeline
    PipelineState *pipelineState =
        FindGraphicsPipeline(renderer, Pipeline_PostProcessing);
    ASSERT_MACRO(pipelineState != NULL);

    vkCmdBindDescriptorSets(renderer->commandBuffer,
            VK_PIPELINE_BIND_POINT_GRAPHICS, pipelineState->layout, 0, 1,
            &pipelineState->descriptorSet, 0, NULL);

    vkCmdBindPipeline(renderer->commandBuffer,
            VK_PIPELINE_BIND_POINT_GRAPHICS, pipelineState->pipeline);

    vkCmdDraw(renderer->commandBuffer, 6, 1, 0, 0);

    vkCmdEndRenderPass(renderer->commandBuffer);
}

void EndFrame(VulkanRenderer *renderer, u32 imageIndex)
{
    VK_CHECK(vkEndCommandBuffer(renderer->commandBuffer));

    // Submit command buffer to GPU
    VkPipelineStageFlags submitStageMask =
        VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;

    VkSubmitInfo submitInfo = {VK_STRUCTURE_TYPE_SUBMIT_INFO};
    submitInfo.waitSemaphoreCount = 1;
    submitInfo.pWaitSemaphores = &renderer->acquireSemaphore;
    submitInfo.pWaitDstStageMask = &submitStageMask;
    submitInfo.commandBufferCount = 1;
    submitInfo.pCommandBuffers = &renderer->commandBuffer;
    submitInfo.signalSemaphoreCount = 1;
    submitInfo.pSignalSemaphores = &renderer->releaseSemaphore;
    VK_CHECK(vkQueueSubmit(renderer->graphicsQueue, 1, &submitInfo, VK_NULL_HANDLE));

    VkPresentInfoKHR presentInfo = {VK_STRUCTURE_TYPE_PRESENT_INFO_KHR};
    presentInfo.swapchainCount = 1;
    presentInfo.waitSemaphoreCount = 1;
    presentInfo.pWaitSemaphores = &renderer->releaseSemaphore;
    presentInfo.pSwapchains = &renderer->swapchainState.handle;
    presentInfo.pImageIndices = &imageIndex;
    VK_CHECK(vkQueuePresentKHR(renderer->presentQueue, &presentInfo));

    // FIXME: Remove this to allow CPU and GPU to run in parallel
    VK_CHECK(vkDeviceWaitIdle(renderer->device));
}

void OnFramebufferResize(VulkanRenderer *renderer, u32 width, u32 height)
{
    // Make doubly sure no resources are still in use
    vkDeviceWaitIdle(renderer->device);

    // Save imageCount and imageFormat for use later
    u32 imageCount = renderer->swapchainState.imageCount;
    VkFormat imageFormat = renderer->swapchainState.imageFormat;

    // Destroy current swapchain
    unconfigureSwapchain(renderer->swapchainState, renderer->device);

    // Create new one with the new dimensions
    renderer->swapchainState = configureSwapchain(renderer->device,
        renderer->surface, renderer->physicalDevice, renderer->renderPass,
        imageCount, imageFormat, width, height);

    for (u32 i = 0; i < renderer->renderPassCount; ++i)
    {
        RenderPassState *renderPass = renderer->renderPasses + i;

        if (renderPass->framebuffer != VK_NULL_HANDLE)
        {
            // Delete framebuffer
            vkDestroyFramebuffer(
                renderer->device, renderPass->framebuffer, NULL);
            // Delete attachments
            DestroyImageState(renderer->device, renderPass->color);
            DestroyImageState(renderer->device, renderPass->depth);

            renderPass->width = width;
            renderPass->height = height;

            // Recreate attachments
            renderPass->color =
                CreateImageState(renderer->device, renderer->physicalDevice,
                    renderPass->width, renderPass->height, renderPass->format,
                    VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT |
                        VK_IMAGE_USAGE_SAMPLED_BIT,
                    VK_IMAGE_ASPECT_COLOR_BIT);

            renderPass->depth =
                CreateImageState(renderer->device, renderer->physicalDevice,
                    renderPass->width, renderPass->height, VK_FORMAT_D32_SFLOAT,
                    VK_IMAGE_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT |
                        VK_IMAGE_USAGE_SAMPLED_BIT,
                    VK_IMAGE_ASPECT_DEPTH_BIT);

            // Recreate framebuffer
            VkImageView attachments[] = {
                renderPass->color.view, renderPass->depth.view};

            // Create framebuffer from the attachments
            renderPass->framebuffer = CreateFramebuffer(renderer->device,
                renderPass->handle, attachments, ARRAY_COUNT(attachments),
                renderPass->width, renderPass->height);

            // Update stored imageState for textures
            ImageState *imageState =
                FindTexture(renderer, renderPass->textureId);
            ASSERT_MACRO(imageState != NULL);
            *imageState = renderPass->color;

            // Find all pipelines which use this textureId
            for (u32 pipelineIdx = 0; pipelineIdx < renderer->pipelineCount;
                 ++pipelineIdx)
            {
                PipelineState *pipelineState =
                    renderer->pipelines + pipelineIdx;
                if (pipelineState->boundTexture == renderPass->textureId)
                {
                    // Need to update descriptor sets since we're deleting the
                    // image views!
                    writeDescriptorSetsForTexture(renderer->device,
                        pipelineState->descriptorSet, imageState->view,
                        renderer->defaultSampler);
                }
            }
        }
    }
}
#endif
