struct Vertex
{
    float px, py, pz;
    float nx, ny, nz;
    float tu, tv;
};

layout(binding = 0) readonly buffer Vertices
{
    Vertex vertices[];
};

#define MAX_CAMERAS 4
#define DEFAULT_CAMERA_INDEX 0
#define SKYBOX_CAMERA_INDEX 1

layout(binding = 1) uniform Camera {
    mat4 viewMatrices[MAX_CAMERAS];
    mat4 projectionMatrices[MAX_CAMERAS];
    vec3 cameraPositions[MAX_CAMERAS];
};

layout(binding = 2) readonly buffer ModelMatrices
{
    mat4 modelMatrices[];
};

layout(location=0) out vec2 fragTextureCoords;
layout(location=1) out vec3 fragLocalPosition;
layout(location=2) out vec3 fragNormal;
layout(location=3) out vec3 fragWorldPosition;
layout(location=4) out flat vec3 fragCameraPosition;
