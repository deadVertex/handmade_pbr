#include "rng.h"

TEST_GROUP(RNG);

TEST_SETUP(RNG)
{
}

TEST_TEAR_DOWN(RNG)
{
}

TEST(RNG, XorShift32)
{
    RandomNumberGenerator rng = {123456};
    u32 x = XorShift32(&rng);
    TEST_ASSERT_NOT_EQUAL(123456, x);
}

TEST(RNG, RandomUnilateral)
{
    RandomNumberGenerator rng = {123456};
    f32 x = RandomUnilateral(&rng);
    TEST_ASSERT_TRUE(x >= 0.0f);
    TEST_ASSERT_TRUE(x < 1.0f);
}

TEST(RNG, RandomBilateral)
{
    RandomNumberGenerator rng = {123456};
    f32 x = RandomBilateral(&rng);
    TEST_ASSERT_TRUE(x >= -1.0f);
    TEST_ASSERT_TRUE(x < 1.0f);
}

TEST_GROUP_RUNNER(RNG)
{
    RUN_TEST_CASE(RNG, XorShift32);
    RUN_TEST_CASE(RNG, RandomUnilateral);
    RUN_TEST_CASE(RNG, RandomBilateral);
}
