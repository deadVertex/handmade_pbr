#pragma once

struct StackAllocator
{
    void *base;
    u64 size;
    u64 capacity;
};

//! Creates a new stack allocator using the memory provided. This memory must
//persist for the entire lifetime of the stack allocator.
inline StackAllocator CreateStackAllocator(void *base, u64 capacity)
{
    StackAllocator result = {};
    result.base = base;
    result.capacity = capacity;
    return result;
}

//! Allocate 'size' contiguous bytes from the memory managed by the stack
//allocator. Returns NULL if unable to allocate requested memory.
inline void* AllocateBytes(StackAllocator *allocator, u64 size)
{
    void *result = NULL;
    if (allocator->size + size <= allocator->capacity)
    {
        result = (u8 *)allocator->base + allocator->size;
        allocator->size += size;
    }
    return result;
}

inline void ResetStackAllocator(StackAllocator *allocator)
{
    allocator->size = 0;
}

#define ALLOCATE_ARRAY(ALLOCATOR, TYPE, COUNT) \
    (TYPE *)AllocateBytes(ALLOCATOR, sizeof(TYPE) * (COUNT))
