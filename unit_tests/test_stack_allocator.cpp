#include "stack_allocator.h"

TEST_GROUP(StackAllocator);

TEST_SETUP(StackAllocator)
{
}

TEST_TEAR_DOWN(StackAllocator)
{
}

TEST(StackAllocator, AllocateBytes)
{
    u8 buffer[32];
    StackAllocator allocator = CreateStackAllocator(buffer, sizeof(buffer));

    void *bytes = AllocateBytes(&allocator, 4);
    TEST_ASSERT_NOT_NULL(bytes);
}

TEST(StackAllocator, AllocateBytesContiguously)
{
    u8 buffer[32];
    StackAllocator allocator = CreateStackAllocator(buffer, sizeof(buffer));

    void *first = AllocateBytes(&allocator, 4);
    void *second = AllocateBytes(&allocator, 4);

    TEST_ASSERT_EQUAL(4, (u8*)second - (u8*)first);
}

TEST(StackAllocator, ReturnsNullUnableToMakeAllocation)
{
    u8 buffer[32];
    StackAllocator allocator = CreateStackAllocator(buffer, sizeof(buffer));

    void *invalid = AllocateBytes(&allocator, 33);
    TEST_ASSERT_NULL(invalid);
}

TEST(StackAllocator, Reset)
{
    // Given an allocator with memory allocated
    u8 buffer[32];
    StackAllocator allocator = CreateStackAllocator(buffer, sizeof(buffer));

    void *first = AllocateBytes(&allocator, 4);

    // When we reset the allocator
    ResetStackAllocator(&allocator);

    // Then we start allocating from the beginning of the buffer
    void *second = AllocateBytes(&allocator, 4);
    TEST_ASSERT_EQUAL_PTR(first, second);
}

TEST_GROUP_RUNNER(StackAllocator)
{
    RUN_TEST_CASE(StackAllocator, AllocateBytes);
    RUN_TEST_CASE(StackAllocator, AllocateBytesContiguously);
    RUN_TEST_CASE(StackAllocator, ReturnsNullUnableToMakeAllocation);
    RUN_TEST_CASE(StackAllocator, Reset);
}
