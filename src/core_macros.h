#pragma once

#ifdef ENABLE_ASSERTIONS
#define ASSERT_MACRO assert
#else
#define ASSERT_MACRO
#endif
#define COPY_MEMORY memcpy
#define ARRAY_COUNT(ARRAY) (sizeof(ARRAY) / sizeof(ARRAY[0]))
#define KILOBYTES(X) (X * 1024)
#define MEGABYTES(X) (X * 1024 * 1024)
#define INVALID_CODE_PATH() (ASSERT_MACRO(!"Invalid code path"))

