#pragma once

struct HdrImage
{
    f32 *pixels;
    u32 width;
    u32 height;
};
