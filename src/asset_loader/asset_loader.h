#pragma once

// NOTE: Need to call free() on image->pixels when done
int LoadExrImage(HdrImage *image, const char *path);
