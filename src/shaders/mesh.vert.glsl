#version 450
#extension GL_GOOGLE_include_directive : require

#include "common.glsl"
#include "common_vert.glsl"

void main()
{
    mat4 viewMatrix = viewMatrices[DEFAULT_CAMERA_INDEX];
    mat4 projectionMatrix = projectionMatrices[DEFAULT_CAMERA_INDEX];
    mat4 modelMatrix = modelMatrices[modelMatrixIndex];

    Vertex vertex = vertices[gl_VertexIndex + vertexIndexOffset];

    vec3 position = vec3(vertex.px, vertex.py, vertex.pz);
    vec3 normal = vec3(vertex.nx, vertex.ny, vertex.nz);
    vec2 textureCoords = vec2(vertex.tu, vertex.tv);

    fragTextureCoords = textureCoords;

    // TODO: Transform by inverse model matrix
    fragNormal = normal;

    fragWorldPosition = vec3(modelMatrix * vec4(position, 1.0));
    fragCameraPosition = cameraPositions[DEFAULT_CAMERA_INDEX];

    gl_Position =
        projectionMatrix * viewMatrix * modelMatrix * vec4(position, 1.0);
}
