#version 450

layout(location = 0) out vec4 outputColor;

layout(binding = 3) uniform sampler2D albedoTexture;

layout(location=0) in vec2 fragTextureCoords;

vec3 ReinhardToneMapping(vec3 color)
{
    color *= 0.4; // exposure adjustment

    color = color / (vec3(1.0) + color);

    // Perfrom linear to sRGB conversion
    return pow(color, vec3(1.0 / 2.2));
}

void main()
{
    vec3 textureColor = texture(albedoTexture, fragTextureCoords).rgb;
    vec3 tonemappedColor = ReinhardToneMapping(textureColor);
    outputColor = vec4(tonemappedColor, 1);
}
