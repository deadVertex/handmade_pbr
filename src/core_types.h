#pragma once

#include <cstdint>

typedef uint8_t u8;
typedef uint16_t u16;
typedef int32_t i32;
typedef uint32_t u32;
typedef uint64_t u64;
typedef int64_t i64;
typedef float f32;
typedef double f64;
typedef u32 b32;

typedef float f32;
typedef double f64;

#define U32_MAX 0xffffffff
