RendererFrontend CreateRendererFrontend(RendererBackend backend)
{
    RendererFrontend result = {};
    result.backend = backend;
    return result;
}

void OnFramebufferResize(
    RendererFrontend *renderer, u32 newWidth, u32 newHeight)
{
    renderer->backend.onFramebufferResize(
        renderer->backend.renderer, newWidth, newHeight);
}

Mesh *FindMesh(RendererFrontend *renderer, u32 id)
{
    Mesh *mesh = NULL;
    for (u32 i = 0; i < ARRAY_COUNT(renderer->meshes); i++)
    {
        if (id == renderer->meshes[i].id)
        {
            mesh = renderer->meshes + i;
            break;
        }
    }

    return mesh;
}

u32 TranslateCommands(RendererFrontend *renderer, DrawMeshCommand *cmds,
    u32 count, RendererBackendCommand *outputCmds, u32 outputCapacity)
{
    ASSERT_MACRO(count <= outputCapacity);

    for (u32 i = 0; i < count; i++)
    {
        Mesh *mesh = FindMesh(renderer, cmds[i].mesh);
        if (mesh != NULL)
        {
            RendererBackendCommand *backendCmd = outputCmds + i;
            backendCmd->type = RendererBackendCommandType_DrawMesh;
            backendCmd->drawMesh.vertexIndexOffset = mesh->vertexIndexOffset;
            backendCmd->drawMesh.modelMatrixIndex = cmds[i].modelMatrixIndex;
            backendCmd->drawMesh.indexOffset = mesh->indexOffset;
            backendCmd->drawMesh.indexCount = mesh->indexCount;
            backendCmd->drawMesh.pipeline = cmds[i].pipeline;
            backendCmd->drawMesh.renderPass = cmds[i].renderPass;
        }
    }

    return count;
}

void RegisterMesh(RendererFrontend *renderer, u32 id, u32 vertexIndexOffset,
    u32 indexOffset, u32 indexCount)
{
    ASSERT_MACRO(renderer->meshCount < ARRAY_COUNT(renderer->meshes));
    Mesh *mesh = renderer->meshes + renderer->meshCount++;
    mesh->id = id;
    mesh->vertexIndexOffset = vertexIndexOffset;
    mesh->indexOffset = indexOffset;
    mesh->indexCount = indexCount;

    // TODO: Return that we failed to register mesh!
}

void RegisterTexture(RendererFrontend *renderer, u32 id, HdrImage image)
{
    RendererBackendCommand cmds[2] = {};
    cmds[0].type = RendererBackendCommandType_CreateImage;
    cmds[0].createImage.id = id;
    cmds[0].createImage.width = image.width;
    cmds[0].createImage.height = image.height;
    cmds[1].type = RendererBackendCommandType_UpdateImage;
    cmds[1].updateImage.id = id;
    cmds[1].updateImage.pixels = image.pixels;
    cmds[1].updateImage.length = sizeof(f32) * 4 * image.width * image.height;

    renderer->backend.processCommands(
        renderer->backend.renderer, cmds, ARRAY_COUNT(cmds));
}

void RegisterCubeMap(RendererFrontend *renderer, u32 id, HdrCubeMap cubeMap)
{
    u32 width = cubeMap.images[0].width; // TODO: Add checks
    RendererBackendCommand cmds[2] = {};
    cmds[0].type = RendererBackendCommandType_CreateCubeMap;
    cmds[0].createCubeMap.id = id;
    cmds[0].createCubeMap.width = width;
    cmds[1].type = RendererBackendCommandType_UpdateCubeMap;
    cmds[1].updateCubeMap.id = id;
    cmds[1].updateCubeMap.faceNumBytes = sizeof(f32) * 4 * width * width;
    for (u32 i = 0; i < MAX_CUBE_MAP_FACES; ++i)
    {
        cmds[1].updateCubeMap.pixels[i] = cubeMap.images[i].pixels;
    }

    renderer->backend.processCommands(
        renderer->backend.renderer, cmds, ARRAY_COUNT(cmds));
}
