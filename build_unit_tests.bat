@echo off

REM Vulkan SDK (environment variable is setup by the SDK installer)
set vulkan_include_path="%VULKAN_SDK%\Include"

REM GLFW 3.3.7 (Visual Studio 2022 MT config)
set glfw_include_path="..\external\glfw-3.3.7\include"

REM Compiler warning notes
REM 4201 complains about anonymous structs which we use for vec3, vec2, etc
REM 4820 warns if extra padding is added to a struct
REM 4514 warns if a inline function is not used
REM 4061 warns if enum is not explicitly handled in switch statement
REM 4711 warns if a function was inlined by the compiler even if we didn't mark it for inlining
REM 4710 warns if a function was marked for inlining but the compiler decided not to inline it

set compiler_flags=-Od -nologo -MT -Gm- -GR- -EHa -FC -Z7 -Wall -WX ^
    -wd4201 -wd4820 -wd4514 -wd5045 -wd4061 -wd5246 -wd5219 -wd4711 -wd4710 ^
    -DUNITY_FIXTURE_NO_EXTRAS ^
    -I "../external/unity" ^
    -I "../external/fff" ^
    -I %vulkan_include_path% ^
    -I %glfw_include_path% ^
    -I "../src"

set source_files="../unit_tests/test_runner.cpp" ^
    "../external/unity/unity.c" ^
    "../external/unity/unity_fixture.c"

set linker_flags=-opt:ref -incremental:no

REM All files are created in the build directory
IF NOT EXIST build mkdir build
pushd build

REM Monitor build and run time
ctime -begin ..\unit_tests.ctm

REM Compile and link our unit tests
cl %compiler_flags% %source_files% -link %linker_flags%
set last_error=%ERRORLEVEL%

REM Run the unit tests
test_runner.exe

ctime -end ..\unit_tests.ctm %last_error%

popd
exit %last_error%
