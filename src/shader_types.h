#pragma once

struct Vertex
{
    vec3 position;
    vec3 normal;
    vec2 textureCoords;
};

#define MAX_CAMERAS 4
#define DEFAULT_CAMERA_INDEX 0
#define SKYBOX_CAMERA_INDEX 1

struct MatrixBuffer
{
    mat4 viewMatrix[MAX_CAMERAS];
    mat4 projectionMatrix[MAX_CAMERAS];
    vec3 cameraPositions[MAX_CAMERAS];
};

struct Material
{
    vec3 albedo;
    f32 roughness;
};

struct PushConstants
{
    u32 vertexIndexOffset;
    u32 modelMatrixIndex;
    u32 materialIndex;
};

