#version 450
#extension GL_GOOGLE_include_directive : require

#include "common.glsl"

layout(location = 0) out vec4 outputColor;

layout(binding = 3) uniform samplerCube skyboxTexture;

layout(location=0) in vec2 fragTextureCoords;
layout(location=1) in vec3 fragLocalPosition;

void main()
{
    vec3 textureColor = texture(skyboxTexture, fragLocalPosition).rgb;
    outputColor = vec4(textureColor, 1);
}
