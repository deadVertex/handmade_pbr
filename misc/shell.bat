@echo off

REM Set compiler environment for Visual Studio 2022
call "F:\Program Files\Visual Studio 2022\VC\Auxiliary\Build\vcvarsall.bat" x64

REM Set compiler environment for Visual Studio 2017
REM call "C:\Program Files (x86)\Microsoft Visual Studio\2017\Community\VC\Auxiliary\Build\vcvarsall.bat" x64

REM Add ctime utility to our system path
set path=%~dp0..\external\ctime;%path%
