#version 450
#extension GL_GOOGLE_include_directive : require

#include "common.glsl"

layout(location = 0) out vec4 outputColor;

layout(binding = 3) uniform samplerCube skyboxTexture;

layout(location=0) in vec2 fragTextureCoords;
layout(location=2) in vec3 fragNormal;

void main()
{
    vec3 skyboxColor = texture(skyboxTexture, fragNormal).rgb;

    vec3 N = normalize(fragNormal);
    vec3 L = normalize(vec3(1, 1, 1));
    vec3 albedo = vec3(0.18);
    vec3 radiance = albedo * max(0.0, dot(N, L));
    outputColor = vec4(skyboxColor, 1);
}
