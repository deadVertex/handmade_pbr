#pragma once

#define RENDERER_MAX_MESHES 64

enum
{
    Mesh_Triangle,
    Mesh_Quad,
    Mesh_Cube,
    Mesh_Sphere,
};

struct Mesh
{
    u32 id;
    u32 vertexIndexOffset;
    u32 indexOffset;
    u32 indexCount;
};

struct RendererFrontend
{
    RendererBackend backend;

    u32 meshCount;
    Mesh meshes[RENDERER_MAX_MESHES];
};

struct DrawMeshCommand
{
    u32 mesh;
    u32 modelMatrixIndex;
    u32 pipeline;
    u32 renderPass;
};

RendererFrontend CreateRendererFrontend(RendererBackend backend);

void OnFramebufferResize(RendererFrontend *renderer, u32 width, u32 height);
