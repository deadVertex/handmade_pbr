#version 450
#extension GL_GOOGLE_include_directive : require

#include "common.glsl"

layout(location = 0) out vec4 outputColor;

void main()
{
    outputColor = vec4(0, 1, 0, 1);
}
