#include "core_types.h"
#include "core_macros.h"
#include "math_types.h"
#include "math_functions.cpp"
#include "vec2.cpp"
#include "vec3.cpp"
#include "vec4.cpp"

TEST_GROUP(MathLibrary);

TEST_SETUP(MathLibrary)
{
}

TEST_TEAR_DOWN(MathLibrary)
{
}

#define TEST_ASSERT_VEC3_EQUAL(A, B) \
    TEST_ASSERT_FLOAT_WITHIN(EPSILON, A.x, B.x); \
    TEST_ASSERT_FLOAT_WITHIN(EPSILON, A.y, B.y); \
    TEST_ASSERT_FLOAT_WITHIN(EPSILON, A.z, B.z);

#define TEST_ASSERT_VEC2_EQUAL(A, B) \
    TEST_ASSERT_FLOAT_WITHIN(EPSILON, A.x, B.x); \
    TEST_ASSERT_FLOAT_WITHIN(EPSILON, A.y, B.y); \

#define TEST_ASSERT_VEC4_EQUAL(A, B) \
    TEST_ASSERT_FLOAT_WITHIN(EPSILON, A.x, B.x); \
    TEST_ASSERT_FLOAT_WITHIN(EPSILON, A.y, B.y); \
    TEST_ASSERT_FLOAT_WITHIN(EPSILON, A.z, B.z); \
    TEST_ASSERT_FLOAT_WITHIN(EPSILON, A.w, B.w);

TEST(MathLibrary, AddVec3)
{
    vec3 a = Vec3(1.0f, 2.0f, 3.0f);
    vec3 b = Vec3(0.1f, 0.2f, 0.3f);

    vec3 expected = Vec3(1.1f, 2.2f, 3.3f);
    vec3 actual = a + b;
    TEST_ASSERT_VEC3_EQUAL(expected, actual);
}

TEST(MathLibrary, AddToVec3)
{
    vec3 a = Vec3(0.1f, 0.2f, 0.3f);
    vec3 b = Vec3(0.2f, 0.2f, 0.2f);

    vec3 expected = Vec3(0.3f, 0.4f, 0.5f);
    a += b;
    TEST_ASSERT_VEC3_EQUAL(expected, a);
}

TEST(MathLibrary, NegateVec3)
{
    vec3 a = Vec3(1.0f);
    vec3 expected = Vec3(-1.0f, -1.0f, -1.0f);
    vec3 actual = -a;
    TEST_ASSERT_VEC3_EQUAL(expected, actual);
}

TEST(MathLibrary, ScaleVec3)
{
    vec3 a = Vec3(0.1f, 0.2f, 0.3f);
    f32 b = 3.0f;
    vec3 expected = Vec3(0.3f, 0.6f, 0.9f);
    vec3 actual = a * b;
    TEST_ASSERT_VEC3_EQUAL(expected, actual);

    vec3 actualReversed = b * a;
    TEST_ASSERT_VEC3_EQUAL(expected, actualReversed);
}

TEST(MathLibrary, CrossVec3)
{
    vec3 a = Vec3(1, 0, 0);
    vec3 b = Vec3(0, 1, 0);
    vec3 expected = Vec3(0, 0, 1);
    vec3 actual = Cross(a, b);
    TEST_ASSERT_VEC3_EQUAL(expected, actual);
}

TEST(MathLibrary, AddVec2)
{
    vec2 a = Vec2(1.0f, 2.0f);
    vec2 b = Vec2(2.0f, 1.0f);

    vec2 expected = Vec2(3.0f, 3.0f);
    vec2 actual = a + b;
    TEST_ASSERT_VEC2_EQUAL(expected, actual);
}

TEST(MathLibrary, NegateVec2)
{
    vec2 a = Vec2(1.0f);
    vec2 expected = Vec2(-1.0f, -1.0f);
    vec2 actual = -a;
    TEST_ASSERT_VEC2_EQUAL(expected, actual);
}

TEST(MathLibrary, ScaleVec2)
{
    vec2 a = Vec2(1.0f, 2.0f);
    f32 b = 2.0f;
    vec2 expected = Vec2(2.0f, 4.0f);
    vec2 actual = a * b;
    TEST_ASSERT_VEC2_EQUAL(expected, actual);

    vec2 actualReversed = b * a;
    TEST_ASSERT_VEC2_EQUAL(expected, actualReversed);
}

TEST(MathLibrary, AddVec4)
{
    vec4 a = Vec4(1.0f, 2.0f, 3.0f, 4.0f);
    vec4 b = Vec4(1.0f);

    vec4 expected = Vec4(2.0f, 3.0f, 4.0f, 5.0f);
    vec4 actual = a + b;
    TEST_ASSERT_VEC4_EQUAL(expected, actual);
}

TEST(MathLibrary, NegateVec4)
{
    vec4 a = Vec4(1.0f);
    vec4 expected = Vec4(-1.0f, -1.0f, -1.0f, -1.0f);
    vec4 actual = -a;
    TEST_ASSERT_VEC4_EQUAL(expected, actual);
}

TEST(MathLibrary, ScaleVec4)
{
    vec4 a = Vec4(0.1f, 0.2f, 0.3f, 0.4f);
    f32 b = 4.0f;
    vec4 expected = Vec4(0.4f, 0.8f, 1.2f, 1.6f);
    vec4 actual = a * b;
    TEST_ASSERT_VEC4_EQUAL(expected, actual);

    vec4 actualReversed = b * a;
    TEST_ASSERT_VEC4_EQUAL(expected, actualReversed);
}

TEST(MathLibrary, LerpVec4)
{
    vec4 a = Vec4(1.0f, 2.0f, 3.0f, 4.0f);
    vec4 b = Vec4(0.0f, 1.0f, 0.0f, 2.0f);
    f32 t = 0.5f;

    vec4 expected = Vec4(0.5f, 1.5f, 1.5f, 3.0f);
    vec4 actual = Lerp(a, b, t);
    TEST_ASSERT_VEC4_EQUAL(expected, actual);
}

TEST(MathLibrary, ToSphericalCoordinates)
{
    // Given
    vec3 inputDirections[] = {
        Vec3(1, 0, 0),
        Vec3(0, 0, -1),
        Vec3(0, 1, 0),
        Normalize(Vec3(0, 1, 1)),
        Vec3(-1, 0, 0),
    };
    vec2 expectedSphereCoords[] = {
        Vec2(0.0f, PI * 0.5f),
        Vec2(-PI * 0.5f, PI * 0.5f),
        Vec2(0.0f, 0.0f),
        Vec2(PI * 0.5f, PI * 0.25f),
        Vec2(PI, PI * 0.5f),
    };

    for (u32 i = 0; i < ARRAY_COUNT(inputDirections); ++i)
    {
        vec3 direction = inputDirections[i];
        vec2 expected = expectedSphereCoords[i];

        // When
        vec2 sphereCoords = ToSphericalCoordinates(direction);

        // Then
        TEST_ASSERT_EQUAL_FLOAT(expected.x, sphereCoords.x);
        TEST_ASSERT_EQUAL_FLOAT(expected.y, sphereCoords.y);
    }
}

TEST(MathLibrary, MapToEquirectangular)
{
    // Given
    vec2 inputSphereCoords[] = {
        Vec2(0.0f, 0.0f),
        Vec2(0.0f, PI),
        Vec2(0.0f, PI * 0.5f),
        Vec2(PI * 0.5f, PI * 0.5f),
        Vec2(PI, PI * 0.5f),
        Vec2(PI * -0.5f, PI * 0.5f),
    };

    vec2 expectedUVs[] = {
        Vec2(0.0f, 1.0f),
        Vec2(0.0f, 0.0f),
        Vec2(0.0f, 0.5f),
        Vec2(0.25f, 0.5f),
        Vec2(0.5f, 0.5f),
        Vec2(0.75f, 0.5f),
    };

    for (u32 i = 0; i < ARRAY_COUNT(inputSphereCoords); ++i)
    {
        vec2 sphereCoords = inputSphereCoords[i];
        vec2 expected = expectedUVs[i];

        // When
        vec2 uv = MapToEquirectangular(sphereCoords);

        // Then
        TEST_ASSERT_EQUAL_FLOAT(expected.x, uv.x);
        TEST_ASSERT_EQUAL_FLOAT(expected.y, uv.y);
    }
}

TEST(MathLibrary, MapEquirectangularToSphereCoordinates)
{
    // Given
    vec2 inputUVs[] = {
        Vec2(0.0f, 1.0f),
        Vec2(0.0f, 0.0f),
        Vec2(0.0f, 0.5f),
        Vec2(0.25f, 0.5f),
        Vec2(0.5f, 0.5f),
        Vec2(0.75f, 0.5f),
    };

    vec2 expectedSphereCoords[] = {
        Vec2(0.0f, 0.0f),
        Vec2(0.0f, PI),
        Vec2(0.0f, PI * 0.5f),
        Vec2(PI * 0.5f, PI * 0.5f),
        Vec2(PI, PI * 0.5f),
        Vec2(PI * -0.5f, PI * 0.5f),
    };

    for (u32 i = 0; i < ARRAY_COUNT(inputUVs); ++i)
    {
        vec2 uv = inputUVs[i];
        vec2 expected = expectedSphereCoords[i];

        // When
        vec2 sphereCoords = MapEquirectangularToSphereCoordinates(uv);

        // Then
        TEST_ASSERT_EQUAL_FLOAT(expected.x, sphereCoords.x);
        TEST_ASSERT_EQUAL_FLOAT(expected.y, sphereCoords.y);
    }
}

TEST(MathLibrary, MapSphericalToCartesianCoordinates)
{
    // Given
    vec2 inputSphereCoords[] = {
        Vec2(0.0f, PI * 0.5f),
        Vec2(-PI * 0.5f, PI * 0.5f),
        Vec2(0.0f, 0.0f),
        Vec2(PI * 0.5f, PI * 0.25f),
        Vec2(PI, PI * 0.5f),
    };

    vec3 expectedDirections[] = {
        Vec3(1, 0, 0),
        Vec3(0, 0, -1),
        Vec3(0, 1, 0),
        Normalize(Vec3(0, 1, 1)),
        Vec3(-1, 0, 0),
    };

    for (u32 i = 0; i < ARRAY_COUNT(inputSphereCoords); ++i)
    {
        vec2 sphereCoords = inputSphereCoords[i];
        vec3 expected = expectedDirections[i];

        // When
        vec3 direction = MapSphericalToCartesianCoordinates(sphereCoords);

        // Then
        TEST_ASSERT_FLOAT_WITHIN_MESSAGE(EPSILON, expected.x, direction.x, "X axis");
        TEST_ASSERT_FLOAT_WITHIN_MESSAGE(EPSILON, expected.y, direction.y, "Y axis");
        TEST_ASSERT_FLOAT_WITHIN_MESSAGE(EPSILON, expected.z, direction.z, "Z axis");

        TEST_ASSERT_EQUAL_FLOAT(1.0f, Length(direction));
    }
}

// TODO: What is this test for?
TEST(MathLibrary, SphereCoordsSample)
{
    f32 phi = 0.0f;
    f32 theta = 0.0f;

    // Get sample vector in tangent space
    vec3 tangentDir = MapSphericalToCartesianCoordinates(Vec2(phi, theta));

    TEST_ASSERT_VEC3_EQUAL(Vec3(0, 1, 0), tangentDir);

    vec3 normal = Vec3(1, 0, 0);
    vec3 tangent = Vec3(0, 1, 0);
    vec3 bitangent = Vec3(0, 0, -1);

    vec3 worldDir = normal * tangentDir.y + tangent * tangentDir.x +
                    bitangent * tangentDir.z;

    TEST_ASSERT_VEC3_EQUAL(Vec3(1, 0, 0), worldDir);
}


TEST_GROUP_RUNNER(MathLibrary)
{
    // Vec3 test cases
    RUN_TEST_CASE(MathLibrary, AddVec3);
    RUN_TEST_CASE(MathLibrary, AddToVec3);
    RUN_TEST_CASE(MathLibrary, NegateVec3);
    RUN_TEST_CASE(MathLibrary, ScaleVec3);
    RUN_TEST_CASE(MathLibrary, CrossVec3);

    // Vec2 test cases
    RUN_TEST_CASE(MathLibrary, AddVec2);
    RUN_TEST_CASE(MathLibrary, NegateVec2);
    RUN_TEST_CASE(MathLibrary, ScaleVec2);

    // Vec4 test cases
    RUN_TEST_CASE(MathLibrary, AddVec4);
    RUN_TEST_CASE(MathLibrary, NegateVec4);
    RUN_TEST_CASE(MathLibrary, ScaleVec4);
    RUN_TEST_CASE(MathLibrary, LerpVec4);
}
