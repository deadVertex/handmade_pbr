#include "image.h"
#include "image.cpp"

TEST_GROUP(Image);

TEST_SETUP(Image)
{
}

TEST_TEAR_DOWN(Image)
{
}

TEST(Image, CreateImage)
{
    vec4 buffer[16] = {};
    StackAllocator allocator = CreateStackAllocator(buffer, sizeof(buffer));
    HdrImage image = CreateHdrImage(&allocator, 4, 4);

    TEST_ASSERT_NOT_NULL(image.pixels);
    TEST_ASSERT_EQUAL(4, image.width);
    TEST_ASSERT_EQUAL(4, image.height);
}

TEST(Image, SetPixel)
{
    vec4 buffer[4] = {};
    StackAllocator allocator = CreateStackAllocator(buffer, sizeof(buffer));
    HdrImage image = CreateHdrImage(&allocator, 2, 2);
    TEST_ASSERT_NOT_NULL(image.pixels);

    SetPixel(&image, 1, 1, Vec4(1, 0, 1, 1));

    TEST_ASSERT_VEC4_EQUAL(Vec4(1, 0, 1, 1), buffer[3]);
}

TEST(Image, GetPixel)
{
    vec4 buffer[4] = {};
    StackAllocator allocator = CreateStackAllocator(buffer, sizeof(buffer));
    HdrImage image = CreateHdrImage(&allocator, 2, 2);
    SetPixel(&image, 1, 1, Vec4(1, 0, 1, 1));

    vec4 pixel = GetPixel(image, 1, 1);
    TEST_ASSERT_VEC4_EQUAL(Vec4(1, 0, 1, 1), pixel);
}

TEST(Image, SampleImageNearest)
{
    vec4 buffer[4] = {};
    StackAllocator allocator = CreateStackAllocator(buffer, sizeof(buffer));

    // Given an image   | (0, 0, 0, 0) | (1, 1, 1, 1) |
    HdrImage image = CreateHdrImage(&allocator, 2, 1);
    SetPixel(&image, 0, 0, Vec4(0));
    SetPixel(&image, 1, 0, Vec4(1));

    // When we sample the center of the image
    vec4 sample = SampleImageNearest(image, Vec2(0.55f, 0.5f));

    // Then we get the color of the nearest pixel
    TEST_ASSERT_EQUAL_FLOAT(1, sample.r);
}

TEST(Image, SampleImageBilinear)
{
    vec4 buffer[4] = {};
    StackAllocator allocator = CreateStackAllocator(buffer, sizeof(buffer));

    // Given an image with a checkerboard pattern
    // | (0, 0, 0, 0) | (1, 1, 1, 1) |
    // | (1, 1, 1, 1) | (0, 0, 0, 0) |
    HdrImage image = CreateHdrImage(&allocator, 2, 2);
    SetPixel(&image, 0, 0, Vec4(0));
    SetPixel(&image, 1, 0, Vec4(1));
    SetPixel(&image, 0, 1, Vec4(1));
    SetPixel(&image, 1, 1, Vec4(0));

    // When we sample the center of the image
    vec4 sample = SampleImageBilinear(image, Vec2(0.5, 0.5));

    // Then we get the color of the 4 nearest pixels blended together
    TEST_ASSERT_EQUAL_FLOAT(0.5, sample.r);
}

TEST(Image, SampleImageBilinearClampToEdge)
{
    vec4 buffer[4] = {};
    StackAllocator allocator = CreateStackAllocator(buffer, sizeof(buffer));
    // Given an image with a checkerboard pattern
    // | (0, 0, 0, 0) | (1, 1, 1, 1) |
    // | (1, 1, 1, 1) | (0, 0, 0, 0) |
    HdrImage image = CreateHdrImage(&allocator, 2, 2);
    SetPixel(&image, 0, 0, Vec4(0));
    SetPixel(&image, 1, 0, Vec4(1));
    SetPixel(&image, 0, 1, Vec4(1));
    SetPixel(&image, 1, 1, Vec4(0));

    // When we sample the edge of the image
    vec4 sampleMax = SampleImageBilinear(image, Vec2(1.0, 0.5));
    vec4 sampleMin = SampleImageBilinear(image, Vec2(0.0, 0.5));

    // Then we get the color of the edge pixels blended
    TEST_ASSERT_EQUAL_FLOAT(0.5, sampleMax.r);
    TEST_ASSERT_EQUAL_FLOAT(0.5, sampleMin.r);
}

TEST_GROUP_RUNNER(Image)
{
    RUN_TEST_CASE(Image, CreateImage);
    RUN_TEST_CASE(Image, SetPixel);
    RUN_TEST_CASE(Image, GetPixel);
    RUN_TEST_CASE(Image, SampleImageNearest);
    RUN_TEST_CASE(Image, SampleImageBilinear);
    RUN_TEST_CASE(Image, SampleImageBilinearClampToEdge);
}
